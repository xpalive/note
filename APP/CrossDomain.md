### 浏览器测试跨域

#### 使用原生请求

```javascript
var xhr = new XMLHttpRequest();
xhr.open('POST', 'http://10.0.88.221:9527/smart-temple/login?userName=dev&pwd=123456&validateCode=b3b2', true);
xhr.send();
xhr.onload = function (e) {
    var xhr = e.target;
    console.log(xhr.responseText);
}
```

#### 使用 axios

```html

<html>
<head>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body>
这里是文档的主体 ... ...</br>
<p><label for="get">
    <input type="radio" name="radio" id="get" checked=true value="get">get
</label>
    <label for="post">
        <input type="radio" name="radio" id="post" value="post">post
    </label>
</p>
<p><textarea rows="3" cols="20" id="textfield">http://192.168.10.81:8081/test</textarea></p>
<p><input type="button" value="按钮" onclick="req()"></input></p>
</body>
</html>

<script>
    function req() {
        const url = document.getElementById("textfield").value
        const radioArr = document.getElementsByName("radio")
        let method;
        for (let radio of radioArr) {
            if (radio.checked) {
                method = radio.value
            }
        }
        axios({
            method: method,
            url: url
        })
        .then(function (response) {
            console.log(response)
        })
        .catch(function (error) {
            console.log(error)
        })
        // axios.post(url)
        //     .then(function (response) {
        //         console.log(response)
        //     })
        //     .catch(function (error) {
        //         console.log(error)
        //     })
    }

</script>

```

http://localhost:8886/tiantongsi/login?userName=dev&pwd=123456&validateCode=b3b2
http://www.jszw.info:8886/smart-temple/login?userName=dev&pwd=123456&validateCode=b3b2