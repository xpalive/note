### 设置自定义规则与服务器规则融合，通过javascript脚本
1. 在Settings界面中找到 profile Mixin
2. 选择JavaScript
3. JavaScript点击编辑（Edit）
4. 输入脚本
   ```javascript
    module.exports.parse = ({ content, name, url }, { yaml, axios, notify }) => {
      // 添加规则
      content.rules.unshift("DOMAIN-SUFFIX,51zhifo.com,DIRECT");
      content.rules.unshift("DOMAIN-KEYWORD,openai,DIRECT");
      // 添加dns解析
      content.dns['default-nameserver'].unshift("10.1.1.1");
      content.dns['nameserver'].unshift("10.1.1.1");
      content.dns['fallback'].unshift("10.1.1.1");
      return content
    }
   ``` 

### 命令
/usr/bin/clash 
clash -d 指定配置文件目录
clash -f 指定配置文件
clash -t 测试配置文件

### 后台启动
nohup clash > /dev/null 2>&1 &

### clash 证书问题导致代理不可用
证书问题https://www.microsoft.com/en-us/download/details.aspx?id=45633直接安装这个应该可以解决