### 账号密码
grafana admin/123@abcd


### docker compose
#### prometheus & grafana
```yaml
version: "3.9"
services:
  prometheus:
    image: prom/prometheus:v2.28.0
    container_name: prometheus
    volumes: 
      - /data/monitor/prometheus:/etc/prometheus
      - /etc/localtime:/etc/localtime:ro
    ports: 
      - 9090:9090
  grafana:
    image: grafana/grafana:5.4.5
    container_name: grafana
    volumes:
      - /data/monitor/grafana/config:/etc/grafana
      - /etc/localtime:/etc/localtime:ro
    ports:
      - 3000:3000
    depends_on:
      - prometheus
  mysql_galaxy_exporter:
    image: prom/mysqld-exporter:v0.13.0
    container_name: mysql_galaxy_exporter
    ports:
      - 9104:9104
    environment:
      - DATA_SOURCE_NAME=root:123abcd@(mysql01:3306)/galaxy
  redis_exporter:
    image: oliver006/redis_exporter:v1.24.0
    container_name: redis_exporter
    environment:
      - REDIS_PASSWORD=foobared
      - REDIS_ADDR=redis://redis01:6379
    ports:
      - 9121:9121
networks:
  default:
    external: true
    name: bear-net
```

#### prometheus
/etc/prometheus/prometheus.yml
```yaml
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 1m
scrape_configs:
  - job_name: prometheus
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    follow_redirects: true
    static_configs:
    - targets: ['prometheus:9090']

  - job_name: 'mysql_galaxy_exporter'
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    follow_redirects: true
    static_configs:
    - targets: ['mysql_galaxy_exporter:9104']
      labels: 
        instance: mysql_galaxy_exporter

  ## config for scraping the exporter itself
  - job_name: 'redis_exporter'
    scrape_interval: 10s  
    static_configs:
    - targets: ['redis_exporter:9121']
      labels: 
        instance: redis_exporter
  

```
#### grafana
grafana 参数配置  
/etc/grafana/grafana.ini 此文件不可少  
grafana 配置数据  
/etc/grafana/provisioning/datasource.yml  可不配置
```yaml
#apiVersion: 1
#deleteDatasources:
#- name: Prometheus
#  orgId: 1
#
#datasources:
#- name: Prometheus
#  type: prometheus
#  access: docker-proxy.md
#  orgId: 1
#  url: http://prometheus:9090
#  basicAuth: false
#  isDefault: true
#  version: 1
#  editable: true
```