### git 命令
本地仓库关联远程仓库(远程仓库未初始化)
```text
git init //初始化git本地仓库
touch .gitignore //添加忽略文件
git remote add origin <url> //添加远程仓库地址
git add . //将所有文件添加至index
git commit -m "init" //提交至本地仓库
git push -u origin master //推送至远程仓库
```

本地仓库关联远程仓库(远程仓库已初始化)
```text
git init
touch .gitignore
git remote add origin <url>
git add .
git commit -m "init"
git push -u origin master //推送至远程仓库会失败
git branch --set-upstream-to=origin/master master //设置上游仓库关联,可设置多个
git pull //如果出现refusing to merge unrelated histories则用下面的命令
git pull --allow-unrelated-histories //不同版本仓库pull的合并
git push 
```
克隆远程仓库
```text
git clone <url>
git clone -b [branch name] <url>
```

取消add的内容恢复暂存区
```shell
git restore --staged .
```

git自动清理本地文件
```text
git gc --auto
```
git将远程分支更新到最新状态(删除已废弃的远程branch)
```text
git fetch -p
or
git remote prune origin
```
git查看分支
```text
git branch -a
```
git创建本地分支
```text
git branch [branch name]
```
git推送本地分支至远程
```text
git push --set-upstream origin [branch name]
```
git切换分支
```text
git checkout [branch name]
```
git删除本地分支
```text
git branch -d [branch name]
```
git删除远程分支
```text
git branch -r -d origin/[branch name]
git push origin :[branch name]
```

没有执行git add的修改放弃
```shell
git checkout -- filepathname(eg: git checkout -- text.md)
```
放弃所有
```shell
git checkout .
```

已经执行了git add的修改
```shell
git reset HEAD filepathname(eg: git reset HEAD readme.md)
```
放弃所有
```shell
git reset HEAD .
```

已经执行了git commit的修改
```shell
git reset --hard HEAD^
```
可以用命令回退到任意版本
```shell
git reset --hard commitid
```

git放弃本地修改强制更新，对本地修改不做任何理会
```text
git fetch --all
git reset --hard origin/master
```
> git fetch 只是下载远程的库的内容，不做任何的合并 git reset 把Head指向刚刚下载的最新的版本

git reset命令
```text
git reset --hard HEAD^  //回退到上一个版本
git reset --hard HEAD^100  //回退到上100个版本
git reset --hard [log id]  //回退到指定log id 的版本
```

系统出现如下错误：warning: LF will be replaced by CRLF
```text
git config --gobal core.autocrlf false  //未实际操作过
```
git配置ssl认证
```text
git config --system http.sslverify false/true
```
git查看当前用户名和邮箱地址
```text
git config user.name
git config user.email
修改
git config --global user.name "username"
git config --global user.email "email"
git config --global core.editor vim
文件名过长问题
git config --global core.longpaths true  


```
> 会生成.gitconfig 文件   
仅仅是用于提交时的用户信息配置，并非用户名密码

通过git命令可以存储git账号名密码，第一次需要输入，之后不需要在当前系统下会生成.git-credentials 文件用于存储用户名密码
```text
git config --global credential.helper store
```


```shell script
git update # 升级git,2.17.1版本之前  
git update-git-for-windows # 升级git,2.17.1版本之后  
git remote -v # 查看远程仓库地址  
git remote set-url origin git@github.com:xpalive/utils.git # 修改远程地址
git remote set-url --add origin git@github.com:xpalive/utils.git # 添加远程地址
git switch -c dev # 创建分支并切换分支
git checkout -b dev # 创建分支并切换分支
git branch dev # 创建分支
git switch dev # 切换分支
git branch # 查看当前分支
git branch -d dev # 删除分支
git merge dev # 合并分支到当前分支
git config --global --list # 查看global配置
git merge --abort  #中止合并  
git reset --merge  #撤销合并  
git pull  #拉取代码  
```

```shell
git remote -v # 查看当前远程地址信息
git remote add origin http://github.com/xpalive/note
git remote add gitee http://gitee.com/xpalive/note
# 这样推送就需要分两次推送
git push -u origin
git push -u gitee
# 如果想一次性推送两个地址那么需要如下操作
git remote set-url --add origin http://gitee.com/xpalive/note
# 再次查看当前仓库远程地址信息
origin http://github.com/xpalive/note (fetch)
origin http://github.com/xpalive/note (push)
origin http://gitee.com/xpalive/note (push)

# 这样就可以一次性推送2个地址了(测试)
git push
```

### .ssh 配置
```shell
Host github.com
    HostName github.com
    User git
    # IdentityFile ~/.ssh/id_rsa # key地址
    # 走 HTTP 代理
    # ProxyCommand socat - PROXY:127.0.0.1:%h:%p,proxyport=8080
    # 走 socks5 代理（如小飞机 or V2xxx）
    ProxyCommand connect -S 127.0.0.1:7890 %h %p
    # linux下配置应为
    # ProxyCommand nc --proxy-type socks5 -- proxy 127.0.0.1:7891 %h %p
```

```shell
clone: git clone -c http.proxy="127.0.0.1:xxxx" https://github.com/Gump8/xxxx.git  
fetch upstream: git -c http.proxy="127.0.0.1:xxxx" fetch upstream  
*注意: fetch 后面不能 -c, clone 是可以的  
```

```shell
git clone -b <分支> <仓库>
git clone <仓库>
git clone -c http.proxy="127.0.0.1:xxxx" https://github.com/Gump8/xxxx.git  
```