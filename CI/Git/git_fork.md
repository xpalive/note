### fork 一个仓库并同步
一般情况下应该是先将原始仓库fork到自己的github上
然后设置upstream，将需要修改的分支切一个branch出来，进行修改或阅读
当原分支有修改时就可以继续将原分支更新同步到自己的github上

1. [Configuring a remote for a fork][1] 添加upstream
```shell
# List the current configured remote repository for your fork.
$ git remote -v
> origin  https://github.com/YOUR_USERNAME/YOUR_FORK.git (fetch)
> origin  https://github.com/YOUR_USERNAME/YOUR_FORK.git (push)

# Specify a new remote upstream repository that will be synced with the fork.
$ git remote add upstream https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git

# Verify the new upstream repository you've specified for your fork.
$ git remote -v
> origin    https://github.com/YOUR_USERNAME/YOUR_FORK.git (fetch)
> origin    https://github.com/YOUR_USERNAME/YOUR_FORK.git (push)
> upstream  https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git (fetch)
> upstream  https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git (push)
```
2. [Syncing a fork from the command line][2]
```shell
# Fetch the branches and their respective commits from the upstream repository.
# Commits to BRANCHNAME will be stored in the local branch upstream/BRANCHNAME.
$ git fetch upstream
> remote: Counting objects: 75, done.
> remote: Compressing objects: 100% (53/53), done.
> remote: Total 62 (delta 27), reused 44 (delta 9)
> Unpacking objects: 100% (62/62), done.
> From https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY
>  * [new branch]      main     -> upstream/main

# Check out your fork's local default branch - in this case, we use main.
$ git checkout main
> Switched to branch 'main'

# Merge the changes from the upstream default branch -
# in this case, upstream/main - into your local default branch.
# This brings your fork's default branch into sync with the upstream repository,
# without losing your local changes.
$ git merge upstream/main
> Updating a422352..5fdff0f
> Fast-forward
>  README                    |    9 -------
>  README.md                 |    7 ++++++
>  2 files changed, 7 insertions(+), 9 deletions(-)
>  delete mode 100644 README
>  create mode 100644 README.md
```





[1]:https://docs.github.com/en/github/collaborating-with-pull-requests/working-with-forks/configuring-a-remote-for-a-fork
[2]:https://docs.github.com/en/github/collaborating-with-pull-requests/working-with-forks/syncing-a-fork