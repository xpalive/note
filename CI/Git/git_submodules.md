### submodule
通过submodule可以在同一个仓库下管理多个不同仓库的代码，且互不影响
参考：
https://www.jianshu.com/p/2d74a6f41d07
https://cloud.tencent.com/developer/article/2136829

### 创建一个含有submodule的项目
1. 在项目中添加一个第三方库
2. 创建一个文件夹用于存放第三方库
```shell
mkdir lib
cd lib
git submodule add git@github.com:xpalive/SubMoudule.git
git submodule add git@github.com:xpalive/SubMoudule.git src/B/C/SubMoudule "希望 submodule 位于的文件夹路径"
$ git commit -m "Add timezone converter library as a submodule"
```

### 克隆一个含有submodule的项目
```shell
git clone git@github.com:xpalive/ParentMoudule.git
git submodule update --init --recursive
# 或者
git clone git@github.com:xpalive/ParentMoudule.git --recurse-submodules
```

### 配置项
```shell
# 配置通过主项目命令来操作子模块的更新(不包含clone和push)
# 启用该命令后，可以直接在主项目执行pull等命令完成子模块的更新操作，不需要单独使用gitsumodule相关命令，简化操作
git config --global submodule.recurse true 

# 配置通过主项目命令来操作子模块更新推送
# 通过git push 来代替需要进入子模块后才能进行的push操作
git config --global push.recurseSubmodules on-demand

# 跟踪submodule特定分支（固定子模块分支）
# submodule.ffmpeg.branch 项目子模块名称 
# dev 分支名称
git config -f .gitmodules submodule.ffmpeg.branch dev

#### 删除子模块
# 通过git submodule deinit 子模块名称 来删除本地子模块，如果添加–force还会清空保存的工作区，但是这种方式不能完全移除子模块的配置，还要配合后边一条指令
git submodule deinit ffmpeg
# 通过git rm 子模块名称 来删除本地gitsubmodule的相关配置以及移除子模块目录
git rm ffmpeg
# 清除当前目录git缓存
git rm --cached
```
