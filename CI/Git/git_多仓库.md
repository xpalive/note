```shell
git remote -v # 查看当前远程地址信息
git remote add origin http://github.com/xpalive/note
git remote add gitee http://gitee.com/xpalive/note
# 这样推送就需要分两次推送
git push -u origin
git push -u gitee

------------------------------------------------------

# 如果想一次性推送两个地址那么需要如下操作
git remote set-url --add origin http://gitee.com/xpalive/note
# 再次查看当前仓库远程地址信息
origin http://github.com/xpalive/note (fetch)
origin http://github.com/xpalive/note (push)
origin http://gitee.com/xpalive/note (push)

# 这样就可以一次性推送2个地址了(测试)
git push
```