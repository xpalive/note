### gradle 的生命周期
1. 项目扫描
2. 配置：运行build.gradle脚本，创建任务图
3. 执行：构建项目有用的部分


### gradle 配置 项目根目录中 gradle.properties
org.gradle.daemon=true     #启用gradle后台进程
org.gradle.parallel=true   #多线程编译
org.gradle.jvmargs=-Xms2048
