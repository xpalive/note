### jar 下载
mvn dependency:get -DremoteRepositories=https://repo1.maven.org/maven2/ -DgroupId=org.xerial.snappy -DartifactId=snappy-java -Dversion=1.1.7
mvn dependency:sources -DremoteRepositories=https://repo1.maven.org/maven2/ -DgroupId=org.xerial.snappy -DartifactId=snappy-java -Dversion=1.1.7
> dependency:get是一个插件
全名是: org.apache.maven.plugins:maven-dependency-plugin:3.1.2:get
可选参数(等号右边改为实参):
-Dartifact=groupId:artifactId:version[:packaging[:classifier]] (等于是下面的几个参数组合写在一起)
-DgroupId=groupId
-DartifactId=artifactId
-Dversion=version
-Dpackaging=packaging
-Dclassifier=classifier (表示在相同版本下针对不同的环境或者jdk使用的jar, 用于进一步限定)
-DremoteRepositories=id::[layout]::url(或者仅写url, 多个仓库源用逗号分隔, 例如 central::default::https://repo.maven.apache.org/maven2,myrepo::::https://repo.acme.com,https://repo.acme2.com)
-Dtransitive=true | false (是否下载传递的依赖jar, 默认true)
-Dskip=false | true (是否跳过插件的执行, 默认false)


### mvn 打包
文档地址：https://maven.apache.org/ref/3.9.1/maven-embedder/cli.html
maven.apache.org -> release Note -> reference documentation -> CLI-options
```shell
mvn clean package -pl module_name -am -Dmaven.test.skip=true
```
-pl (--projects) 构建指定模块
-am (--also-make) 同时构建依赖模块
-rf (--resume-from) 从指定模块开始继续构建

-Dmaven.test.skip=true 表示跳过测试，并且不编译测试代码
-DskipTests 表示跳过测试，但是编译测试代码到target/test-classes下

mvn clean package -rf :spider-net-blui

```shell
mvn clean package -pl ksign-main-manage -am -Dmaven.test.skip=true  -Dmaven.javadoc.skip=true -Dmaven.compile.fork=true -P online -T 1C

mvn clean install -pl ksign-cesium -am -Dmaven.test.skip=true  -Dmaven.javadoc.skip=true -Dmaven.compile.fork=true -P online -T 1C
```
-P 指定profile
-T 开启多线程 （-T 4 是直接指定4线程  /  -T 1C 表示CPU线程的倍数）

