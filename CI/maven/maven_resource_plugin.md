### 指定多个目录
```xml
<project>
    <build>
        <resources>
            <resource>
                <directory>[your folder here]</directory>
                <filtering>true</filtering> <!-- 表示需要替换占位符 -->
            </resource>
            <!-- 可以指定多个 -->
            <resource>
                <directory>[your folder here]</directory>
            </resource>
            <resource>
                <directory>[your folder here]</directory>
                <!-- 包含的文件 -->
                <includes>
                    <include>[resource file #1]</include>
                    <include>[resource file #2]</include>
                </includes>
                <!--  -->
                <excludes>
                    <exclude>[non-resource file #1]</exclude>
                    <exclude>[non-resource file #2]</exclude>
                </excludes>
            </resource>
        </resources>
        
        <plugin>
            <gourpId>org.apache.maven.plugins</gourpId>
            <artifactId>maven-resources-plugin</artifactId>
            <version>3.1.0</version>
            <configuration>
                <nonFilteredFileExtensions>
                    <nonfilteredFileExtension>json</nonfilteredFileExtension>
                </nonFilteredFileExtensions>
            </configuration>
        </plugin>
    </build>
</project>
```