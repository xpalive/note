### 以下是一些常用的 CMake 内建变量
`CMAKE_SOURCE_DIR`: 顶层 `CMakeLists.txt` 文件所在的目录，与 `PROJECT_SOURCE_DIR` 等效。  
`CMAKE_BINARY_DIR`: 顶层 CMake 构建目录（通常是你运行 CMake 生成构建文件的目录，例如 build 目录）。  
`PROJECT_SOURCE_DIR`: 当前项目的根目录，与 `CMAKE_SOURCE_DIR` 等效。  
`PROJECT_BINARY_DIR`: 当前项目的构建目录，与 `CMAKE_BINARY_DIR` 等效。  
`CMAKE_CURRENT_SOURCE_DIR`: 当前处理的 `CMakeLists.txt` 文件所在的目录。  
`CMAKE_CURRENT_BINARY_DIR`: 当前处理的 `CMakeLists.txt` 文件生成的二进制文件的目录。  

### find_package方法 和 pkg_check_modules方法
${AVFORMAT_INCLUDE_DIRS} 是通过 pkg-config 工具从 FFmpeg 的 .pc 配置文件中获取的。
在使用 find_package(PkgConfig REQUIRED) 和 pkg_check_modules(AVFORMAT REQUIRED libavformat) 这两行代码时，
CMake 会调用 pkg-config 来查询 libavformat 库的相关信息。  

调用 pkg_check_modules(AVFORMAT REQUIRED libavformat) 时，
CMake 会设置 AVFORMAT_* 变量，其中包括：  

AVFORMAT_FOUND：如果找到库，则为 TRUE。  
AVFORMAT_INCLUDE_DIRS：库的头文件搜索路径。  
AVFORMAT_LIBRARY_DIRS：库文件的搜索路径。  
AVFORMAT_LIBRARIES：需要链接的库。  
AVFORMAT_VERSION：库的版本。  

#### 示例`CMakeLists.txt`
```text
cmake_minimum_required(VERSION 3.10)
project(MyCppProject)

# 设置 C++ 标准
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# 添加 include 目录
include_directories(${PROJECT_SOURCE_DIR}/include)

# 添加库搜索路径
link_directories(${PROJECT_SOURCE_DIR}/libs)

# 查找共享库（假设库名为 MyLibrary）
find_library(MYLIBRARY_LIB MyLibrary PATHS ${PROJECT_SOURCE_DIR}/libs)

# 如果找到了共享库，配置使用它
if(MYLIBRARY_LIB)
    message(STATUS "Found MyLibrary: ${MYLIBRARY_LIB}")
else()
    message(FATAL_ERROR "MyLibrary not found in ${PROJECT_SOURCE_DIR}/libs")
endif()

# 添加源文件
set(SOURCES
    src/main.cpp
    src/mylibrary/myclass.cpp
)

# 定义可执行文件
add_executable(MyCppProject ${SOURCES})

# 生成共享库
add_library(MyLibrary SHARED ${SOURCES})

# 设置库版本
set_target_properties(MyCppProject PROPERTIES VERSION 1.0 SOVERSION 1)

# 链接共享库到可执行文件
target_link_libraries(MyCppProject ${MYLIBRARY_LIB})

```