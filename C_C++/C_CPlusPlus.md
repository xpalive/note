### * 与 &
#### *
1. 当 * 用作定义变量，表示定义的变量时指针类型。
   1. （int *b  (int*) b） b就是个指针
2. 其他场景表示取指针的值
   1. int a = *b 表示取指针b指向的值给a，此处b必须为指针类型或是指针地址
3. 函数名就是指针
   1. int plus(int a,int b)
   2. int (* plus)(int a,int b)

#### &
1. 位与
2. 当&与变量一起使用时，表示取内存地址  &b 取 b的地址
3. 当&用作定义变量的时候表示引用变量

### 左值和右值
左值的英文简写为“lvalue”，右值的英文简写为“rvalue”。很多人认为它们分别是"left value"、"right value" 的缩写，其实不然。lvalue 是“loactor value”的缩写，可意为存储在内存中、有明确存储地址（可寻址）的数据，而 rvalue 译为 "read value"，指的是那些可以提供数据值的数据（不一定可以寻址，例如存储于寄存器中的数据）。

reference
https://blog.csdn.net/kangshuangzhu/article/details/122796756
https://www.zhihu.com/question/37608201/answer/1601079930
https://blog.csdn.net/Z_xOOOO/article/details/119515300

### 基本概念
``` 
#include <iostream>
using namespace std;

int main() {
    // i 变量
    int i = 20;
    // 声明指针p 指向 i的地址 ”&i 为取址运算“
    int* p = &i;
    // *p 解引用操作， 将p指针指向的地址的值获取出来
    int value = *p;
    // &d 标识一个引用变量，这意味着d将引用p，并且对d的操作会影响到指针p
    // 这种引用的使用在 C++ 中有时用于函数的参数传递，允许函数内部修改传递给它的指针，同时也会影响到函数外部指针的值。
    int* &d = p;

    cout << &i << endl;
    cout << d << endl;
    cout << &d << endl;
    cout << &p << endl;
    cout << value << endl;
    cout << p << endl;
    cout << *d << endl;

    return 0;
}
```