### 中央仓库
https://conan.io/center

### 管理软件 conan
C:\app\Conan\conan
配置环境变量 path

### 使用
下载依赖包
ex:
conan install conanfile.txt --output-folder=build --build=missing

ex:
conan install . --output-folder=cmake-build-debug --build=missing -s build_type=Debug
`conan install` ：这是运行 Conan 安装命令的起始部分。
`.` ：点号表示当前目录，这意味着命令将在当前目录中查找配置文件conanfile.txt。
`--output-folder=cmake-build-debug` ：这个选项指定了输出文件夹的路径。在这个例子中，输出文件夹被设置为  `cmake-build-debug` ，这意味着安装的依赖项将被放置在该文件夹中。
`--build=missing` ：这个选项指定了构建策略。 `missing`  表示只构建缺失的包，而不是重新构建所有包。这可以加快构建过程，避免不必要的构建。
`-s build_type=Debug` ：这个选项设置了构建类型为  `Debug` （当然如果没有这条参数的话，默认使用上文第二点提到的default文件所制定的配置）。这将影响构建的输出文件，通常用于开发和调试目的。
