### ubuntu 下 gcc版本管理
查看gcc软连接  
sudo update-alternatives --config gcc  
查看安装了的gcc版本  
dpkg -l | grep gcc  
添加gcc软连接  
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.7 30 --slave /usr/bin/g++ g++ /usr/bin/g++-4.7  
删除gcc软连接
sudo update-alternatives --remove gcc /usr/bin/gcc-9

--install指令里的数越大，则优先级越高 这里gcc-4.7 是 30


#### gcc 安装
sudo apt update
sudo apt install build-essential

如果安装不上可以换清华源
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak  备份
```清华源
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-backports main restricted universe multiverse
 
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-security main restricted universe multiverse
# # deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-security main restricted universe multiverse
 
deb http://security.ubuntu.com/ubuntu/ jammy-security main restricted universe multiverse
# deb-src http://security.ubuntu.com/ubuntu/ jammy-security main restricted universe multiverse
 
# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-proposed main restricted universe multiverse
# # deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-proposed main restricted universe multiverse
```
sudo apt update 换源后需要刷新

#### 参考
https://blog.csdn.net/krokodil98/article/details/113394752

