cmake_minimum_required (VERSION 2.8...3.2)
project(demoForFile)

# 指定了不同版本的构建参数
set(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g -ggdb -lpthread")
set(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")


get_filename_component(DIR_3RD_ABOVE ${CMAKE_CURRENT_SOURCE_DIR} ABSOLUTE)

message(STATUS ${DIR_3RD_ABOVE})

link_directories(${DIR_3RD_ABOVE}/3rd/lib/ffmpeg)
link_directories(${DIR_3RD_ABOVE}/3rd/lib/fisheye)
link_directories(${DIR_3RD_ABOVE}/3rd/lib/uv)

include_directories(${DIR_3RD_ABOVE}/3rd/include/fisheye)
include_directories(${DIR_3RD_ABOVE}/3rd/include/ffmpeg)
include_directories(${DIR_3RD_ABOVE}/3rd/include/uv)
include_directories(${DIR_3RD_ABOVE}/src)

get_property(dirs DIRECTORY PROPERTY   INCLUDE_DIRECTORIES)
get_property(linkdirs DIRECTORY PROPERTY   LINK_DIRECTORIES)

message("include Directories: ${dirs}")
message("link Directories: ${linkdirs}")

add_subdirectory(src)
# 指定生成目标
add_executable(fisheyeDemo main.cpp)

# 添加链接库
target_link_libraries(fisheyeDemo uv twinvision fisheyecorrect  pthread  avformat avcodec swscale avutil avfilter swresample
audcodec_linux g726_linux stdg722_linux x264
stdaacld_linux g7221_linux imf svml intlc g711_linux g722_linux g728_linux g729_linux
adpcm_linux g7221c_linux mp3enc_linux mp3dec_linux aaclcenc_linux aaclcdec_linux opus_linux
g719_linux aacldenc_linux aaclddec_linux mp2_linux amr_nb_linux audcommonlib_linux videomanage_linux)