## MingW和cygwin都可以用来跨平台开发。

### 推荐使用
x86_64-posix-seh
posix相比win32拥有C++ 11多线程特性，sjlj和seh对应异常处理特性，seh对应异常处理零开销（异常需要自己处理）

### MinGW
> MinGW是Minimalistic GNU for Windows的缩写，也就是Win版的GCC。 
相对的MingW也有一个叫MSys（Minimal SYStem）的子项目，
主要是提供了一个模拟Linux的Shell和一些基本的Linux工具。
因为编译一个大型程序，光靠一个GCC是不够的，还需要有Autoconf等工具来配置项目，
所以一般在Windows下编译ffmpeg等Linux下的大型项目都是通过Msys来完成的，
当然Msys只是一个辅助环境，根本的工作还是MingW来做的。

### Cygwin
> Cygwin则是全面模拟了Linux的接口，提供给运行在它上面的的程序使用，
并提供了大量现成的软件，更像是一个平台。

### 用MingW和cygwin编译出来的程序的区别。
> 首先,MingW和cygwin都不能让Linux下的程序直接运行在Windows上，必需通过源代码重新编译。
现代操作系统包括Windows和Linux的基本设计概念像进程线程地址空间虚拟内存这些都是大同小异
的，之所以二者上的程序不能兼容，主要是它们对这些功能具体实现上的差异，首先是可执行文件
的格式，Window使用PE的格式，并且要求以.EXE为后缀名。Linux则使用Elf。

> 其次,操作系统的API也不一样，如Windows用CreateProcess()创建进程，而Linux使用fork()。
所以要移植程序必然要在这些地方进行改变，MingW有专门的W32api头文件，来把代码中Linux方式
的系统调用替换为对应的Windows方式。而Cygwin则通过cygwin1.dll这个文件来实现这种API的转换
，并模拟一个Linux系统调用接口给程序，程序依然以Linux的方式调用系统API，只不过这个API在
cygwin1.dll上，cygwin1.dll再调用Windows对应的实现，来把结果返回给程序。可以用查看他们编
译好的程序的导入表来验证这点。二者生成的程序都是能在Windows上运行的EXE文件，显然都是PE
格式，用一个PE格式查看工具检查一下就能发现，Cygwin生成的程序依然有fork()这样的Linux系统
调用，但目标库是cygwin1。而MingW生成的程序，则全部使用从KERNEL32导出的标准Windows系统API。
这样看来用Mingw编译的程序性能会高一点，而且也不用带着那个接近两兆的cygwin1.dll文件。但
Cygwin对Linux的模拟比较完整，甚至有一个Cygwin X的项目，可以直接用Cygwin跑X。另外Cygwin
可以设置-mno-cygwin的flag，来使用Mingw编译。而与Cygwin更有可比性的MSys上的工具也是通过
Cygwin这种模拟的方式来提供的。总之这两个项目有千丝万缕的关系，一个不恰当的比方，如果
Mingw是MFC，Cygwin就是.NET了。
        
### MinGW-W64 GCC 各小版本异同
for further reference, please consider following sections;
https://blog.csdn.net/m0_60352504/article/details/119978713

x86_64-posix-sjlj
x86_64-posix-seh
x86_64-win32-sjlj
x86_64-win32-seh

posix 版本：这个版本的线程是使用了pthread的Windows版本，posix是操作系统统一接口标准，不同操作系统需要提供相同的接口
以方便应用移植，降低移植成本，但是只支持POSIX线程模型。pthread是在Linux下常用的线程库，使用posix接口意味着应用的多
线程模型移植更方便，哪怕从Linux下拉过来一个应用框架也可以较小代价修改代码。但是在某些情况下，会引起性能下降，毕竟
不是Windows原生的线程摩，C标准可能到C11以上，并且支持Cll (C语言编码标准)的特性。

win32 版本：这个版本使用的是Windows的原生线程库，性能等方面表现更出色，兼容性更好，没有C11那些眼花缭乱的特性，就看
你是否熟悉Windows相关接口。

sjlj(setjmp/longjmp):这个版本的编译器支持Windows下的异常抛出。即你的程序出现错误时，可以调用Windows的异常处理，好比如Linux下载程序运行错误时会报
Segment Fault,同时也支持在自己的程序中插入setjmp()。或者kmgjmp()函数以完成类似goto功能。这个版本支持Win32和64,但用这个编译器时你的代码编译成的二进制文
件大小会膨胀，运行性能上损失约15%其至更多，哪怕你的代码没有走到异常分支也是这样。即：增大了性能开铺，毕竟Windows麻。

seh (zero overhead exception):异常分支处理零开销，即：不会调用windows的异常处理回调，完全由用户自己设计实现。该版本支持win64。

dwarf(DW2, dwarf-2):首先dwarf是一种可执行文件的格式，类似于Linux下的elf文件，以及Windows专用的*exe或者安卓的*.apk安装包内的可执行二进制文件，dwarf内部
描述了可执行程序的函数符号信息、关联的动态库、变量、堆栈大小等一系列信息。该版本会将程序编译成以dwarf格式的二进制文件，Windows本身支持该类型的可执
行文件，但是有个缺陷：如果你想调试你的程序，如gdb打断点调试，就不能调试dll接口(dll是Windows的动态库文件)，这样对于调用dll接口地方的传参你就没有办法
调试，从而没有办法得知是否是数据人参不正确或者库函数接口调用方式不正确。并且这个版本只有Win32版本，而且编译时所有调用栈信息都会用氐zrf格式存储，而非
兼容windows可执行格式。

### mingw64目录结构
bin  - gcc g++等编译器可执行文件目录
...
x86_64-w64-mingw32  - readelf objdump等工具目录

### 备注
* 修改编译器,让window下的编译器把诸如fork的调用翻译成等价的形式--这就是mingw的做法. 
* 修改库,让window提供一个类似unix提供的库,他们对程序的接口如同unix一样,而这些库,当然是由win32的API实现的--这就是cygwin的做法.

### TDM-GCC