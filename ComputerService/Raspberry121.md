### 安装的软件

**java 环境**

/usr/local/java/jdk1.8.0_381

---
**nginx**

**安装目录：**/usr/local/nginx  
**启动命令：**/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf  

---
**proxychains4**

- /etc/proxychains4.conf
- /usr/bin/proxychains4

---
**clash**

- /usr/bin/clash
- /home/xiongping/.config/clash/config.yaml
### 定时任务：
crontab -e
```shell
0 */1 * * * sudo /bin/bash /home/xiongping/bin/raspberry_job.sh >> /home/xiongping/log/rsp.log 2>&1
0 */1 * * * /bin/bash /home/xiongping/bin/cf-ddns.sh >> /home/xiongping/log/ddns.log 2>&1
```

### 硬件版本
V1.4