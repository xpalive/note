### VMware虚拟机上共享主机网络的设置方法
产品：VMware® Workstation 16 Pro
版本：16.1.2 build-17966106

#### VMware网络设置步骤
1. 选择 -> 编辑
   1. 选择 -> 虚拟网络编辑器
   2. 选择 -> 更改设置
   3. 选择 -> VMnet8
      1. 选择 -> NAT设置
         1. 设置 -> 网关IP：192.168.159.2 
         2. 勾选 -> 允许活动的FTP
         3. 勾选 -> 允许任何组织唯一标识符
      2. 勾选 -> 将主机虚拟适配器连接到此网络
      3. 勾选 -> 使用本地DHCP服务将IP地址分片给虚拟机
      4. 设置 -> 子网IP：192.168.159.0
      5. 设置 -> 子网掩码：255.255.255.0
2. 选择 -> 虚拟机
   1. 选择 -> 设置
      1. 选择 -> 网络适配器
         1. 勾选 -> 已连接
         2. 勾选 -> 启动时连接
         3. 勾选 -> NAT模式
#### VMware 网络无法连接（ubuntu 20 报错 Connection failed: Activation of network connection failed）
1. 选择 -> 编辑
2. 选择 -> 虚拟网络编辑器
3. 选择 -> 更改设置
4. 选择 -> VMnet0
5. 选择 -> 选择还原默认设置

### Centos设置
```shell
cd /etc/sysconfig/network-scripts/
vi ifcfg-ens33

# /etc/sysconfig/network-scripts/ifcfg-ens33
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
#BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
#IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33"
UUID="ae7cb769-bc95-43ed-82ea-50948acd95f5"
DEVICE="ens33"
ONBOOT="yes"

BOOTPROTO="static"
IPADDR="192.168.159.128"
NETMASK="255.255.255.0"
GATEWAY="192.168.159.2"
DNS1="10.1.1.1"

# 重启网络服务
service network restart
```

# 重启网络服务失败
1. 和NetworkManager服务有冲突
```shell
service NetworkManager stop
chkconfig NetworkManager off
service network restart
```

### 虚拟机时间同步
虚拟机 - 设置 - 选项 - vmtools - 开启时间同步

### mysql连不上报错10060
尝试重启下虚拟机，或宿主机

### docker 没有权限写入宿主机
chown -R systemd-bus-proxy log