### 树莓派
| 账号 | 密码 | 
| --- | --- | 
|  pi | 123@abcd | 
|  root | 123@abcd |

xrdp 3389  
ssh 22

#### 虚拟机

| 机器 | 账号 | 密码 | 描述 |
| --- | --- | --- | --- |
| env1/80 |  xp/root | 123@abcd | redis |
| env2/81 |  xp/root | 123@abcd | mysql |
| env3/82 |  xp/root | 123@abcd | master nginx/app |
| env4/83 |  xp/root | 123@abcd | slave nginx/app |
| env5/84 |  xp/root | 123@abcd | slave nginx/app |

通用密码：123@abcd

#### 读源码工具
sourceInsight