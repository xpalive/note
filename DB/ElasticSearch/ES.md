### es 可视化工具
Cerebro

### es第三方包
https://easy-es.cn/#/en/

### ES 语法
should 或者
must 并且
must_not 
sort 排序
term 词项
boost 权重


### ES中的geo_point 之BKD树
https://www.elastic.co/cn/blog/lucene-points-6-0
KD树 k-dimensional tree
BKD树 block k-dimensional tree


### BM25 用定相似度调节BM25
k1：这个参数控制着词频结果在词频饱和度中的上升速度。默认值为1.2。值越小饱和度变化越快，值越大饱和度变化越慢。
b：这个参数控制着字段长归一值所起的作用，0.0会禁用归一化，1.0会启用完全归一化。默认值为0.75。

### DSL 查询
Domain Specific Language ===  based on JSON to define queries

### 倒排所以

### B+tree 索引

### 位图(bit map) 、 咆哮位图(roaring bit map)
`位图 [1,2,4,6,8,9] -> [1,1,0,1,0,1,0,1,1]`
`咆哮位图 [1,65537] -> (0,1) (1,1)`

### es的索引 对比 mysql的索引
FST：有穷状态转化器，字典树的升级版 近实时的原因

磁盘压缩（索引帧技术） 256 为一个块

过滤查询一般用缓存查询（咆哮位图）

es -> memory buffer(flush/s) -> file system cache(flush/30min) -> disk

### es 中 query dsl 和 filter dsl 的区别
query:
relevance
full text
no cache
slower

filter:
boolean yes/no
extra values
cached
faster

### es中分片和索引几乎等价
1个有50个分片的索引和 50个索引都只有一个分片查询是等价的

### es中的别名是可以对应到多个索引的
在es中别名可以当作分组来使用

### ES优化文件系统缓存filesystem cache
https://blog.csdn.net/ZGL_cyy/article/details/118230086