### DSL
// 查询 and
```json lines
GET ksign_ar_item/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "appId": 2
          }
        },
        {
          "term": {
            "appName": "ksign"
          }
        }
      ]
    }
  }
}
```