### 语法

```text
_search 搜索
_doc 操作文档
_bulk 批量操作
_count get the number of matches for a search query
```

### dsl 语义

```json lines
POST /es/_db/_doc/_search
{
  "query": {
    "term": {
      "name": "i am chinese"
    }
  }
}
POST /es/_db/_doc/_search
{
  "query": {
    "match": {
      "name": "i am chinese"
    }
  }
}
POST /es/_db/_doc/_search
{
  "query": {
    "multi_match": {
      "query": "xiongping",
      "fields": [
        "name",
        "title"
      ]
    }
  }
}

POST /es/_db/_doc/_search
{
  "query": {
    "query_string": {
      "query": "xiongping OR dingyang"
    }
  }
}

// term 表示不分词
// match 表示会分词
// match_phase 会分词，但是会顺序要求
// query_string 查询所有字段 可以使用 'AND' 'OR' 连接字符
```

### es 映射 mapping

分为动态映射和静态映射 动态映射：插入数据时推断数据类型 静态映射：先定义数据类型，再插入数据

### group by

```json lines
GET /ksign_ar/_search
{
  "size": 0,
  // 指定hits返回的数量
  "aggs": {
    "appName": {
      "terms": {
        "field": "appName"
      }
    }
  }
}
```

### count

```json lines
GET /ksign_ar_item/_count
{
  "query": {
    "term": {
      "itemType": {
        "value": 1
      }
    }
  }
}
```

### 查询es mapping

GET /es_db/mapping

### 创建es mapping

```json lines
PUT /es_db
{
  "mapping": {
    "name": {
      "type": "keyword",
      "index": true,
      "store": true
    }
  }
}
```

index true/false表示是/否分词 store

### 查询

```json lines
GET /es_db/_search
{
  "query": {
    "match": {
      "remark": {
        "query": "java architect assistant",
        "minimum_should_match": "68%"
      }
    }
  }
}

// minimum_should_match 值的匹配百分数
```

### 查询es信息

GET /

### 查看所有索引

GET /_cat/indices?v

### 查询es集群节点

GET /_cat/nodes?v

### 查询某个索引

GET /#{index}/_search

### 根据id获取数据

get /#{index}/_doc/${id}

### ES查询hits统计总数不准确

hits.total.value 当超过10000条时返回10000 需要返回靳准值时

```text
GET /#{index}/_search
{
    "track_total_hits": true
}
此时一次性查询出总数
```

### DSL
boost 权重值
dis_max 
    best fields 最佳字段
    most fields 最多字段
    tie_breaker
```json
{
  "查询DSL": {
    "记录查询": {
      "无条件": {
        "sample": "match_all:{}"
      },
      "有条件": {
        "叶子查询（单条件）": {
          "模糊匹配": {
            "sample1": "match",
            "sample2": "prefix",
            "sample3": "regexp"
          },
          "精确匹配": {
            "sample1": "term:{fieldName:fieldVal}",
            "sample2": "terms:{fieldName:[fieldVal1,fieldVal2]}",
            "sample3": "range:{fieldName:{gte:0,lte:2}}",
            "sample4": "exists:{'field':fieldName}",
            "sample5": "ids:[id1,id2]"
          }
        },
        "组合查询（多条件）": {
          "sample1": {
            "bool": [
              "must",
              "filter",
              "must_not",
              "should"
            ]
          },
          "sample2": "constant_score",
          "sample3": "dis_max"
        },
        "连接查询": {
          "父子文档查询": {
            "has_child": {
              "type": "指定子文档名称",
              "query": "子文档的查询条件",
              "inner_hits": "内层过滤"
            },
            "has_parent": {
              "parent_type": "指定父文档名称",
              "query": "父文档的查询条件",
              "inner_hits": "内层过滤"
            }
          },
          "嵌套文档查询": {
            "nested": {
              "path": "嵌套字段路径",
              "query": "嵌套文档查询条件"
            }
          }
        }
      }
    },
    "结果聚合（aggs）": {
    }
  }
}
```

### 深分页
通过 _scroll_id 进行深分页 

### 参考

https://www.jianshu.com/p/59999ddc0a6a  ES基本语法
https://blog.csdn.net/qq_25438391/article/details/121399842    DSL语法
https://www.cnblogs.com/hld123/p/15972227.html   统计总数及深度分页

https://www.elastic.co/guide/en/elasticsearch/reference/current/rest-apis.html
https://www.elastic.co/guide/en/elasticsearch/client/java-rest/6.8/index.html   java api