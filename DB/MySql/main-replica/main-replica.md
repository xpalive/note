### 主库sql
create user 'rep'@'%' identified by '123@abcd';
grant replication slave,replication client on *.* to 'rep'@'%';

show master STATUS;
查看binlog和position

show GLOBAL VARIABLES like 'server_id';
show VARIABLES like 'log_%';


### 从库sql
change master to master_host='192.168.10.81',master_port=3306,master_user='rep',master_password='123@abcd',master_log_file='mysql-bin.000002',master_log_pos=613;

start SLAVE;


show GLOBAL VARIABLES like 'server_id';
show VARIABLES like 'binlog_format';
show slave STATUS;

#### 如果从库停止同步 slave_sql_running = no
stop slave; --先停止从库同步
通过show slave STATUS; 查看当前的同步位置
1. master_log_file = 'xxx'
2. read_master_log_pos = 1846

更新配置`change master to master_host='192.168.142.128',master_port=3307,master_user='rep',master_password='123@abcd',master_log_file='mysqlbin.000001',master_log_pos=989;`
start slave; --重新启动从库同步

#### read_only = 1对super权限用户无效


### 参考
https://zhuanlan.zhihu.com/p/459558400
https://zhuanlan.zhihu.com/p/371118458
https://blog.csdn.net/xianjuke008/article/details/125722519
https://www.cnblogs.com/cheyunhua/p/16715086.html
https://blog.csdn.net/yaxuan88521/article/details/123939786
https://blog.csdn.net/weixin_45310179/article/details/100600424