```sql
SELECT
	table_name,
	column_name
FROM
	information_schema. COLUMNS
WHERE
	table_name = 'tableName'
AND column_name = 'RESULT';
```

### mysql 表更新自己
```sql
UPDATE tb_house join ( select CONCAT_WS(" ",h.fullName,h.roomNum) as aa,h.id from tb_house as h where h.dictId=287) as bb
on (tb_house.id=bb.id) set tb_house.fullName = bb.aa
WHERE dictId=287
```

### 查询 mysql 隔离级别
```sql
-- 查询
show variables like 'tx_isolation'
-- 设置
set tx_isolation = 'REPEATABLE-READ'
```

### 查询 mysql sql记录
> 开启日志模式  
> SET GLOBAL log_output = 'TABLE';SET GLOBAL general_log = 'ON';  //日志开启  
> SET GLOBAL log_output = 'TABLE'; SET GLOBAL general_log = 'OFF';  //日志关闭  

> 查询日志  
> SELECT * from mysql.general_log ORDER BY event_time DESC;  

> 清空表（delete对于这个表，不允许使用，只能用truncate）  
> truncate table mysql.general_log;  

> 查询主键自增最大值
> SELECT auto_increment from information_schema.tables WHERE table_schema = database() and table_name = 'ksign_log';
> 更新主键自增最大值
> ALTER TABLE ksign_app_date auto_increment = 5;

### mysql 连接url
jdbc:mysql://10.68.8.26:3306/twin_vision_platform?useUnicode=true&characterEncoding=UTF8&rewriteBatchedStatements=true&serverTimezone=PRC&useSSL=false&allowMultiQueries=true

useUnicode=true  使用Unicode字符集
characterEncoding=UTF8   使用UTF8编码
rewriteBatchedStatements=true  重写批量语句
serverTimezone=PRC  设置时区
useSSL=false  关闭ssl
allowMultiQueries=true  允许多语句查询