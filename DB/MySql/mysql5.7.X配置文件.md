#mysql5.7.X 配置文件
```conf

[mysqld]
user        = mysql
socket      = /var/run/mysqld/mysqld.sock
port        = 3306
basedir     = /usr  #mysql的安装目录
datadir     = /var/lib/mysql #mysql数据目录
tmpdir      = /tmp
bind-address        =  0.0.0.0
key_buffer      = 16M
max_allowed_packet  = 16M
thread_stack        = 192K
thread_cache_size       = 8
myisam-recover         = BACKUP
query_cache_limit   = 1M
query_cache_size        = 16M
log_error                = /var/log/mysql/error.log
expire_logs_days    = 10  #从日志过期时间
max_binlog_size         = 100M
skip-name-resolve  #禁止对外部连接进行dns解析
max_connections = 200  # 默认的链接最大数量
max_connect_errors = 10  # 默认的重连最大次数
character-set-server=utf8  # 默认的字符集
default-storage-engine=INNODB  # 默认的存储引擎
# binlog
log-bin=mysqlbin #默认在 /var/lib/mysql
binlog-format=ROW #binlog格式，以记录的方式
server-id=123456 #服务唯一ID


[client]
port        = 3306
socket      = /var/run/mysqld/mysqld.sock
user=root # 可以使客户端免密  
password=123@abcd # 可以使客户端免密  

[mysqld_safe]
socket      = /var/run/mysqld/mysqld.sock
nice        = 0

[mysqldump]
quick
quote-names
max_allowed_packet  = 16M

[mysql]

[isamchk]
key_buffer      = 16M
```

> mysql配置文件的组是为了配置对应程序的参数（与程序名一致）  
> 如： `[mysqld]`和`[mysql]`那么就是分别对应mysqld服务端和mysql客户端的配置 

### Mysql 启动脚本
${MYSQL_HOME}/bin/mysqld --lower_case_table_names=1 --initialize-insecure

--lower_case_table_names=1 表名是否区分大小写 0区分 1不区分 
--initialize-insecure 初始化数据库时不设置root密码





