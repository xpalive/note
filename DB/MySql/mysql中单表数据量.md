### 为什么说mysql单表数据量不超过2000w条
![img.png](image/mysql-single-table-record.png) b+tree
![img.png](image/mysql-page-construct.png) 页存储结构
假设存储在mysql中的数据大小为1k左右
mysql中一页的存储为16k

假设数据大小为1k
那么一页数据为16条

假设索引为bigint类型，占8个字符+innodb中指针的6个字符
那么一页可以存储的索引为 `16k*1024 / (8+6) =1170` -> 8 + 6 表示索引+指针，索引和指针是间隔存储的

所以2层树存储的数据量为 1170 * 16 = 18720；3层树存储的数据量为 1170 * 1170 * 16 = 21902400；
