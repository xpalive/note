1. 查询当前连接数 show status like '%Threads%';
2. 查询历时连接次数 show status like '%connections%'
3. 查询当前具体的线程 show processlist;
4. 查询 thread_cache_size 的值 show VARIABLES LIKE '%thread_cache_size%';
5. 线程缓存未命中率=Threads_created /Connections
6. 查看 缓存命中率的值
7. 设置thread_cache_size的值 set global thread_cache_size=32