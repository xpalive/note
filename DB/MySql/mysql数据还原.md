### mysql数据还原
> 恢复数据库https://www.cnblogs.com/YCcc/p/10825870.html
> show master logs; 查看binlog
> show master status; 查看最后一条binlog

> 通过log_pos 还原数据
> show binlog events in "mysqlbin.00003" limit 0,3; 查看内容
> /usr/bin/mysqlbinlog  --stop-position=435 --database=test  /var/lib/mysql/mysqlbin.000006 | /usr/bin/mysql -uroot -p密码 -v test
> /usr/bin/mysqlbinlog  --start-position=488 --stop-position=5122 --database=test  /var/lib/mysql/mysqlbin.000002 | /usr/bin/mysql -uroot -p123@abcd -v test

> 通过时间还原数据
> 使用查看工具查看  mysqlbinlog mysqlbin.00001;
> /usr/bin/mysqlbinlog --start-datetime="2018-04-27 20:57:55" --stop-datetime="2018-04-27 20:58:18" --database=hello /var/lib/mysql/mysql-bin.000009 | /usr/bin/mysql -uroot -p8856769abcd -v hello