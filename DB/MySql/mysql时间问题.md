### 配置服务器时区
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

```text
查询时区
show VARIABLES like '%time_zone%';
```