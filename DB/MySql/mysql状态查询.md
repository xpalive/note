### mysql当前状态查询

> SHOW PROCESSLIST;  -- 当前线程查询  
> SHOW FULL PROCESSLIST;   
> SHOW VARIABLES LIKE '%max_connections%';  
> SHOW STATUS LIKE '%Connection%';

> show status like 'Threads%'; 
> +-------------------+-------+  
> | Variable_name     | Value |  
> +-------------------+-------+  
> | Threads_cached    | 58    |  
> | Threads_connected | 57    |   
> | Threads_created   | 3676  |     
> | Threads_running   | 4     |   
> +-------------------+-------+  
> Threads_running 这个数值指的是激活的连接数，这个数值一般远低于connected数值    
> Threads_created 表示创建过的线程数，如果发现threads_created值过大的话，表明mysql服务器一直在创建线程，这也是比较耗资源  
> Threads_connected 这个数值指的是打开的连接数  
> Threads_connected 跟show processlist结果相同，表示当前连接数。准确的来说，Threads_running是代表当前并发数    

> show variables like '%max_connections%';  
> +-----------------+-------+  
> | Variable_name   | Value |  
> +-----------------+-------+  
> | max_connections | 100  |  
> +-----------------+-------+ 
>
> 可以在/etc/my.cnf里面设置数据库的最大连接数  
> max_connections = 1000
> show variables like '%timeout%' 查询超时时间
> show variables like '%thread%'
> show status like '%thread%'
> SHOW VARIABLES LIKE '%tx_isolation%'; 查询当前事务隔离级别
> SELECT * FROM INFORMATION_SCHEMA.INNODB_TRX; 查询当前事务
> 

版本查询
SELECT @@version

show open tables WHERE in_use > 0;  # 查看表被锁状态
show engine innodb status;  # 查询db是否发生死锁
select * from information_schema.INNODB_LOCKS;  # 查询正在锁的事务  lock_mode S锁（共享） X锁（独占）
desc information_schema.INNODB_LOCK_WAITS;
desc information_schema.INNODB_LOCKS;
desc information_schema.INNODB_TRX;

show processlist;
show status like 'table%';
show engine innodb status; # 开启innodb_lock_monitor后，再使用show engine innodb status查看，能够找到锁阻塞的根因。