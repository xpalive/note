### nosql 

#### mongodb
* 基本概念
  > database 数据库 
  > collection 数据库表/集合
  > document 数据库记录/文档
  > field 数据库字段/域
* 存储文档的结构： BSON 有数据大小限制，其中限制为 16m，也就是一条记录的大小不能超过16m
* GridFS存储大数据或者元数据

#### hbase

* 基本概念

  > table :  an Hbase table as a multi-dimensional map. An table consists of multiple rows
  > row: an Hbase row consists of a row key and one or more columns, rows are sorted alphabetically by row key as they are sorted.
  > column: a column in Hbase consists of column family and column qualifier
  > cell: a cell is a combination of row, column family, and column qualifiter, and contains a value and a timestamp



### 对比



| 项             | mongodb                                                      | hbase                                                        | 备注                                       |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------ |
| 单条数据存储   | BSON (Binary JSON)，有大小限制16M                            | 几乎无限扩展，列式存储，k/v 结构 wide column<br />column的值建议小于10M<br />cell (row ，column，version) |                                            |
| 分布式文件系统 | GridFS 将文件分割成小部分存储到document和collection中，默认使用fs.chunks （存储文件二进制数据） 和 fs.files （存储文件META信息），将文件的信息存储到Mongodb中 | HDFS (Hadoop Distributed File System) StoreFile(HFile)二进制流的形式存储在HDFS上block块儿中 |                                            |
| 数据类型       | 支持多种数据类型strings, 32 and 64 bit integers, floats, Decimal 128, dates, timestamps and **geospatial**（地理信息） | 二进制字节                                                   |                                            |
| 事务           | 4.x以后的版本提供了multi-document transactions               |                                                              |                                            |
| 数据量         | 通过集群方案可扩展 sharded cluster                           | 通过集群方案可扩展                                           | 单个实例的情况下hbase的存储能力高于mongodb |
| 数据结构       | 异构                                                         | 异构                                                         |                                            |
| 聚合分析       | mongodb提供了多种分析数据途径，如JOINs, graph traversals, search facets | hbase需要额外的专门分析数据的工具进行数据处理                |                                            |
| 索引           | 支持（B+tree）                                               | 不支持（只能按照主键范围查询或全表检索）                     | mongodb检索速度快于hbase                   |
| 擅长           | 比较适合条件查询，实时返回数据                               | 只能按照主键范围查询，或全表检索，适合数据分析，但是需要用到其他的分析工具如hive |                                            |
| 写入速度       | 需要维护主键索引，二级索引                                   | 只需要维护主键索引                                           | hbase比mongodb快                           |
| 门槛           | 适中                                                         | 难                                                           |                                            |
| 合适场景       | 读多于写                                                     | 写多于读，                                                   |                                            |
| 依赖           | 无                                                           | Hadoop                                                       |                                            |
| 集群架构       | 主从，副本集，分片（推荐，提高吞吐量，高可用，水平扩展）     | Hadoop，zookeeper                                            |                                            |



https://zhuanlan.zhihu.com/p/145551967

https://www.mongodb.com/compare/mongodb-hbase

https://blog.csdn.net/u010270403/article/details/51648462

hbase values limit https://hbase.apache.org/book.html#supported.datatypes

地理信息数据 https://docs.mongodb.com/manual/reference/geojson/
