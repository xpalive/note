### yum 安装 prostgresql
1. 安装yum 源
    ```shell
    sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
    # 如果yum 源安装报错那么需要安装一个ssl认证包
    yum install -y ca-certificates.noarch
    ```
2. 安装SQL服务
   ```shell
   sudo yum install -y postgresql14-server

   ```
3. 初始数据库，并设置自动启动（可选）
   ```shell
   # 初始化数据库
   sudo /usr/pgsql-14/bin/postgresql-14-setup initdb
   # 开机启动
   sudo systemctl enable postgresql-14
   # 启动服务
   sudo systemctl start postgresql-14
   ```