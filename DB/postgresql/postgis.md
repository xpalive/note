### R-Tree
https://blog.csdn.net/peterchan88/article/details/52248714
https://blog.csdn.net/u012436758/article/details/54586469
https://developer.aliyun.com/article/175035#slide-6
> r-tree rectangle-tree 就是将空间以矩形的方式划分出来，并以层叠的方式用更大的矩形
> 将空间进一步划分，![](img.png)
> 
> 


### pg sql
for further reference,please consider the following sections:
https://www.cnblogs.com/mchina/archive/2013/04/19/3028573.html

查询索引sql
select * from pg_stat_user_indexes;

查询索引sql
select indexrelname, pg_size_pretty(pg_relation_size(relid)), relid from pg_stat_user_indexes where schemaname='public' order by pg_relation_size(relid) desc;
-- 其中schemaname 固定是 public


### pg 备份恢复数据库
> 备份
> pg_dump -h 127.0.0.1 -U postgres ksign > ksign.bak
                          用户名    数据库    文件
> 恢复
> psql -h 127.0.0.1 -U postgres -d ksign_xp < ksign.bak
>                       用户名       数据库      文件

### pg 登录
su postgres #切换用户
psql -d gis  #登录gis这个用户

### 查看数据库中用户权限
\du
### 查看数据库
select datname from pg_database;
或
\l
### 进入数据库
\c 数据库名

### 查看表
\dt
或
select tablename from pg_tables where tablename not like 'pg%' and tablename not like 'sql_%' order by tablename
或
\d 数据库

### 查看表结构
\d 表名

### 显示字符集
\encoding

### 退出psql
\q

### 安装空间数据库扩展
CREATE EXTENSION IF NOT EXISTS postgis;

### 断开pg所有的连接
SELECT pg_terminate_backend(pid) from (SELECT pid from pg_stat_activity where datname = 'ksign_test') A


### 创建数据库
CREATE DATABASE "ksign_v1_0_4_120203"
WITH
OWNER = "ksign_user"
ENCODING = 'utF8'
LC_COLLATE = 'en_US.utf8'
LC_CTYPE = 'en_US.utf8'
TABLESPACE = "pg_default"
;
### 删除数据库
DROP DATABASE "ksign_v1_0_4_120203"
### 提升至超级用户权限
ALTER USER KSIGN USER WITH SUPERUSER;
### 取消超级用户权限
ALTER USER username WITH NOSUPERUSER;