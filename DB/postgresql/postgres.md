### 命令行登录
登录（默认环境下）
1. 切换到 postgres用户 => su postgres  
2. 执行登录命令 => psql

示例：  
创建一个dbuser用户，并创建一个exampledb数据库  
步骤：  
创建新用户  
CREATE USER dbuser WITH PASSWORD 'xxxxxxxxx';  
创建数据库  
CREATE DATABASE exampledb OWNER dbuser;  
赋权  
GRANT ALL PRIVILEGES ON DATABASE exampledb TO dbuser;  
退出  
\q
创建Linux普通用户，与dbuser同名  
sudo adduser dbuser  
sudo passwd dbuser  
以dbuser的身份链接数据库exampledb  
su dbuser 输入密码  
登录命令  
psql -d exampledb

### 最简单的做法

> 以你的普通Linux用户名，在数据库中创建同名的用户和数据库，如xxf，然后就可以本机直接连接到数据库xxf了。
> 
> ~> su - postgres
> Password:
> Last login: Wed Mar 1 13:19:02 CST 2017 on pts/1
> -bash-4.2$ psql
> psql (9.2.18)
> Type "help" for help.
> 
> postgres=# create user xxf with password '******';
> CREATE ROLE
> postgres=# create database xxf owner xxf;
> CREATE DATABASE
> postgres=# grant all privileges on database xxf to xxf;
> GRANT
> postgres=# \q
> -bash-4.2$ exit
> logout
> ~> psql
> psql (9.2.18)
> Type "help" for help.
> 
> xxf=>
> 
> 至此，就在数据库xxf中了。

### 四、开启远程访问
1、编辑配置文件
文件：postgresql.conf
位置：/var/lib/pgsql/data/postgresql.conf
添加/修改：在所有IP地址上监听，从而允许远程连接到数据库服务器：
listening_address: '*'
文件：pg_hba.conf
位置：/var/lib/pgsql/data/pg_hba.conf
添加/修改：允许任意用户从任意机器上以密码方式访问数据库，把下行添加为第一条规则：
host    all             all             0.0.0.0/0               md5
2、重启数据库服务：
$ sudo systemctl restart postgresql
3、此后即可从其它机器上登录，例如用Navicat for PostgreSQL：
主机名或IP： 172.*.*.*
端口：          5432
初始数据库： xxf
用户：          xxf
密码：          ******  (数据库用户xxf的密码，不是Linux用户xxf的密码)

### 查看时区
show time zone;
### 查看可选时区
select * from pg_timezone_names; 

### 修改时区
vi /var/lib/pgsql/9.4/data/postgresql.conf
修改timezone 为 Asia/Shanghai
修改log_timezone 为 Asia/Shanghai