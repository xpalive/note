### vacuum命令

#### 使用方法
vacuum `table_name`

查询统计信息更新情况
```sql
SELECT relname, last_vacuum, last_autovacuum, last_analyze, last_autoanalyze
FROM pg_stat_user_tables
WHERE schemaname = 'public' -- 替换成你的 schema 名称
  AND relname = 'table_name'; -- 替换成你的表名
```

定期对表空间进行优化
```sql
-- 查看当前autovacuum参数
show autovacuum 
-- 设置 autovacuum 参数为开启
ALTER SYSTEM SET autovacuum = on;

-- 设置 autovacuum 执行间隔（以秒为单位，这里设置为 1分钟）
ALTER SYSTEM SET autovacuum_naptime = '1min';

-- 设置 autovacuum 的触发阈值
-- 表示当表中被更新或删除的行数达到或超过 50 行时触发 autovacuum
ALTER SYSTEM SET autovacuum_vacuum_threshold = 50;

-- 如果是通过配置文件postgresql.conf进行修改需要重载配置文件
SELECT pg_reload_conf(); 
```

#### 作用

执行 VACUUM 命令后，对 PostgreSQL 数据库的优化表现主要体现在以下几个方面：

1. 回收空间：
VACUUM 命令会清理表中已标记为删除的行所占用的空间。在 PostgreSQL 中，当删除行时，并不会立即释放占用的磁盘空间，而是将其标记为“可复用”。VACUUM 命令会扫描表并释放这些被标记为删除的行所占用的物理存储空间，从而减少数据库文件的大小，提高磁盘使用效率。

2. 减少表碎片：
长时间运行和频繁更新的表可能会导致表中存在“碎片”，即数据库文件中未分配的空间块。VACUUM 命令会重新组织表中的数据，使得数据库文件中的物理布局更加紧凑和顺序化，减少访问数据时的随机访问，从而提升查询性能。

3. 更新统计信息：
VACUUM 命令在清理的同时会更新表的统计信息。这些统计信息包括表中的行数、最小值、最大值、数据分布等，这些信息对于 PostgreSQL 查询优化器来说非常重要。通过更新统计信息，优化器能够更准确地评估执行查询时选择的执行计划，从而提高查询性能。

4. 防止事务ID溢出：
在 PostgreSQL 中，每个事务都有一个唯一的事务ID（XID）。当事务ID达到特定限制时，可能会导致系统出现事务ID溢出的情况。VACUUM 命令会回收已完成的事务ID，从而延长数据库系统运行的时间，避免事务ID溢出对数据库系统的影响。

5. 其他效果：
VACUUM FULL 命令除了上述效果外，还会对整个表进行重组，从而进一步优化表的物理布局。然而，VACUUM FULL 是一个耗时较长且会阻塞表的操作，一般建议在非高负载时段执行。