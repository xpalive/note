[说明][https://www.cnblogs.com/wingcode/p/14527107.html]
> org.springframework.data.redis.RedisSystemException: Redis exception; nested exception is io.lettuce.core.RedisException: java.io.IOException: 远程主机强迫关闭了一个现有的连接。
> 为什么 Redis 连接会断
> 其实这个问题并不是很重要，因为Socket连接断已经是事实，而且在分布式环境中，网络分区是必然的。在网络环境，Redis 服务器主动断掉连接是很正常的，lettuce 的作者也提及 lettuce 一天发生一两次重连是很正常的。
> 
> 那么哪些情况会导致连接断呢：
> 
> Linux 内核的 keepalive 功能可能会一直收不到客户端的回应；
> 收到与该连接相关的 ICMP 错误信息；
> 其他网络链路问题等等；
> 如果要需要真正查明原因，需要 tcp dump 进行抓包，但意义不大，除非断线的概率大，像一天一两次或者几天才一次不必花这么大力气去查这个。而最主要的问题是 lettuce 客户端能否及时检测到连接已断，并尽快重连。
```java
public class LettuceConfig{
    @Bean
    public ClientResources clientResources(){
        NettyCustomizer nettyCustomizer = new NettyCustomizer() {
            @Override
            public void afterBootstrapInitialized(Bootstrap bootstrap) {
            }

            @Override
            public void afterChannelInitialized(Channel channel) {
                channel.pipeline().addLast(
                    new IdleStateHandler(30, 0,
                        0)
                );
                channel.pipeline().addLast(new ChannelDuplexHandler() {
                    @Override
                    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
                        throws Exception {
                        if (evt instanceof IdleStateEvent) {
                            ctx.disconnect();
                        }
                    }
                });
            }
        };
        return ClientResources.builder().nettyCustomizer(nettyCustomizer).build();
    }
}
```