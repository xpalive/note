### redis
1. redis的数据结构
2. redis的AOF与RDB
3. redis的AOF与RESP协议



### redis config 6.0
1. port 端口设置
2. rdb配置
> save 900 1 //注释掉  
> save 300 10 //注释掉  
> save 60 10000 //注释掉
3. aof配置
> appendonly yes
4. daemonize yes/no 守护进程配置
5. no-appendfsync-on-rewrite yes/no
> 当主进程在写aof文件的时候，子线程又在重新aof文件时（BGSAVE or BGREWRITEAOF），
> 两者都会有大量的磁盘操作，即使是不同的线程也会导致主进程的阻塞
> 如果注重延迟则设置为yes，如果注重数据则设置为no
> 如果是no则发生阻塞，等待重写aof文件完成后，主进程继续写aof文件
> 如果是yes则将主进程的数据先写在缓存中，待aof重写完成后再将输入写入aof文件，这样就可能会造成数据丢失
6. auto-aof-rewrite-percentage 100
> aof文件自上一次重写后文件大小增长了100%则再次触发重写
7. auto-aof-rewrite-min-size 64mb
> aof文件至少要达到64M才会自动重写，文件太小恢复速度本来就
很快，重写的意义不大
8. aof-use-rdb-preamble yes
> rdb aof 混合存储模式
9. maxmemory
> 最大可使用内存
10. maxmemory-policy
> 最大可使用内存到达时策略
11. maxmemory-samples
> 最大内存到达时，执行策略样本
12. protected-mode no
> protected-mode 修改为no，默认为yes，外部无法访问


### sentinel config

### redis数据结构
1.string 字符串
2.hash 哈希
3.list 链表
4.set 集合
5.zset 有序集合
6.bitmap 

### redis cluster
```shell
cluster info
cluster nodes

ttl [key] #剩余时间
```

### redis 压测
redis-benchmark -h 

### nio 多路复用
epoll 

### 渐进式遍历 scan

### redis install from source
tar -xzvf redis-stable.tar.gz
cd redis-stable
make

> If the compile succeeds, you'll find several Redis binaries in the src directory, including:
> redis-server: the Redis Server itself
> redis-cli is the command line interface utility to talk with Redis.

To install these binaries in /usr/local/bin, run:
make install

### redis 重新分配槽
redis-cli --cluster reshard <any-node-ip>:<any-node-port>  只需要输入节点中任意的ip和端口（因为redis cluster的节点彼此是互联的，通过任意节点都可以获取到整个集群的状态）