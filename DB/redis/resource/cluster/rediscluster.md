1.集群的配置
    1.1.开打redis集群
        cluster-enabled yes //集群模式
        cluster-config-file nodes-6379 //集群节点信息,这里会生成对应的文件
        cluster-node-timeout 15000
    1.2.reids数据存储
        使用 aof rdb 混合模式 
        aof-use-rdb-preamble yes
    1.3.打开 aof(append only file)
        appendonly yes // 每次写都将写操作命令追加文件
        aof-rewrite-incremental-fsync yes  // 默认32m
        rdb-save-incremental-fsync yes
需要修改的配置
1. port
2. pidfile
3. cluster-config-file

2.创建集群
> redis-cli -a redispass --cluster create --cluster-replicas 1 192.168.10.81:6379 192.168.10.82:6379 192.168.10.83:6379 192.168.10.81:6380 192.168.10.82:6380 192.168.10.83:6380
> 链接集群 redis-cli -a redispass -c -h 192.168.10.81 -p 6379

3.注意
1. 使用docker创建redis集群需要使用host network ([Redis Cluster and Docker]https://redis.io/docs/management/scaling/)
2. 当使用docker时 daemonize 设为no
3. 使用cluster模式至少有6个节点，三主三从