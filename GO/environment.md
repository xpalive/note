### environment
1. 下载并安装go语言环境，配置环境变量。（与java类似）
   1. GOPATH=C:\Users\xiongping\Go
   2. GO_HOME=C:\Go
   3. PATH=C:\Go\bin
   4. GOPROXY=https://goproxy.cn,direct
   5. GO111MODULE=on
2. 打开GO111MODULE
3. 设置GOPROXY

$GOPATH/pkg/mod 用于存放下载的依赖包
### GO111MODULE
```shell
go env -w GO111MOUDULE=on
```

go module 是golang的包依赖管理，从1.11开始支持使用go module
go module 有三个值on、off、auto
1. GO111MOUDULE=on 禁用模块支持，编译时从GOPATH和vendor文件夹中查找包
2. GO111MOUDULE=off 启用模块支持，编译时会忽略GOPATH和vendor文件夹，只根据go.mod下载依赖
3. GO111MOUDULE=auto 当项目在$GOPATH/src外且项目根目录有go.mod文件时，自动开启模块支持。

### GOPROXY
```shell
go env -w GOPROXY=https://goproxy.cn,direct
```
1. https://goproxy.cn 代理地址
2. direct fallback选项，如果遇到错误会去源地址抓取

设置go的代理，下载依赖时可能会被墙，可以通过代理解决
```text
https://goproxy.io
https://goproxy.cn （七牛云）
https://mirrors.aliyun.com/go…（阿里云）
https://mirrors.cloud.tencent…（腾讯云）
https://athens.azurefd.net（微软）
https://gocenter.io
```