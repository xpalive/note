### spring中Bean的创建过程

在spring容器启动时，spring会先实例化一个DefaultListableBeanFactory，一个BeanDefinitionReader，
和一个ClassPathScanner,并会注册一些默认的BeanFactoryPostProcessor和BeanPostProcessor的BeanDefinition
到DefaultListableBeanFactory中，BeanDefinitionReader会把我们传入的配置类转化为BeanDefinition并注册到DefaultListableBeanFactory中

做完准备工作就进入到refresh方法，开始对BeanFactoryPostProcessor进行处理，这里会优先处理我们通过容器手动注入的BeanDefinitionRegistryPostProcessor
然后再执行内部的BeanDefinitionRegistryPostProcessor，执行到这里，ConfigurationClassPostProcessor会根据我们
传入的配置类对我们的项目进行扫描，将所有被注解标记的类（@Component，@Configuration，@Import）读出来，并生成BeanDefinition注册到DefaultBeanListableBeanFactory中
然后再将所有扫描到的BeanDefinition进行再次的筛选，看看还有没有新的的BeanDefinitionRegistryPostProcessor被扫描到，直到没有新的
BeanDefinitionRegistryPostProcessor,spring
就会继续找到所有的BeanPostProcessor，并进行处理，同时完成BeanPostProcessor的初始化 ，

接着就进入到单例Bean的创建阶段，从DefaultListableBeanFactory的BeanDefinitionNames中循环所有的单例Bean，且不是懒加载的Bean
单例Bean的创建首先会进过Bean的实例化阶段：
首先确保BeanDefinition中定义的BeanClass为Class类型，如果为String类型则通过classLoader加载当前类名并获取当前bean的Class类
情况1：实例化前返回的对象(postProcessBeforeInstantiation) -> 初始化后(postProcessAfterInitialization) -> 返回bean
情况2：实例化前没有返回对象 -> 通过beanDefinition实例化当前bean对象 -> 处理BeanDefinition，(
postProcessMergedBeanDefinition注入点处理并缓存
@Autowired,@Value,@Resource） ->  填充Bean方法开始 ->
情况1：实例化后（postProcessAfterInstantiation）,可以操作Bean对象，如果返回false则直接跳出 -> 填充Bean方法结束
情况2：实例化后，可以操作Bean对象，但返回的是true -> 进行属性填充（postProcessProperties,处理成员变量的属性填充） ->  
-> 初始化Bean：-> aware回调(回调参数，beanName，classLoader，beanFactory) -> 初始化前（postProcessBeforeBeanInitialization,
这里会处理@PostConstruct注解由CommonAnnotationBeanPostProcessor处理）返回null时跳出责任链
-> 初始化(InitializingBean的子类执行AfterPropertiesSet方法，@Bean(initMethod=""),执行initMethod方法) ->
-> 初始化后(postProcessAfterInitialization,代理对象生成)
-> 需要对象销毁的Bean注册，将beanName和Bean添加到DisposableBeans的Map中
到Bean的初始化阶段：

### spring 的循环依赖解决方式

doCreateBean：

1. 在创建Bean A的时候会将Bean A的一部分代理对象生成的逻辑通过lambda表达式保存在singletonFactories中
2. 在PopularBean A的时候的时候，其属性对应的Bean B 在创建Bean时发现要注入Bean A 此时处于循环依赖，程序处于Bean A的填充方法中
3. Bean B在PopularBean的时候获取Bean A，此时在GetSingleton方法中可以从singletonFactories中获取到Bean A
4. 并且将Bean A 存储在earlySingletonObjects中，此时Bean B将A属性填充完毕，完成了Bean B的创建
5. 回到Bean A的填充方法中，完成Bean A的属性B的填充，此时Bean A完成了实例的实例化和初始化过程

### spring中的事务

### spring中的aop

### spring中的动态代理