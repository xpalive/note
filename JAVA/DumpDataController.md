```java
package com.zhiwo.biz.dump.controller;

import cn.hutool.core.io.IoUtil;
import com.zhiwo.biz.dump.dto.DumpQuery;
import com.zhiwo.biz.dump.service.DumpDataService;
import com.zhiwo.engine.rule.RulePF;
import com.zhiwo.engine.util.ProtostuffUtils;
import com.zhiwo.web.base.config.responseHandle.RespWrapSkip;
import io.swagger.annotations.Api;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dump-data")
@Api("数据导出")
@AllArgsConstructor
public class DumpDataController {

    private DumpDataService service;

    @GetMapping("/data")
    @RespWrapSkip
    public ResponseEntity<byte[]> data(DumpQuery query) {
        String hptNo = query.getHptNo();
        String importId = query.getImportId();

        String fileName = "data-" + hptNo + "-" + importId;
        RulePF rulePF = service.dumpPojo(query);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename=" + fileName);
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Last-Modified", new Date().toString());
        headers.add("ETag", String.valueOf(System.currentTimeMillis()));
        byte[] serializer = ProtostuffUtils.serializer(rulePF);
        return org.springframework.http.ResponseEntity
            .ok()
            .headers(headers)
            .contentLength(serializer.length)
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .body(serializer);
    }

    @GetMapping("/data2")
    @RespWrapSkip
    public void data2(DumpQuery query, HttpServletResponse response) {
        String fileName = "dataList1";
        Path path = Paths.get("C:\\Users\\xiongping\\Desktop\\dataList");
        File file = path.toFile();
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Last-Modified", new Date().toString());
        response.setHeader("ETag", String.valueOf(System.currentTimeMillis()));
        System.out.println("xia zai");
        ServletOutputStream out= null;
        try {
            out = response.getOutputStream();
            FileInputStream fileInputStream = new FileInputStream(file);
            IOUtils.copy(fileInputStream, out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
//此处记得关闭输出Servlet流
        IoUtil.close(out);
    }

    @GetMapping("/test")
    @RespWrapSkip
    public void test() {
        DumpQuery query = new DumpQuery();
        query.setImportId("20210714113015");
        query.setHptNo("004");
        RulePF rulePF = service.dumpPojo(query);
        System.out.println(rulePF);
    }
}

```
