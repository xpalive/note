### 拦截器

```java

@Configuration
public class TokenInterceptorConfig implements WebMvcConfigurer {
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //1.加入的顺序就是拦截器执行的顺序
        //2.按顺序执行所有拦截器的preHandle
        //3.所有的preHandle 执行完再执行全部postHandle 最后是postHandle
        registry.addInterceptor(tokenInterceptor()).addPathPatterns("/**").excludePathPatterns(excludePathPatterns);
    }
}

```

org.springframework.web.servlet.handler.MappedInterceptor.PatternAdapter#match