### 类型转换器

jdk 的类型转换器

```java
//定义自定义转换器
public class StringToUser extends PropertyEditorSupport implements PropertyEditor {
    @Override
    public void setAsText(String text) throws IllegalArgumentExcetpion {
        User user = new User();
        user.setName(text);
        this.setValue(user);
    }
}

public class AppConfig {
    // 注册自定义转换器
    @Bean
    public CustomEditorConfigurer customEditorConfigurer() {
        CustomEditorConfigurer customEditorConfigurer = new CustomEditorConfigurer();
        Map<Class<?>, Class<? extends PropertyEditor>> propertyEditorMap = new HashMap<>();
        propertyEditorMap.put(User.class, StringToUser.class);
        customEditorConfigurer.setCustomEditors(propertyEditorMap);
        return customEditorConfigurer;
    }
}

public class Demo {
    // 使用，此时会去找User.class的类型转换器
    @Value("xxx")
    private User user;
}
```

spring的类型转化器

```java
//定义自定义转换器
public class StringToUser implements ConditionalGenericConverter {
    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return sourceType.getType().equals(String.class) && targetType.getType().equals(User.class);
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        return Collections.singleton(new ConvertiblePair(String.class, User.class));
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        User user = new User();
        user.setName(source);
        return user;
    }
}

public class AppConfig {
    // 注册自定义转换器
    @Bean
    public ConversionServiceFactoryBean conversionService() {
        ConversionServiceFactoryBean customEditorConfigurer = new ConversionServiceFactoryBean();
        customEditorConfigurer.setConverters(Collections.singleton(new StringToUser()));
        return customEditorConfigurer;
    }
}

public class Demo {
    // 使用，此时会去找User.class的类型转换器
    @Value("xxx")
    private User user;
}
```

```java
public class Demo {
    public void demo() {
        SimpleTypeConverter typeConverter = new SimpleTypeConverter();
        typeConverter.registerCustomEditor(User.class, new StringToUserPropertyEditor());
        typeConverter.setConversionService(conversionService);

        User user = typeConverter.convertIfNecessary("1", User.class);
    }
}
```