### spring cloud
1. 分布式/版本化配置
2. 服务注册与发现 nacos
3. 路由
4. 服务到服务的调用 dubbo/fegin
5. 负载均衡
6. 断路器 sentinel
7. 全局锁 redission
8. 领导选举和集群状态
9. 分布式消息传递 kafka/rocketmq/rabbitmq

