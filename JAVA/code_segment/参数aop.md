```java
package com.xpalive.aop;

import com.xpalive.biz.muacloud.MuacloudConfig;
import com.xpalive.biz.muacloud.MuacloudException;
import com.xpalive.common.Constants;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author xiongping
 * @date 2024-09-02
 */
@Aspect
@Component
public class ApiTokenAop {

    @Autowired
    private MuacloudConfig muacloudConfig;

    @Around("execution(* com.xpalive.controller..*.*(..))")
    public Object logAroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        final Method method = getTargetMethod(joinPoint);
        if (method != null) {
            // 获取方法参数的注解
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            Object[] args = joinPoint.getArgs();
            // 遍历参数和注解
            for (int i = 0; i < args.length; i++) {
                for (Annotation annotation : parameterAnnotations[i]) {
                    if (annotation instanceof RequestHeader) { // 替换为您的自定义注解类
                        final RequestHeader header = (RequestHeader) annotation;
                        if (Constants.API_TOKEN.equals(header.value())) {
                            final Object arg = args[i];
                            if (!muacloudConfig.getApiToken().equals(arg)) {
                                throw new MuacloudException("apiToken不正确");
                            }
                        }
                    }
                }
            }
            return joinPoint.proceed();
        } else {
            throw new MuacloudException("没有找到对应的方法：" + joinPoint.getSignature().getName());
        }
    }

    // 获取目标方法
    private Method getTargetMethod(ProceedingJoinPoint joinPoint) {
        Method method = null;
        try {
            // 获取方法名和参数类型
            String methodName = joinPoint.getSignature().getName();
            Class<?>[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getParameterTypes();

            // 获取目标类的类对象
            Class<?> targetClass = joinPoint.getTarget().getClass();

            // 查找目标方法
            method = targetClass.getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return method;
    }
}

```