方法一
对象的字段加注EasyExcel对象Long类型太长导致导出精度损失问题解
@NumberFormat(value = “#”)

如果多个Long类型长度太长，不推荐这个地方要加，可以使用方法2

方法二
设置转换器
EasyExcel.write(fileName, cls)
.registerConverter(new LongStringConverter())

             
                       