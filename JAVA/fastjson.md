### 解决日期根式问题
1. 使用注解 @JSONFiled(format = "yyyy-MM-dd HH:mm:ss")
2. 使用 SerializerFeature.WriteDateUseDateFormat
3. 使用方法 JSON.toJSONStringWithDateFormat()

### 使用fastJson序列化并并反序列化之后存入MongoDB，Double类型变成了String
int disableDecimalFeature = JSON.DEFAULT_PARSER_FEATURE & ~Feature,UseBigDecimal.getMask();
String str = JSON.toJSON(entity).toString();
JSONObject jsonObj = JSON.parseObject(str,JSONObject.class,disableDecimalFeature);