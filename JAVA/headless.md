### headless 是什么？
> Headless模式是在缺少显示屏、键盘或者鼠标时的系统配置。听起来不可思议，但事实上你可以在这中模式下完成不同的操作，甚至是用图形数据也可以。
哪里才能用到此模式呢？想想你的应用不停的生成一张图片，比如，当用户每次登陆系统是都要生成一张认证图片。
当创建图片时，你得应用既不需要显示器也不需要键盘。让我们假设一下，现在你的应用有个主架构或者专有服务器，但这个服务没有显示器，键盘或者鼠标。
理想的决定是用环境的大量视觉计算能力而不是非视觉特性。在Headless模式下生成的图片可以传递到Headful系统进行更深层次渲染。
在java.awt.toolkit和java.awt.graphicsenvironment类中有许多方法，除了对字体，图像和打印的操作外还有调用显示器，键盘和鼠标的方法。
但是有一些类中，比如Canvas 和 Panel，可以在headless模式下执行。在J2SE 1.4平台之后就提供了对Headless模式的支持。
注：这篇文章重点讲的是Java SE6 平台版本的文档。任何API的增加或其他增强Java SE平台的规范是由JSR270专家组(JSR 270 Expert Group.)的审查和批准。

### -Djava.awt.headless=true
> 对于一个 Java 服务器来说经常要处理一些图形元素，例如地图的创建或者图形和图表等。
这些API基本上总是需要运行一个X-server以便能使用AWT（Abstract Window Toolkit，抽象窗口工具集）。
然而运行一个不必要的 X-server 并不是一种好的管理方式。
有时你甚至不能运行 X-server,因此最好的方案是运行 headless 服务器，来进行简单的图像处理。

### Linux系统中缺少GUI设备引起的HeadlessException

### 参考
https://blog.csdn.net/MissOfSpring/article/details/100899541
https://www.cnblogs.com/princessd8251/p/4000016.html
http://www.oschina.net/translate/using-headless-mode-in-java-se