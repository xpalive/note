### 内存溢出分析
启动项目时添加启动参数 -XX:+HeapDumpOnOutOfMemoryError -XX:heapDumpPath=${目录}/文件名.hprof
文件名可选，默认为javaheapDump.hprof

#### jhat工具
```shell
cmd > jhat -J-mx5120M d:\123\oom.hprof

cmd > jmap -dump:live,format=b,file=m.hprof <PID> // 手动导出当前内存dump
      
```

### MAT 工具

> Shallow Heap 代表一个对象结构自身所占用的内容大小，不包括其属性引用对象所占的内容  
> Retained Heap 是一个对象被GC回收后，可释放的内存大小，等于释放对象的Retained Heap中所有对象的Shallow Heap的和

通过 thread overview 查看你线程，以及当前线程的线程栈

1. 使用thread_overview
2. 使用dominator_tree 查看大对象

### idea 工具



https://www.jianshu.com/p/ca289e201e24
https://baijiahao.baidu.com/s?id=1697802507443643160&wfr=spider&for=pc
https://blog.csdn.net/Andy_Health/article/details/118933538