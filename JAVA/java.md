### java是解释型语言还是编译型语言
java语言需要通过java编译器将java文件编译为class文件，class文件再交由jvm（java虚拟机）解释执行

### 如何理解编译
编译的本质是把一个相对高级的语言转换为另一个相对低级的语言（机器码对应硬件来说也是相对高级的语言）

### java语言编译解释到运行的过程
*.java源文件编译为*.class文件，通过jvm解释*.class文件执行
具体步骤：
java文件编译为class文件
类加载器加载class文件
字节码校验器校验
解释器解释
操作系统

### java 类的加载过程
https://www.cnblogs.com/myshare/p/16892361.html  
[详见](java类加载/java类的加载过程.puml)

### java 实例的创建过程
使用new关键字的时候分配一段连续的内存空间，在内存分配完成后java虚拟机会创建对象的引用，指向对象在内存中的起始地址，
然后java虚拟机会对成员变量进行初始化（默认或显示的），最后调用构造方法，确保类处于正确的状态，完成实例的创建

### java 命令
jar uvf xxx.jar WEB-INF/classes/bootstrap.yml
替换jar中的文件

参考：
https://www.jianshu.com/p/420566d94573
https://blog.csdn.net/Genmer/article/details/119355224
https://www.pudn.com/news/628f83bdbf399b7f351eb05d.html