```text
[o.s.w.s.m.m.a.ExceptionHandlerExceptionResolver][doResolveHandlerMethodException][414] : Failure in @ExceptionHandler com.zhiwo.web.base.exceptions.ZWExceptionHandler#handleException(Exception)
org.springframework.http.converter.HttpMessageNotWritableException: No converter for [class com.zhiwo.web.base.R] with preset Content-Type 'application/octet-stream'
```
> 该问题可能是下载的时候将response的header设置为'application/octet-stream',但是出错了；
> 由@ExceptionHandler @RestControllerAdvice拦截处理后转换为com.zhiwo.web.base.R对象
> 写出时，应为 'application/json' 类型导致的错误

```text
java.io.IOException: UT010029: Stream is closed
```
> 原因同上，但不知道为何报不一样的错误