### jni & jna
#### jni
JNI定义了一种公用的语法
当Java和c/c++都遵循这样的语法时就可以互相调用(也可调用汇编等其余语言)
JNI不能直接调用c/c++的库，必须使用java编写调用函数，生成C头文件，
再利用C头文件编写C代码，生成动态库，最后JNI使用新生成的动态库完成执行。
这种方式过程繁琐，需增加改动Java和C/C++的程序。

#### jna
JNA提供了一组Java工具类，
用于在运行期间动态访问系统本地库（native library：如Window的dll）
而不需要编写任何Native/JNI代码，省去了对c/c++程序的再封装。

#### 对比
1. 性能：JNA在性能上不如JNI。由于JNA是在JNI的基础上封装了一层。  
2. 移植性：JNA的可移植性要好于JNI，由于开发人员不须要再编写作为代理的动态链接库。  
3. 使用：JNI使用native关键字，使用一个个java方法映射原生方法，利用System.loadLibrary；JNA使用一个java借口来代表动态链接库。使用Native.loadLibrary  



#### 补充
dll：Windows平台动态库（Dynamic link library）
so：linux平台动态库（Shared Object）