### javah 命令，用于生成c\cpp头文件
```shell
# java 文件目录下针对.java文件编译
javah -classpath /usr/local/share com.xpalive.NativeMethod
```

### 查看帮助
```shell
javah -help
```
