### jni
让 JNI 工作起来所需要的一些关键组件如下：

Java 代码 - 我们的类，它将至少包含一种本地方法。
原生代码 - 我们原生代码的实际逻辑，通常使用 C 或者 C++ 代码。
JNI 头文件 - 这个 C/C++ 的头文件 (jni.h)，包括了我们可以在原生程序中使用的所有JNI 元素。
C/C++ 编译器 - 用于为我们的平台生成原生共享库。

代码中的 JNI 组件包括了 Java 和 C/C++ 代码。

Java 代码：
"native" 关键字 - 标记为 native 的方法都必须在原生共享库中实现。
System.loadLibrary(String libname) - 一种静态方法，用于将共享库从文件系统加载到内存中，并使其包含的函数可用于我们的 Java 代码。

C/C++ 代码：
JNIEXPORT - 将共享库中的函数标记为可导出，它将包含在函数表中，因此 JNI 可以找到它。
JNICALL - 与 JNIEXPORT 结合使用，确保我们的方法可用于 JNI 框架。
JNIEnv - 一个包含方法的结构，可以使用我们的原生代码访问 Java 元素。
JavaVM - 一种让我们可以操纵正在运行的 JVM（甚至启动一个新的 JVM）的结构，向它添加线程、销毁它等等。

### jni步骤参考
1. 编写java类
    ```java
    package com.kedacom;
    
    public class NativeCalcMethod {
        public native int add(int a, int b);
    
        public native int minus(int a, int b);
    
        public native int multiply(int a, int b);
    
        public native int divide(int a, int b);
    }
    ```
   
2. 通过javah命令生成头文件
    ```shell
    javah com.kedacom.NativeCalcMethod
    javah -o NativeCalcMethod.h com.kedacom.NativeCalcMethod  #指定生成文件 -o和-d只能用一个
    javah -d NativaMethod com.kedacom.NativeCalcMethod  #指定生成目录
    ```
    需要在包的根目录下执行javah命令
    如com.kedacom.NativeCalcMethod包名，则javah的命令路径则是com目录的上一级

3. 在 CLion 中设置
   在 CLion 中，你可以在 CMake 配置中设置环境变量：

   打开 File > Settings（Windows/Linux）或 CLion > Preferences（macOS）。
   导航到 Build, Execution, Deployment > CMake。
   在 Environment 字段中，添加 JAVA_HOME 变量，例如：
   ruby
   复制代码
   JAVA_HOME=/path/to/your/jdk

4. 设置CMakeLists.txt
    ```text
    cmake_minimum_required(VERSION 3.24)
    project(first)
    
    # 设置 C++ 标准
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED True)
    
    set(JAVA_HOME $ENV{JAVA_HOME})

    # 包含 JNI 头文件
    include_directories(${JAVA_HOME}/include)
    include_directories(${JAVA_HOME}/include/linux)
     
    # 查找 JVM 库
    find_library(JVM_LIBRARY jvm HINTS ${JAVA_HOME}/jre/lib/amd64/server)
     
    # 添加可执行文件
    add_executable(first MyJniProgram.cpp)
    
    # 添加源文件
    set(SOURCES com_kedacom_NativeCalcMethod.cpp
    )
    # 指定头文件目录
    include_directories(./)
    
    # 生成共享库
    add_library(MyLibrary SHARED ${SOURCES})
    
    # 设置库版本
    set_target_properties(MyLibrary PROPERTIES VERSION 1.0 SOVERSION 1)
     
    # 链接 JVM 库
    target_link_libraries(first ${JVM_LIBRARY})
    ```
5. 编译共享库
   ```shell
   mkdir build
   cd build
   cmake ..
   make
   ```

### 备注
#### System.loadLibrary 方法
System.loadLibrary 方法用于加载系统库路径中或通过 java.library.path 系统属性指定路径中的共享库 (.so 文件)。
然而，System.loadLibrary 只能接受库的名称，而不能包含路径分隔符。如果共享库在特定路径下，
则需要将该路径添加到 java.library.path 中。  
假设你的共享库名为 libMyLibrary.so，在 Java 代码中可以这样加载：
注意：在linux下共享库以lib开头，.so结尾
```text
static {
    System.loadLibrary("MyLibrary");  // 不要包含前缀 'lib' 和后缀 '.so'
}
```
设置 java.library.path
如果共享库不在系统默认的库路径中（例如 /usr/lib 或 /usr/local/lib），
则需要通过 java.library.path 属性指定库所在的目录。  

在运行 Java 程序时，通过 -Djava.library.path 参数指定库路径。
例如，如果共享库位于 libs 目录下，可以这样运行程序：  

```java
import java.io.File;

public class NativeCalcMethod {
    static {
        // 获取库的目录并设置 java.library.path
        String libPath = new File("libs").getAbsolutePath();
        System.setProperty("java.library.path", libPath);

        // 重新加载类加载器以应用新的 library path
        try {
            // This is necessary to apply the new library path
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to set library path", e);
        }

        // 加载共享库
        System.loadLibrary("MyLibrary");
    }

    public native int add(int a, int b);
    public native int minus(int a, int b);
    public native int multiply(int a, int b);
    public native int divide(int a, int b);

    public static void main(String[] args) {
        NativeCalcMethod calc = new NativeCalcMethod();
        System.out.println("Add: " + calc.add(10, 5));
        System.out.println("Minus: " + calc.minus(10, 5));
        System.out.println("Multiply: " + calc.multiply(10, 5));
        System.out.println("Divide: " + calc.divide(10, 5));
    }
}
```

#### System.load 方法
通过这种方式，你可以在 Java 应用程序中加载并使用资源目录中的共享库。

```java
package com.kedacom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

public class NativeCalcMethod {
    static {
        try {
            // 从资源目录中读取共享库
            String libName = "libMyLibrary.so";
            InputStream in = NativeCalcMethod.class.getResourceAsStream("/" + libName);
            if (in == null) {
                throw new RuntimeException("Cannot find " + libName);
            }

            // 创建临时文件
            File tempFile = Files.createTempFile("libMyLibrary", ".so").toFile();
            tempFile.deleteOnExit();

            // 将资源中的共享库写入临时文件
            try (OutputStream out = new FileOutputStream(tempFile)) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            }

            // 加载共享库
            System.load(tempFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to load native library", e);
        }
    }

    public native int add(int a, int b);
    public native int minus(int a, int b);
    public native int multiply(int a, int b);
    public native int divide(int a, int b);

    public static void main(String[] args) {
        NativeCalcMethod calc = new NativeCalcMethod();
        System.out.println("Add: " + calc.add(10, 5));
        System.out.println("Minus: " + calc.minus(10, 5));
        System.out.println("Multiply: " + calc.multiply(10, 5));
        System.out.println("Divide: " + calc.divide(10, 5));
    }
}

```

### 参考
https://blog.csdn.net/yaojingqingcheng/article/details/123497697
https://blog.csdn.net/weixin_45492007/article/details/123018918
https://blog.csdn.net/qq_22494029/article/details/80095448
https://blog.csdn.net/weixin_45914251/article/details/131051382
https://blog.csdn.net/weixin_45414277/article/details/109364327

### 代码示例
https://github.com/xpalive/sample/tree/master/native-interface
