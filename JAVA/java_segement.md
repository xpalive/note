* distinct
```java
public class Sample {
    public void sample() {
        Optional.ofNullable(dtoList).orElseGet(ArrayList::new)
        .stream().collect(Collectors.collectingAndThen(
            Collectors.toCollection(() -> new TreeSet<>(
                Comparator.comparing(
                    RuleXiChaiSolutionDTO::getDrugId))), ArrayList::new))
        .stream()
        .map(this::getOrSaveSolvent).collect(Collectors.toList());
    }
}
```
```java
public class Sample {
    public void sample() {
        
    }
}
```