### web socket
在我们springboot项目启动的时候，WebSocket已经被注入了这个DeviceService的Bean，
但是WebSocket的特点是:建立一个连接，会生成一个新的WebSocket对象，
分析源码，也可以理解:每个请求后，建立一个连接生成一个新的WebSocket，故这样是不能获得自动注入的对象了。
