### 安全管理器
> 当运行未知的Java程序的时候，该程序可能有恶意代码（删除系统文件、重启系统等），
为了防止运行恶意代码对系统产生影响，需要对运行的代码的权限进行控制，这时候就要启用Java安全管理器。

### 默认的配置文件
> 默认的安全管理器配置文件是 $JAVA_HOME/jre/lib/security/java.policy

### 启动方式
> -Djava.security.manager
> -Djava.security.manager -Djava.security.policy="E:/java.policy"

