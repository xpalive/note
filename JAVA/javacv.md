### org.bytedeco.javacpp.Loader#getCacheDir 创建文件目录

```java
class CodeFragment {
    
    public void codeFragment() {
        String[] dirNames = {System.getProperty("org.bytedeco.javacpp.cachedir"),
                System.getProperty("org.bytedeco.javacpp.cacheDir"),
                System.getProperty("user.home") + "/.javacpp/cache/",
                System.getProperty("java.io.tmpdir") + "/.javacpp-" + System.getProperty("user.name") + "/cache/"};
        for (String dirName : dirNames) {
            if (dirName != null) {
                File f = new File(dirName);
                try {
                    if ((f.exists() || f.mkdirs()) && f.canRead() && f.canWrite() && f.canExecute()) {
                        cacheDir = getCanonicalFile(f);
                        break;
                    }
                } catch (SecurityException e) {
                    logger.warn("Could not access " + f + ": " + e.getMessage());
                    // No access, try the next option.
                }
            }
        }
    }
}
```

```java
class CodeFragment {
    public void codeFragment(Class cls,String name){
        // 获取资源路径
        URL url = cls.getResource(name);
    }
}
```

### org.bytedeco.javacpp.Loader#cacheResource(java.net.URL, java.lang.String)

### org.bytedeco.javacpp.Loader#extractResource(java.net.URL, java.io.File, java.lang.String, java.lang.String, boolean)
拷贝 jar包中的文件   其中还有系统文件锁
### org.bytedeco.javacpp.Loader#findLibrary(java.lang.Class, org.bytedeco.javacpp.ClassProperties, java.lang.String, boolean)
获取 jar包文件的路径