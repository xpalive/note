```java
public class ClassLoader {
    protected Class<?> loadClass(String name, boolean resolve)
            throws ClassNotFoundException {
        synchronized (getClassLoadingLock(name)) {
            // First, check if the class has already been loaded
            // 首先去当前类加载器去获取加载过的类
            // 第一次是AppClassLoader
            // 第二次是ExtClassLoader
            Class<?> c = findLoadedClass(name);
            // 如果 类没有找到则判断当前类的parent是否为空
            if (c == null) {
                long t0 = System.nanoTime();
                try {
                    if (parent != null) {
                        // 不为空则进入parent 类加载器（ExtClassLoader）
                        c = parent.loadClass(name, false);
                    } else {
                        // ExtClassLoader类的parent为null，则进入bootstrapClassLoader去查类，如果依旧没有找到
                        c = findBootstrapClassOrNull(name);
                    }
                } catch (ClassNotFoundException e) {
                    // ClassNotFoundException thrown if class not found
                    // from the non-null parent class loader
                }

                if (c == null) {
                    // If still not found, then invoke findClass in order
                    // to find the class.
                    long t1 = System.nanoTime();
                    // 则在这里第一次抛出ClassNotFound异常，这里的异常会被递归中的catch捕获
                    c = findClass(name);

                    // this is the defining class loader; record the stats
                    sun.misc.PerfCounter.getParentDelegationTime().addTime(t1 - t0);
                    sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                    sun.misc.PerfCounter.getFindClasses().increment();
                }
            }
            if (resolve) {
                resolveClass(c);
            }
            return c;
        }
    }
}
```