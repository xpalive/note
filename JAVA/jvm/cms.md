### 年轻代
parNew
### 老年代 
cms（标记清除算法）
concurrent mark sweep gc

### 过程
初始标记（stw）
并发标记
重新标记（stw，修正并发标记产生的浮动垃圾）
并发清除（浮动垃圾产生）
并发重置

### 可调整的参数
老年代使用率的阈值  CMSInitiatingOccupancyFraction
标记整理的次数


### cms的三色标记


reference：
https://www.cnblogs.com/meixiaoyu/articles/16672179.html cms 优化
https://blog.csdn.net/luzhensmart/article/details/105876603 cms 优化
https://blog.csdn.net/aesop_wubo/article/details/38406709 cms 优化 对外内存回收
https://www.jianshu.com/p/007052ee3773  对外内存