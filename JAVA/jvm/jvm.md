java 1.8
```shell
-XX:+UnlockDiagnosticVMOptions # 诊断jvm配置项
-XX:+PrintFlagsFinal  # 打印jvm参数

-XX:PermSize # 元空间默认大小 v1.7
-XX:MaxPermGen # 元空间最大大小 v1.7
-XX:MetaspaceSize=128m # 元空间默认大小
-XX:MaxMetaspaceSize=128m # 元空间最大大小

java -XX:+UnlockDiagnosticVMOptions -XX:+PrintFlagsFinal -version
```

![img.png](img.png)

| minor GC |                    | Major GC                          |
| Eden     | S0 | S1            | Old Memory                        | Perm         |
|      -Xms -Xmx                                                    | -XX:PermSize |
|  -Xmn                         |                                   |
           | -XX:SurvivorRatio  | --XX:NewRatio = OldGen/Young Gen  |

### MetaSpace
![MateSpace.png](MateSpace.png)
Metaspace used 2425K, capacity 4498K, committed 4864K, reserved 1056768K
class space used 262K, capacity 386K, committed 512K, reserved 1048576K

metaspace 用于存放类对象及一些静态变量
used       当前虚拟机实际使用的内存空间
capacity   当前虚拟机使用的内存空间
committed  当前虚拟机申请的内存空间
reserved   操作系统给当前虚拟机保留的内存空间

### MetaSpace设置了最大值为256m为什么reserved的值会高于最大值
https://stackoverflow.com/questions/31075761/java-8-reserves-minimum-1g-for-metaspace-despite-maxmetaspacesize
https://docs.oracle.com/javase/8/docs/technotes/guides/vm/gctuning/considerations.html

### jinfo 命令

### jvm
-XX:+PrintGC 打印gc信息
-XX:+PrintGCDetail 打印gc信息

### jvm 配置
-XX:+UseG1GC         使用G1垃圾回收
SerialGC
  -XX:+UseSerialGC     就是Young区和old区都使用serial 垃圾回收算法
ParNewGC
  -XX:+UseParNewGC     设置年轻代为多线程收集。可与CMS收集同时使用
  -XX:ParallelGCThreads 限制线程数量
ParallelGC
  -XX:+UseParallelGC   选择垃圾收集器为并行收集器。此配置仅对年轻代有效,不能和CMS一起使用
CMS
  -XX:+UseConcMarkSweepGC   Old 区：使用CMS
  -XX:CMSInitiatingOccupancyFraction=75   这个值指定了老生代使用到成么程度后进行回收。
  -XX:+UseCMSInitiatingOccupancyOnly   如果没有使用此参数，那么HotSpot VM只是利用上述参数值来启动第一次CMS垃圾回收，后面都是使用HotSpot VM自动计算出来的值。
  -XX:+ UseCMSCompactAtFullCollection Full GC后，进行一次碎片整理；整理过程是独占的，会引起停顿时间变长
  -XX:+CMSFullGCsBeforeCompaction  设置进行几次Full GC后，进行一次碎片整理
  -XX:ParallelCMSThreads  设定CMS的线程数量
  see：https://blog.csdn.net/liubenlong007/article/details/88541589

-XX:+HeapDumpOnOutOfMemoryError                                                                                                                       
-XX:HeapDumpPath=/data/jvm/oom.hprof  
-XX:+PrintGC                     打印gc信息
-XX:+PrintGCDetails              打印gc
-XX:+PrintGCDateStamps                                                                                                                                
-XX:ErrorFile=/var/log/hs_err_pid<pid>.log  jvm故障日志文件
-Xloggc:/data/jvm/gcdetail.log   保存gc日志
-XX:+UseGCLogFileRotation        日志分割开启                                                                                                                      
-XX:NumberOfGCLogFiles=5         gc日志数量                                                                                                                     
-XX:GCLogFileSize=10M            gc日志大小                                                                                                                     
-XX:+PrintTenuringDistribution                                                                                                                        
-XX:SurvivorRatio=8              eden:s0:s1=8:1:1    survivor 永远占1                                                                                                                   
-XX:NewRatio=4                   new:old=1:4         new 永远占1                                                                                           
-javaagent:/apm-agent/skywalking-agent.jar                                                                                                            
-javaagent:/opt/exporter/jmx_prometheus_javaagent-0.3.2-SNAPSHOT.jar=9203:/opt/exporter/config.yaml

-XX:MaxRAMPercentage=75 最大内存百分比
-XX:InitialRAMPercentage=75  初始化内存百分比
-XX:MinRAMPercentage=75 最小内存百分比

-XX:+UseCompressedClassPointers     压缩指针
-XX:CompressedClassSpaceSize=1024m  压缩指针
-XX:NativeMemoryTracking=detail   堆外内存跟踪
### jvm 配置1
-Xms2048m 
-Xmx2048m 
-XX:MetaspaceSize=256m 
-XX:MaxMetaspaceSize=256m 
-XX:+PrintGCDetails 
-XX:NewRatio=2 
-XX:SurvivorRatio=4 
-Xloggc:/data/jvm/gcdetail.log

### jvm 配置2 parNew+cms
-Xms2048m  
-Xmx2048m  
-XX:+UseParNewGC  
-XX:+UseConcMarkSweepGC  
-XX:CMSInitiatingOccupancyFraction=75  
-XX:+UseCMSInitiatingOccupancyOnly  
-XX:CMSFullGCsBeforeCompaction=5  
-XX:MetaspaceSize=256m  
-XX:MaxMetaspaceSize=256m  
-XX:+PrintGCDetails  
-XX:NewRatio=1  
-XX:SurvivorRatio=8  
-Xloggc:/data/jvm/ar_gcdetail.log  

最小内存，最大内存，年轻代用parNewGc，老年代用cms，老年代占用75%开始fullgc，使用设置的参数
5次fullgc后整理一次碎片，元空间大小，打印gc日志，年轻代老年代比例，Eden和s0，s1比例

### jvm GC G1
-XX:G1HeapRegionSize=n  设置the size of a G1 region的大小，2的幂次方；范围1MB~32MB。目标是根据最小的java堆大小划分出约2048个区域  
-XX:MaxGCPauseMillis=200  设置最长暂停时间目标值，默认是200ms  
-XX:G1NewSizePercent=5  设置年轻代最小值所占总堆的百分比。默认值是堆的5%;建议20%如果太小会造成过多young GC，太大的话young GC时间过长  
-XX:G1MaxNewSizePercent=60  设置年轻代最大值所占总堆的百分比。默认值是Java堆的60%;默认60，建议75，可根据具体业务场景调整
-XX:+UnlockExperimentalVMOptions  解锁实验标记，打开后上面两项才会生效  
-XX:ParallelGCThreads=n  设置STW并行gc工作线程数的值。将n的值设置为逻辑处理器的数量。n的值与逻辑处理器的数量相同，最多为8。
                         如果逻辑逻辑处理器不止8个，则将n的值设置为逻辑处理器数的5/8左右。这适用于大多数情况，
                         触发是较大的SPARC系统,其中n的值可用是逻辑处理器数的5/16左右  
-XX:ConcGCThreads=n  并发标记借调，并发执行的线程数。将n设置为并行垃圾回收线程数（ParallelGCThreads）的1/4左右。                         
-XX:InitiatingHeapOccupancyPercent=45  设置触发全局并发标记周期的Java堆占用率是整个Java堆的45%  
-XX:G1MixedGCLiveThresholdPercent=65  old generation region中的存货对象的占比，只有占比小于此参数的Old region，才会被选入CSet
                                      数越大，获得对象越多，这个region可回收的就越少，gc效果就不明显，就优先不gc这些region
-XX:G1HeapWastePercent=10  设置您愿意浪费的堆百分比。如果可回收百分比小于堆废物百分比，Java HotSpot VM不会启动混合垃圾周期。默认值是10%
                           在global concurrent marking结束后，我们可以知道old gen regions中有多少空间要被回收，在每次YGC之后和再次
                           发生Mixed GC之前，会检查垃圾占比是否达到此参数，只有达到了，下次才会发生Mixed GC
-XX:G1MixedGCCountTarget=8  一次global concurrent marking之后，最多执行Mixed GC的次数  
-XX:G1OldCSetRegionThresholdPercent=10  一次Mixed GC中能被选入CSet的最多old generation region数量。默认值是Java堆的10%  
-XX:G1ReservePercent=10  设置作为限制空间的预留内存百分比，以降低目标空间溢出的风险。增减百分比时，请确保堆总的Java堆调整相同的量
-XX:+ParallelRefProcEnabled  使用多线程进行引用处理  

-XX:+PrintClassHistogram	gc日志参数，打印类信息  
-XX:+PrintTenuringDistribution	gc日志参数，打印对象年龄  
-XX:+PrintGCApplicationStoppedTime	打印应用STW时间，不仅是包括gc，还有jvm的一些操作    
-XX:+PrintGCTimeStamps	gc日志参数，打印gc具体发生时间  
-XX:+PrintAdaptiveSizePolicy	gc日志参数，打印自适应调整策略、gc原因    
-XX:+PrintReferenceGC	gc日志参数，打印引用处理细节  
-XX:-UseBiasedLocking	关闭偏向锁，偏向锁是synchronized优化，使用会增加stw时间	对时间敏感的服务建议关闭偏向锁  

### jvm G1 配置
-Xms8g
-Xmx8g
-XX:G1HeapRegionSize=16m
-XX:+UseG1GC
-XX:G1ReservePercent=25
-XX:InitiatingHeapOccupancyPercent=45
-XX:ParallelGCThreads=8
-XX:ConcGCThreads=4
-XX:+ParallelRefProcEnabled
-XX:+HeapDumpOnOutOfMemoryError
-Xloggc:/data/share/ksign-alg/ar_gcdetail.log
-XX:HeapDumpPath=/data/share/ksign-alg/oom.hprof
-XX:ErrorFile=/data/share/ksign-alg/java_error.log


### G1 日志解读
```text
2019-08-08T17:18:00.733+0800: 178504.896: [GC pause (G1 Evacuation Pause) (young)
Desired survivor size 301989888 bytes, new threshold 15 (max 15)
- age   1:    1385360 bytes,    1385360 total
- age   2:     389088 bytes,    1774448 total
- age   3:     455928 bytes,    2230376 total
- age   4:     620632 bytes,    2851008 total
- age   5:      27168 bytes,    2878176 total
- age   6:     394680 bytes,    3272856 total
- age   7:      12400 bytes,    3285256 total
- age   8:        608 bytes,    3285864 total
- age   9:     236688 bytes,    3522552 total
- age  10:        336 bytes,    3522888 total
- age  11:     489064 bytes,    4011952 total
- age  12:     316944 bytes,    4328896 total
- age  13:      12120 bytes,    4341016 total
- age  14:       2072 bytes,    4343088 total
- age  15:        512 bytes,    4343600 total
 178504.897: [G1Ergonomics (CSet Construction) start choosing CSet, _pending_cards: 6803, predicted base time: 11.03 ms, remaining time: 88.97 ms, target pause time: 100.00 ms]
 178504.897: [G1Ergonomics (CSet Construction) add young regions to CSet, eden: 1147 regions, survivors: 3 regions, predicted young region time: 12.85 ms]
 178504.897: [G1Ergonomics (CSet Construction) finish choosing CSet, eden: 1147 regions, survivors: 3 regions, old: 0 regions, predicted pause time: 23.88 ms, target pause time: 100.00 ms]
2019-08-08T17:18:00.745+0800: 178504.908: [SoftReference, 0 refs, 0.0015087 secs]2019-08-08T17:18:00.747+0800: 178504.910: [WeakReference, 0 refs, 0.0004373 secs]2019-08-08T17:18:00.747+0800: 178504.910: [FinalReference, 19154 refs, 0.0012160 secs]2019-08-08T17:18:00.748+0800: 178504.911: [PhantomReference, 0 refs, 168 refs, 0.0008367 secs]2019-08-08T17:18:00.749+0800: 178504.912: [JNI Weak Reference, 0.0000690 secs], 0.0187290 secs]
   [Parallel Time: 10.9 ms, GC Workers: 15]
      [GC Worker Start (ms): Min: 178504897.0, Avg: 178504897.1, Max: 178504897.3, Diff: 0.3]
      [Ext Root Scanning (ms): Min: 1.8, Avg: 2.0, Max: 2.6, Diff: 0.8, Sum: 30.6]
      [Update RS (ms): Min: 0.0, Avg: 0.6, Max: 0.7, Diff: 0.7, Sum: 9.4]
         [Processed Buffers: Min: 0, Avg: 13.4, Max: 26, Diff: 26, Sum: 201]
      [Scan RS (ms): Min: 0.1, Avg: 0.2, Max: 0.2, Diff: 0.1, Sum: 2.9]
      [Code Root Scanning (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.0]
      [Object Copy (ms): Min: 7.1, Avg: 7.2, Max: 7.2, Diff: 0.1, Sum: 108.0]
      [Termination (ms): Min: 0.4, Avg: 0.4, Max: 0.5, Diff: 0.1, Sum: 6.6]
         [Termination Attempts: Min: 257, Avg: 290.2, Max: 313, Diff: 56, Sum: 4353]
      [GC Worker Other (ms): Min: 0.0, Avg: 0.1, Max: 0.2, Diff: 0.2, Sum: 1.4]
      [GC Worker Total (ms): Min: 10.4, Avg: 10.6, Max: 10.8, Diff: 0.4, Sum: 159.0]
      [GC Worker End (ms): Min: 178504907.7, Avg: 178504907.7, Max: 178504907.8, Diff: 0.1]
   [Code Root Fixup: 0.0 ms]
   [Code Root Purge: 0.0 ms]
   [Clear CT: 0.4 ms]
   [Other: 7.3 ms]
      [Choose CSet: 0.0 ms]
      [Ref Proc: 4.3 ms]
      [Ref Enq: 0.2 ms]
      [Redirty Cards: 0.2 ms]
      [Humongous Register: 0.1 ms]
      [Humongous Reclaim: 0.0 ms]
      [Free CSet: 1.1 ms]
   [Eden: 4588.0M(4588.0M)->0.0B(4596.0M) Survivors: 12.0M->8192.0K Heap: 7347.2M(8192.0M)->2758.0M(8192.0M)]
 [Times: user=0.19 sys=0.00, real=0.02 secs]
```


* [GC pause (G1 Evacuation Pause) (young)：GC 的原因是 YGC
* Desired survivor size 301989888 bytes, new threshold 15 (max 15)：surivivor 区大小为 301989888 字节，年龄超过 15 的对象才进入老年代 age 
1: 1385360 bytes, 1385360 total：survivor 区各个年龄带对象的大小
* [G1Ergonomics (CSet Construction) start choosing CSet, _pending_cards: 6803, predicted base time: 11.03 ms, 
remaining time: 88.97 ms, target pause time: 100.00 ms]：会根据目标停顿时间动态选择部分垃圾对并多的 Region 回收，这一步就是选择 Region。_pending_cards 是关于 RSet 的 Card Table。predicted base time 是预测的扫描 card table 时间。
* [G1Ergonomics (CSet Construction) add young regions to CSet, eden: 1147 regions, survivors: 3 regions, predicted 
young region time: 12.85 ms]：这一步是添加 Region 到 collection set
* [G1Ergonomics (CSet Construction) finish choosing CSet, eden: 1147 regions, survivors: 3 regions, old: 0 regions, 
predicted pause time: 23.88 ms, target pause time: 100.00 ms]：这一步是对上面两步的总结。预计总收集时间 23.88ms
* [SoftReference, 0 refs, 0.0015087 secs] 2019-08-08T17:18:00.747+0800: 178504.910: [WeakReference, 0 refs, 0.0004373 
secs] 2019-08-08T17:18:00.747+0800: 178504.910: [FinalReference, 19154 refs, 0.0012160 secs] 2019-08-08T17:18:00.748+0800: 178504.911: [PhantomReference, 0 refs, 168 refs, 0.0008367 secs] 2019-08-08T17:18:00.749+0800: 178504.912: [JNI Weak Reference, 0.0000690 secs], 0.0187290 secs]：各种引用的统计
* [Parallel Time: 10.9 ms, GC Workers: 15] 使用 15 个线程并行进行，总共耗时 10.9ms
* [GC Worker Start (ms): Min: 178504897.0, Avg: 178504897.1, Max: 178504897.3, Diff: 0.3]：收集线程开始的时间，使用的是相对时间，Min 
是最早开始时间，Avg 是平均开始时间，Max 是最晚开始时间，Diff 是 Max-Min（下同）
* [Ext Root Scanning (ms): Min: 1.8, Avg: 2.0, Max: 2.6, Diff: 0.8, Sum: 30.6]：gc root 扫描耗时
* [Update RS (ms): Min: 0.0, Avg: 0.6, Max: 0.7, Diff: 0.7, Sum: 9.4]：更新 Remembered Set 上的时间
* [Processed Buffers: Min: 0, Avg: 13.4, Max: 26, Diff: 26, Sum: 201]：处理 buffer 记录消耗的时间
* [Scan RS (ms): Min: 0.1, Avg: 0.2, Max: 0.2, Diff: 0.1, Sum: 2.9]：扫描 CS 中的 region 对应的 RSet，因为 RSet 是 
points-into，所以这样实现避免了扫描 old generadion region，但是会产生 float garbage
* [Code Root Scanning (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.0]：扫描 code root 耗时。code root 指的是经过 JIT 
编译后的代码里，引用了 heap 中的对象。引用关系保存在 RSet 中。
* [Object Copy (ms): Min: 7.1, Avg: 7.2, Max: 7.2, Diff: 0.1, Sum: 108.0]：拷贝活的对象到新 region 的耗时
* [Termination (ms): Min: 0.4, Avg: 0.4, Max: 0.5, Diff: 0.1, Sum: 6.6]：线程结束，在结束前，它会检查其他线程是否还有未扫描完的引用，如果有，则” 偷” 
过来，完成后再申请结束，这个时间是线程之前互相同步所花费的时间
* [GC Worker Other (ms): Min: 0.0, Avg: 0.1, Max: 0.2, Diff: 0.2, Sum: 1.4]：花费在其他工作上的时间
* [GC Worker Total (ms): Min: 10.4, Avg: 10.6, Max: 10.8, Diff: 0.4, Sum: 159.0]：每个线程花费的时间和
* [GC Worker End (ms): Min: 178504907.7, Avg: 178504907.7, Max: 178504907.8, Diff: 0.1]：每个线程结束的时间
* [Code Root Fixup: 0.0 ms]：用来将 code root 修正到正确的 evacuate 之后的对象位置所花费的时间
* [Code Root Purge: 0.0 ms]：清除 code root 的耗时，code root 中的引用已经失效，不再指向 Region 中的对象，所以需要被清除
* [Clear CT: 0.4 ms]：清除 card table 的耗时
* [Other: 7.3 ms]：其他事项共耗时，其他事项包括选择 CSet，处理已用对象，引用入 ReferenceQueues，大对象的注册、声明，释放 CSet 中的 region 到 free list
* [Eden: 4588.0M (4588.0M)->0.0B (4596.0M) Survivors: 12.0M->8192.0K Heap: 7347.2M (8192.0M)→2758.0M (8192.0M)]：年轻代被清空，大小调整到 4596M


### linux系统报错日志
/var/log/messages

```shell
egrep -i 'killed process' /var/log/messages # 查询日志内容
dmesg | grep java  # 通过dmesg查看
```

### jvm挂掉怎样排查原因
先翻dump文件，dump如果没有，翻hs_err_pid.log日志。如果还没有，翻内核日志。

### jvm什么时候加载类
当使用对应类首次被使用的时候才去加载（静态方法静态属性被访问，类被实例化，类被反射调用）
静态类会直接在jvm启动时加载