### springboot logback
```xml
<?xml version="1.0" encoding="utf-8" ?>
<!--
说明：
    1. 文件的命名和加载顺序有关
       logback.xml早于application.yml加载，logback-spring.xml晚于application.yml加载
       如果logback配置需要使用application.yml中的属性，需要命名为logback-spring.xml
    2. logback使用application.yml中的属性
       使用springProperty才可使用application.yml中的值 可以设置默认值
-->

<!-- 从高到地低 OFF 、 FATAL 、 ERROR 、 WARN 、 INFO 、 DEBUG 、 TRACE 、 ALL -->
<!-- 日志输出规则  根据当前ROOT 级别，日志输出时，级别高于root默认的级别时  会输出 -->
<!-- 以下  每个配置的 filter 是过滤掉输出文件里面，会出现高级别文件，依然出现低级别的日志信息，通过filter 过滤只记录本级别的日志-->

<!-- 属性描述 scan：性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true scanPeriod:设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，
默认单位是毫秒。当scan为true时，此属性生效。默认的时间间隔为1分钟。debug:当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。 -->
<configuration scan="true" scanPeriod="60 seconds" debug="false">
    <springProperty scope="context" name="logName" source="spring.application.name" defaultValue="xxx"/>
    <!-- 定义日志文件 输入位置 -->
    <property name="logPath" value="./java-starter/${logName}/log"/>
    <!-- 日志最大的历史 30天 -->
    <property name="maxHistory" value="30"/>

    <!-- 配置项， 通过此节点配置日志输出位置（控制台、文件、数据库）、输出格式等-->
    <!-- ConsoleAppender代表输出到控制台 -->
    <appender name="consoleLog" class="ch.qos.logback.core.ConsoleAppender">
        <!-- layout代表输出格式 -->
        <layout class="ch.qos.logback.classic.PatternLayout">
            <pattern>%date %level [%thread] %logger [%file:%line] %msg%n</pattern>
        </layout>
    </appender>
    <!-- 日志输出文件 -->
    <appender name="fileInfoLog" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder>
            <pattern>%date %level [%thread] %logger [%file:%line] %msg%n</pattern>
        </encoder>
        <!-- 滚动记录文件，先将日志记录到指定文件，当符合某个条件时，将日志记录到其他文件 RollingFileAppender-->
        <!-- 滚动策略，它根据时间来制定滚动策略.既负责滚动也负责触发滚动 -->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 输出路径 -->
            <fileNamePattern>${logPath}/info/%d.log</fileNamePattern>
            <!-- 可选节点，控制保留的归档文件的最大数量，超出数量就删除旧文件假设设置每个月滚动，且<maxHistory>是6，
            则只保存最近6个月的文件，删除之前的旧文件。注意，删除旧文件是，那些为了归档而创建的目录也会被删除-->
            <maxHistory>${maxHistory}</maxHistory>
        </rollingPolicy>
<!--        <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">-->
<!--            <layout class="com.geostar.common.log.logback.LogLayout" />-->
<!--        </encoder>-->
        <!-- 按照固定窗口模式生成日志文件，当文件大于20MB时，生成新的日志文件。窗口大小是1到3，当保存了3个归档文件后，将覆盖最早的日志。
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
          <fileNamePattern>${logPath}/%d{yyyy-MM-dd}/.log.zip</fileNamePattern>
          <minIndex>1</minIndex>
          <maxIndex>3</maxIndex>
        </rollingPolicy>   -->
        <!-- 查看当前活动文件的大小，如果超过指定大小会告知RollingFileAppender 触发当前活动文件滚动
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>5MB</maxFileSize>
        </triggeringPolicy>   -->
    </appender>
    <!-- 特殊记录Error日志 -->
    <appender name="fileErrorLog" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!-- 只记录ERROR级别日志，添加范围过滤，可以将该类型的日志特殊记录到某个位置 -->
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>ERROR</level>
        </filter>
        <encoder>
            <pattern>%date %level [%thread] %logger [%file:%line] %msg%n</pattern>
        </encoder>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${logPath}/error/%d.log</fileNamePattern>
            <!-- 日志最大的历史 60天 -->
            <maxHistory>60</maxHistory>
        </rollingPolicy>
<!--        <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">-->
<!--            <layout class="com.geostar.common.log.logback.LogLayout" />-->
<!--        </encoder>-->
    </appender>

<!--    &lt;!&ndash;日志异步到数据库 &ndash;&gt;-->
<!--    &lt;!&ndash; 数据库日志记录 &ndash;&gt;-->
<!--    <appender name="DB_APPENDER" class="com.geostar.usersystem.log.LogDBAppender">-->
<!--        　　-->
<!--        <filter class="com.geostar.usersystem.log.LogbackMarkerFilter">-->
<!--            &lt;!&ndash; 自定义标志 &ndash;&gt;-->
<!--            　　　-->
<!--            <marker>DB</marker>-->
<!--            　　　-->
<!--            <onMatch>ACCEPT</onMatch>-->
<!--            　　　-->
<!--            <onMismatch>DENY</onMismatch>-->
<!--        </filter>-->
<!--        &lt;!&ndash; 配置数据源 springboot默认情况会开启光连接池 &ndash;&gt;-->
<!--        <connectionSource class="ch.qos.logback.core.db.DriverManagerConnectionSource">-->
<!--            <driverClass>com.mysql.cj.jdbc.Driver</driverClass>-->
<!--            <url>jdbc:mysql://127.0.0.1:3306/ceshi?useSSL=false&amp;useUnicode=true&amp;characterEncoding=UTF-8&amp;serverTimezone=UTC</url>-->
<!--            <user>root</user>-->
<!--            <password>123456</password>-->
<!--        </connectionSource>-->
<!--    </appender>-->
    <!-- 异步输出 -->
    <appender name="ASYNC-INFO" class="ch.qos.logback.classic.AsyncAppender">
        <!-- 不丢失日志.默认的,如果队列的80%已满,则会丢弃TRACT、DEBUG、INFO级别的日志 -->
        <discardingThreshold>0</discardingThreshold>
        <!-- 更改默认的队列的深度,该值会影响性能.默认值为256 -->
        <queueSize>256</queueSize>
        <!-- 添加附加的appender,最多只能添加一个 -->
        <appender-ref ref="fileInfoLog"/>
    </appender>
    <appender name="ASYNC-ERROR" class="ch.qos.logback.classic.AsyncAppender">
        <!-- 不丢失日志.默认的,如果队列的80%已满,则会丢弃TRACT、DEBUG、INFO级别的日志 -->
        <discardingThreshold>0</discardingThreshold>
        <!-- 更改默认的队列的深度,该值会影响性能.默认值为256 -->
        <queueSize>256</queueSize>
        <!-- 添加附加的appender,最多只能添加一个 -->
        <appender-ref ref="fileErrorLog"/>
    </appender>

    <!-- 根节点，表名基本的日志级别，里面可以由多个appender规则 -->
    <!-- level="info"代表基础日志级别为info -->
    <root level="info">
        <!-- 引入控制台输出规则 -->
        <appender-ref ref="consoleLog"/>

        <appender-ref ref="ASYNC-INFO" />
        <appender-ref ref="ASYNC-ERROR" />
    </root>

    <!-- 不同包，设置不同的日志级别 -->
    <logger name="com.cn.geostar.md.harvest" level="debug"/>
    <logger name="java.sql.PreparedStatement" level="info"/>
</configuration>
```

#### 配置文件


scan：性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true  
scanPeriod:设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位是毫秒。当scan为true时，此属性生效。默认的时间间隔为1分钟。
debug:当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。  




### 参考
#### 异步日志
AsyncAppender 配置

https://blog.csdn.net/loophome/article/details/103254677
https://logback.qos.ch/manual/appenders.html#AsyncAppender
#### logback 格式
https://www.cnblogs.com/sxdcgaq8080/p/7886251.html
#### logback 组件
https://blog.csdn.net/xiaolegeaizy/article/details/108324837