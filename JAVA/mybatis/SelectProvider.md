### @SelectProvider 自定义sql 构造器
@SelectProvider(type=xxxx.class,method=”xxxx”)
> 自定义的SQL语句提供者
> type：指定获取SQL语句的类
> method：类中获取SQL语句的方法

```text
@SelectProvider(type=SqlProvider.class,method="selectAll")
public User selectAll(@Param(value="id") Integer id);

public class SqlProvider{
    public String selectAll(Map<String,Object> param){
        return new SQL(){{
            SELECT("*");
            FROM("USER")
            WHERE("ID="+param.get("id"))    
        }}.toString();
    }
    
    public static String getSqlOfSelectList(@Param("tableName") String tableName, @Param("columns") String columns,
                                            @Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize) {
        return new SQL()
                .SELECT(columns)
                .FROM(tableName)
                .WHERE("migrate_flag = " + MigrateFlagEnum.NO_MIGRATE.getCode())
                .ORDER_BY("id ASC")
                .LIMIT(pageSize)
                .OFFSET((pageNo - 1) * pageSize)
                .toString();
    }
}
```

