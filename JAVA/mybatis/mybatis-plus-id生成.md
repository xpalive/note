## mybatis自定义id生成器
### mybatis-plus-boot-starter:3.5.1
在自动配置类：com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration
配置了IdentifierGenerator的Bean
注：@Configuration(proxyBeanMethods = false) 其中proxyBeanMethods属性需要spring 5.2版本

```java
// 配置自定义ID生成器
@Configuration
public class MybatisPlusConfig {

    @Bean
    public IdentifierGenerator identifierGenerator() {
        // 配置自定义 ID 生成器
        return new CustomIdGenerator();
    }
}

public class CustomIdGenerator implements IdentifierGenerator {

    @Override
    public Number nextId(Object entity) {
        return StringUtils.unionKey();
    }
}
```

###