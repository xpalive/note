### mybatis plus 方法注入
https://gitee.com/baomidou/mybatis-plus-samples/tree/master/mybatis-plus-sample-sql-injector

### 实现自定义类型转化
@MappedTypes
BaseTypeHandler

### 结果集映射
org.apache.ibatis.executor.resultset.DefaultResultSetHandler#handleResultSets

### 类型处理handler缓存池
ResultSetWrapper.typeHandlerMap
org.apache.ibatis.executor.resultset.ResultSetWrapper#getTypeHandler
TypeHandlerRegistry

### Configuration 配置类
Configuration
protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry(); //注册类型转换器

### 缓存对象信息，使用lambda表达式（具体异常忘记了）
TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), MigrateFlagEntity.class);