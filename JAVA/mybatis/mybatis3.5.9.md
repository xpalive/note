### 自定义mybatis的ResultMapper
1. 在对应的Mapper.xml文件中定义<resultMap>
2. 定义result
> <result column="position" jdbcType="OTHER" property="position" javaType="com.xpalive.dto.PGPoint3D" 
   typeHandler="com.xpalive.dao.handler.JsonbTypeHandler"/>

3. 定义对应的实体类，将对应的resultMap的id设置到resultMap中 
> @TableName(value = "tableName",resultMap = "com.xpalive.dao.ArSceneDao.BaseResultMapMyBatis")
      public class ArSceneDto implements Serializable {...}
> resultMap -> namespace+resultMapId