```java
package com.kedacom.net;

import cn.hutool.core.thread.NamedThreadFactory;
import cn.hutool.json.JSONUtil;
import com.kedacom.enums.MediaStatus;
import com.kedacom.net.clientHandler.ClientInitializer;
import com.kedacom.net.config.AlgorithmServerConfig;
import com.kedacom.net.helper.AlgorithmClientSession;
import com.kedacom.net.helper.AlgorithmClientSessionManager;
import com.kedacom.net.helper.MessageWrapper;
import com.kedacom.net.helper.NettyHelper;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
public class AlgorithmSocketClient {
    
    private final Logger logger = LoggerFactory.getLogger(AlgorithmClientSessionManager.class);
    
    /**
     * <pre>
     * 这里的socket连接主要是将码流信息推送给算法服务 因为这里的线程主要是为了建立socket连接，所以1-2个线程就够了；
     * 如果出现高并发的情况，且堆积严重，则启动8个临时线程加速连接的创建
     * </pre>
     */
    private final ExecutorService threadPool = new ThreadPoolExecutor(1, 10, 30, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(512), new NamedThreadFactory("Establish-Connect-", false),
            new ThreadPoolExecutor.DiscardOldestPolicy());
    
    private final String host;
    
    private final int port;
    
    @Autowired
    private AlgorithmClientSessionManager manager;
    
    private EventLoopGroup group;
    
    private Bootstrap boot;
    
    @Autowired
    public AlgorithmSocketClient(AlgorithmServerConfig config) {
        this.host = config.getHost();
        this.port = config.getPort();
    }
    
    @PostConstruct
    public void init() {
        group = new NioEventLoopGroup();
        boot = new Bootstrap();
        boot.group(group).channel(NioSocketChannel.class).handler(new ClientInitializer(manager));
        
    }
    

    
    /**
     * @param deviceId 统一媒体平台的资源ID
     */
    public void connect(String deviceId, Supplier<MessageWrapper> supplier, Consumer<MediaStatus> consumer) {
        CompletableFuture.runAsync(() -> {
            try {
                if (consumer != null) {
                    consumer.accept(MediaStatus.CONNECTING);
                }
                ChannelFuture connectFuture = boot.connect(host, port);
                connectFuture.addListener(future -> {
                    if (future.isSuccess()) {
                        if (consumer != null) {
                            consumer.accept(MediaStatus.CONNECTED);
                            logger.info("deviceId:{},连接完成", deviceId);
                        }
                        Channel ch = connectFuture.channel();
                        manager.online(deviceId, ch);
                        if (supplier != null && supplier.get() != null) {
                            sendMsg(deviceId, supplier.get());
                            if (consumer != null) {
                                consumer.accept(MediaStatus.PUSHING);
                            }
                        }
                        ChannelFuture closeFuture = ch.closeFuture();
                        closeFuture.addListener(getCloseFutureListener(deviceId, consumer));
                    } else {
                        consumer.accept(MediaStatus.DIS_CONNECT);
                        Throwable cause = future.cause();
                        if (cause != null) {
                            logger.error("连接失败，" + cause.getMessage(), cause);
                        }
                    }
                });
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                consumer.accept(MediaStatus.DIS_CONNECT);
            }
        }, threadPool);
    }
    
    private ChannelFutureListener getCloseFutureListener(String deviceId,
            Consumer<MediaStatus> consumer) {
        return f -> {
            if (f.isSuccess()) {
                logger.info("deviceId:{},连接断开", deviceId);
                if (consumer != null) {
                    consumer.accept(MediaStatus.DIS_CONNECT);
                }
            } else {
                Throwable cause = f.cause();
                if (cause != null) {
                    logger.error(cause.getMessage(), cause);
                }
            }
        };
    }
    
    private Channel connectTransform() {
        try {
            ChannelFuture connectFuture = boot.connect(host, port).sync();
            logger.info("相机transform传输通道连接成功");
            Channel ch = connectFuture.channel();
            ChannelFuture closeFuture = ch.closeFuture();
            closeFuture.addListener(f -> {
                if (f.isSuccess()) {
                    logger.info("相机transform传输通道关闭");
                } else {
                    logger.info("相机transform传输通道关闭失败");
                    Throwable cause = f.cause();
                    if (cause != null) {
                        logger.error(cause.getMessage(), cause);
                    }
                }
            });
            return ch;
        } catch (InterruptedException e) {
            logger.info("相机transform传输通道连接失败");
        }
        return null;
    }
    
    public Channel getTransformChannel() {
        Channel transformChannel = manager.getTransformChannel();
        if (transformChannel == null || !transformChannel.isActive()) {
            Channel channel = connectTransform();
            manager.setTransformChannel(channel);
            return channel;
        } else {
            return transformChannel;
        }
    }
    
    public boolean isConnected(String deviceId) {
        return manager.isConnected(deviceId);
    }
    
    public void sendMsg(String deviceId, MessageWrapper wrap) {
        AlgorithmClientSession session = manager.getSessionByDeviceId(deviceId);
        Channel channel = session.getChannel();
        if (logger.isDebugEnabled()) {
            logger.debug("deviceId:{},发送数据:{}", deviceId, JSONUtil.toJsonStr(wrap));
        }
        NettyHelper.sendMessage(channel, wrap);
    }
    
    /**
     *
     */
    @PreDestroy
    public void shutdownGracefully() {
        Future<?> future = group.shutdownGracefully();
        try {
            future.sync();
        } catch (InterruptedException e) {
        }
    }
    
    @PreDestroy
    public void shutdownThreadPool() {
        try {
            threadPool.shutdown();
        } catch (Exception e) {
        }
    }
    
}

```