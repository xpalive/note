### 在使用Netty中LengthFieldBasedFrameDecoder的时候出现了异常导致无法触发
方式一：通过替换掉原来的ChannelHandler来处理
不确定有什么风险，比如是否存在内存泄露等问题
```java
public class Java{
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof TooLongFrameException) {
            ChannelPipeline pipeline = ctx.pipeline();
            LengthFieldBasedFrameDecoder decoder = (LengthFieldBasedFrameDecoder) pipeline.get(
                    "lengthFieldBasedFrameDecoder");
            if (decoder != null) {
                pipeline.replace("lengthFieldBasedFrameDecoder", "lengthFieldBasedFrameDecoder",
                        new LengthFieldBasedFrameDecoder(1024, 0, 4, 0, 4));
                System.out.println("exceptionCaught");
            }
        } else {
            super.exceptionCaught(ctx, cause);
        }
    }
    
}
```
方式二：获取到ChannelHandler对象来重置相关状态，需要看源码