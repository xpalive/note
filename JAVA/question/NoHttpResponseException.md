### NoHttpResponseException异常
#### 现象
使用restTemplate频繁发送http请求导致抛出NoHttpResponseException异常

#### 分析
可能是由于restTemplate服用http连接导致的
1. 情况一：由于上一次的http服务响应后，上一次服务器响应的FIN包或RST包还未收到，就紧接着进行了下一次服务请求
2. 情况二：由于服务端的keepalive时间与客户端的keepalive时间不一致，导致客户端在发送消息时，服务端正好发送了FIN包或RST包

#### 解决
1. 发送http请求直接添加时间间隔
2. 添加重试机制，捕获NoHttpResponseException异常，重新发起http请求，需要创建新的http连接
3. 主动检测http连接，当http的空闲时长大于服务端的空闲时长时主动断开连接

#### task
复现一下这个异常并解决