### 异常代码
```
Caused by: javax.websocket.DeploymentException: Cannot deploy POJO class [com.xpalive.service.ws.WebSocket$$EnhancerBySpringCGLIB$$7792c1b8] as it is not annotated with @ServerEndpoint 
  at org.apache.tomcat.websocket.server.WsServerContainer.addEndpoint(WsServerContainer.java:245)
  at org.apache.tomcat.websocket.server.WsServerContainer.addEndpoint(WsServerContainer.java:228)
  at org.springframework.web.socket.server.standard.ServerEndpointExporter.registerEndpoint(ServerEndpointExporter.java:156)
```
> 被代理的类不允许被@ServerEndpoint注解
> 也就是说Spring的webSocket类不可以被代理

解决：
1. 需要检测当前类是否被切面代理
2. 查看spring.factories中是否有第三方框架提供了切面方法
3. 检测是否添加了@Configuration，@Async，@Service等注解导致Spring生成了代理类