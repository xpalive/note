### IDEA远程调试 remote_debug
1. 选择 -> edit Configurations
![img.png](img.png)
2. 选择 -> Add New Configuration
![img_1.png](img_1.png)
3. 选择 -> Remote JVM Debug
4. 选择对应的jdk版本，设置ide参数
![img_2.png](img_2.png)
   1. 1.3 版本之前的参数： -Xnoagent -Djava.compiler=NONE -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005
   2. 1.4 版本的参数： -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005
   3. 5-8 版本： -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
   4. 9 版本之后： -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005
5. 服务端启动脚本
   linux
   > nohup java \
   > -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=50055 \
   > -jar remote-debug-0.0.1-SNAPSHOT.jar &

   window
   > java ^
   > -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=50055 ^
   > -jar remote-debug-0.0.1-SNAPSHOT.jar
   
   端口需与ide中的remote端口一致

6. 启动服务、再启动idea 