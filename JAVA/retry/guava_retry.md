```xml
<dependency>
            <groupId>com.github.rholder</groupId>
            <artifactId>guava-retrying</artifactId>
            <version>2.0.0</version>
        </dependency>
```
```java
import com.github.rholder.retry.*;
import java.util.concurrent.TimeUnit;

public class RetryExample {
    public static void main(String[] args) {
        Retryer<Object> retryer = RetryerBuilder.newBuilder()
            .retryIfException() // 遇到异常时重试
            .retryIfResult(result -> result == null) // 结果为 null 时重试
            //.withWaitStrategy(WaitStrategies.fixedWait(1,  TimeUnit.SECONDS)) // 固定时长
            //.withWaitStrategy(WaitStrategies.fibonacciWait(1, 15, TimeUnit.SECONDS)) // 斐波那契
            .withWaitStrategy(WaitStrategies.exponentialWait(1, 30, TimeUnit.SECONDS)) // 指数级等待时间 第一个参数是毫秒，
            .withStopStrategy(StopStrategies.stopAfterAttempt(5)) // 最多重试 5 次
            .build();

        try {
            retryer.call(() -> {
                long startTime = System.currentTimeMillis();
                System.out.println("Attempting operation...");
                
                // 模拟操作和时间计算
                performOperation();
                
                long endTime = System.currentTimeMillis();
                System.out.println("Elapsed Time: " + (endTime - startTime) + " ms");
                
                // 模拟返回 null 的情况
                return null;
            });
        } catch (Exception e) {
            System.err.println("Operation failed after retries: " + e.getMessage());
        }
    }

    private static void performOperation() {
        // Simulate some work here
        try {
            Thread.sleep(100); // Simulate work delay
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

```