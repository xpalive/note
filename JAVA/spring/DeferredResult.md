`DeferredResult` 是 Spring 框架中提供的一种处理异步请求的机制。它允许你在处理长时间运行的请求时，
不会阻塞服务器的工作线程，从而提高应用的性能和响应能力。

### 什么是 DeferredResult
`DeferredResult` 允许你在控制器方法中返回一个 DeferredResult 对象，而不是直接返回一个视图或者数据。
这个 DeferredResult 对象可以在稍后由另一个线程完成并设置结果。一旦结果被设置，响应就会发送回客户端。