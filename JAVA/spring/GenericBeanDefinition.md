```scss
// 创建一个 GenericBeanDefinition 对象
GenericBeanDefinition personDef = new GenericBeanDefinition();

// 指定 Bean 的 class
personDef.setBeanClass(Person.class);

// 指定构造函数参数
ConstructorArgumentValues constructorArgs = new ConstructorArgumentValues();
constructorArgs.addIndexedArgumentValue(0, "John"); // 第一个参数为 name
constructorArgs.addIndexedArgumentValue(1, 30);     // 第二个参数为 age
personDef.setConstructorArgumentValues(constructorArgs);

// 指定属性值
MutablePropertyValues propertyValues = new MutablePropertyValues();
propertyValues.add("name", "John");
propertyValues.add("age", 30);
personDef.setPropertyValues(propertyValues);

// 将 BeanDefinition 注册到 BeanFactory 中
beanFactory.registerBeanDefinition("person", personDef);
```