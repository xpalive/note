```java
public class DefaultStreamOperations {
    @Override
    public MapRecord<K, HK, HV> deserializeRecord(ByteRecord record) {
        // redis key 使用的时keySerializer
        // redis map 中的key 使用的时hashKeySerializer
        // redis map 中的value 使用的时hashValueSerializer
        return record.deserialize(keySerializer(), hashKeySerializer(), hashValueSerializer());
    }
}
```

```java
public class DefaultListOperations{
    // 通过 keySerializer将key序列化为byte发送到redis中
    @Override
    public V index(K key, long index) {
        //使用 valueSerializer将value序列化为对象
        //此处为命令模式
        return execute(new ValueDeserializingRedisCallback(key) {
            @Override
            protected byte[] inRedis(byte[] rawKey, RedisConnection connection) {
                return connection.lIndex(rawKey, index);
            }
        });
    }
}

```
rawKey             -> keySerializer()
rawValues          -> valueSerializer()

keySerializer()             序列化redis中的key
valueSerializer()           序列化redis中的值
hashKeySerializer()         序列化值中的key
hashValueSerializer()       序列化值中的值

DefaultSetOperations              keySerializer(), valueSerializer()
DefaultStreamOperations           keySerializer(), hashKeySerializer(), hashValueSerializer()
DefaultHashOperations             keySerializer(), hashKeySerializer(), hashValueSerializer()
DefaultValueOperations            keySerializer(), valueSerializer()
DefaultGeoOperations              keySerializer(), valueSerializer()
DefaultClusterOperations          对集群操作
DefaultListOperations             keySerializer(), valueSerializer()
DefaultZSetOperations             keySerializer(), valueSerializer()
DefaultHyperLogLogOperations      keySerializer(), valueSerializer()