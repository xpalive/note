### spring websocket

> // 首先要注入ServerEndpointExporter，这个bean会自动注册使用了@ServerEndpoint注解声明的Websocket endpoint。
> // 要注意，如果使用独立的servlet容器，而不是直接使用springboot的内置容器，就不要注入ServerEndpointExporter，因为它将由容器自己提供和管理。

```java
public class Config {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
```

### session发送消息是加上同步锁可以保证频繁发消息不挂掉

```java
public class WebSocket {
    
    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        //加同步锁，解决多线程下发送消息异常关闭
        synchronized (this.session) {
            this.session.getBasicRemote().sendText(message);
        }
    }
}
```

### ServerEndpointConfig.Configurator 重写
```java
@Component
public class BasedSpringConfigurator extends ServerEndpointConfig.Configurator
        implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
    
    // 重新该方法会使得WebSocket使用同一个对象，但是通信的Session会不同
    // 解答，如果使用同一个对象的webSocket，那么成员变量就会公用，会有线程安全问题
    // 如果是不同的webSocket对象，那么就不会有线程安全问题
    // 静态变量都会有线程安全问题
    @Override
    public <T> T getEndpointInstance(Class<T> clazz) throws InstantiationException {
        return this.applicationContext.getBean(clazz);
    }
    
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        super.modifyHandshake(sec, request, response);
    }
}
```
### ws获取ip地址
```java
public class Ws{
    @OnOpen
    public void open(Session session) {
        // 获取远程ip
        RemoteEndpoint.Async asyncRemote = session.getAsyncRemote();
        // 通过反射拿到asyncRemote中封装的ip地址 asyncRemote#base#socketWrapper#socket#sc#remoteAddress
        // WsRemoteEndpointAsync#WsRemoteEndpointImplServer#NioEndpoint$NioSocketWrapper#NioChannel#SocketChannelImpl#InetSocketAddress
    }
}
```

### sample
xp-spring-boot-ws
