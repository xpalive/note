### spring aop

aop 是在初始化后进行的
1. 通过`父类`AbstractAutoProxyCreator类中的postProcessAfterInitialization作为创建代理对象的入口
   1. 真正创建代理对象的方法为createProxy
2. 在@EnableAspectJAutoProxy注解中实际上导入的类为`AnnotationAwareAspectJAutoProxyCreator`beanName为`org.springframework.aop.config.internalAutoProxyCreator`，aop与事务设置的beanName都是internalAutoProxyCreate，且设置了升级顺序
   1. InfrastructureAdvisorAutoProxyCreator (transaction)
   2. AspectJAwareAdvisorAutoProxyCreator （aop aspectj）
   3. AnnotationAwareAspectJAutoProxyCreator （aop aspectj 解析aspectj的注解）
3. 但是他们的父类都是AbstractAutoProxyCreator，且构造代理对象都是由父类中的方法完成
4. 具体的代理逻辑就由层层的advice(MethodInterceptor)进行提供

#### BeanNameAutoProxyCreator
PostProcessor实例化前
BeanNameAutoProxyCreator.postProcessBeforeInstantiation
```
BeanNameAutoProxyCreator creator = new BeanNameAutoProxyCreator();
creator.setBeanNames("userSe*"); 根据名字匹配
creator.setInterceptorNames("aroundAdvice");
```

#### DefaultAdvisorAutoProxyCreator
1. NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();
    pointcut.addMethodName("test");

2. BeforeAdvice advice = new BeforeAdvice();

3. DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
    advisor.setPointcut(pointcut);
    advisor.setAdvice(advice)

4. DefaultAdvisorAutoProxyCreator proxyCreator = new DefaultAdvisorAutoProxyCreator();
 -> @Import(DefaultAdvisorAutoProxyCreator.class)

#### AnnotationAwareAspectJAutoProxyCreator
通过该@EnableAspectJAutoProxy支持
其原理：
1. 导入 AspectJAutoProxyRegistrar.class 类，这个类实现了 ImportBeanDefinitionRegistrar 接口
2. 通过 ImportBeanDefinitionRegistrar接口的注册方法registerBeanDefinitions
   1. 获取注解上属性值
   2. 注册AnnotationAwareAspectJAutoProxyCreator.class的beanDefinition
   3. AopConfigUtils类中定义了AnnotationAwareAspectJAutoProxyCreator.
      class的BeanDefinition的beanName为AUTO_PROXY_CREATOR_BEAN_NAME常量
3. 注册AnnotationAwareAspectJAutoProxyCreator 会对aspectj的注解进行解析比如@Aspect，@Pointcut, @Around, @Before, 
   @After, @AfterReturning, @AfterThrowing


### 注
1. spring aop中主要是由 Advice和PointCut 来完成的，通过Advice 和PointCut 组成一个Advisor来完成代码增强
2. advisor在spring生命周期中创建的时机
   1. 实例化前AbstractAutoProxyCreator的postProcessBeforeInstantiation方法中
   2. 在真正需要生成代理的逻辑中创建advisor对象，并缓存，这是主流的逻辑线
3. 通过AnnotationAwareAspectJAutoProxyCreator