### aop & transaction
虽然 aop和transaction的后置处理会被覆盖
升级顺序
1. InfrastructureAdvisorAutoProxyCreator (transaction)
2. AspectJAwareAdvisorAutoProxyCreator （aop aspectj）
3. AnnotationAwareAspectJAutoProxyCreator （aop aspectj 解析aspectj的注解）
但是他们还是需要使用对应的注解来开启对应的能力
@EnableTransactionManagement 开启事务
@EnableAspectJAutoProxy 开启aspectj注解支持的aop
4. 因为事务还需要 ProxyTransactionManagementConfiguration的支持
5. 而aspectj需要 AnnotationAwareAspectJAutoProxyCreator 来对 aspectJ的注解进行解析支持