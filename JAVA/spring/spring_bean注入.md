### spring @Autowired @Resource区别

#### @Autowired

1. 由spring提供的注解
2. 默认通过byType的方式找到Bean，如果找到多个则报错
3. 可以通过@Qualifier进行类名指定
4. 后置处理器AutowiredAnnotationBeanPostProcessor

#### @Resource

1. 由javax提供的注解
2. 默认通过ByName的方式找到Bean，如果找到多个则报错
3. 可以通过属性值type来指定通过类型找Bean
4. 后置处理器是CommonAnnotationBeanPostProcessor