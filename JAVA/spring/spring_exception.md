### 注入报错
```text
Description:

The bean 'gpsTrackDao' could not be injected because it is a JDK dynamic proxy

The bean is of type 'com.sun.proxy.$Proxy129' and implements:
	com.kedacom.ksigndao.dao.GpsTrackDao

Expected a bean of type 'com.kedacom.ksigncommon.service.IEsGpsTrackService' which implements:
```
原因： @Autowired 与@Resource 寻找bean的与注入bean的区别
@Autowired 找Bean的优先级为先找到声明类型的Bean是否存在，如果只有一个返回
如果有多个则再根据name过滤Bean

@Resource 找Bean的优先级为先根据变量名去找Bean是否存在，如果不存在则根据
声明的类型去寻找Bean


### 循环依赖报错
Error creating bean with name 'ar3dItemController': Injection of resource dependencies failed; nested exception is org.springframework.beans.factory.BeanCurrentlyInCreationException: Error creating bean with name 'ar3dItemServiceImpl': Bean with name 'ar3dItemServiceImpl' has been injected into other beans [esCommonService] in its raw version as part of a circular reference, but has eventually been wrapped. This means that said other beans do not use the final version of the bean. This is often the result of over-eager type matching - consider using 'getBeanNamesForType' with the 'allowEagerInit' flag turned off, for example.
Error creating bean with name 'ar3dItemServiceImpl': Bean with name 'ar3dItemServiceImpl' has been injected into other beans [esCommonService] in its raw version as part of a circular reference, but has eventually been wrapped. This means that said other beans do not use the final version of the bean. This is often the result of over-eager type matching - consider using 'getBeanNamesForType' with the 'allowEagerInit' flag turned off, for example.
