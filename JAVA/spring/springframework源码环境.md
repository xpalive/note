### spring framework 5.3.10
1. 导入spring源码后，可以修改setting -> build -> build tool -> gradle 
    build and run using -> intellij idea  
    run test using -> intellij idea   
    这样可以不用跑一大批的gradle task  
    > 在.gitignore 文件中加入 对于模块的构建后目录的忽略 如：/xp-*/build
   > 添加check style 忽略 src/checkstyle/checkstyle-suppressions.xml 加入忽略如下  
   > ```xml
   >   <!-- xp-alive 可以避免构建的时候报style错误-->
   >  <suppress files="src[\\/]main[\\/]java[\\/]com[\\/]xiongping[\\/].*" checks=".*" />
   > ```    

2. 如果出现错误就通过gradle build 一下子模块
3. 如果还是找不到可以通过Build 进行 rebuild

4. kotlin 版本为 1.5

5. run debug 可能会出现异常 kotlin插件bug
    ```shell
    Exception in thread "main" java.lang.NoClassDefFoundError: kotlin/Result
    AgentPremain
    ```
    禁用kotlin插件可以解决 setting -> build -> debug -> data views -> kotlin
    
6. 切面.aj文件    
    安装AspectJ插件 下载地址 https://www.jetbrains.com/help/idea/aspectj.html
    以下步骤待验证
    安装aspectj-1.9.7.jar 下载地址 https://github.com/eclipse/org.aspectj/releases
        Add c:\aspectj1.9\lib\aspectjrt.jar to your CLASSPATH
        Modify your PATH to include c:\aspectj1.9\bin

7.   


https://www.cnblogs.com/qubo520/p/13264036.html?utm_source=tuicool
https://blog.51cto.com/u_15269008/4846349#1CoroutinesUtils_102