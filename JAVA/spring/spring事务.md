### 事务


1. 第一种：使用启动事务的语句，这种是显式的启动事务。比如 begin 或 start transaction 语句。与之配套的提交语句是 commit，回滚语句是 rollback。
2. 第二种：autocommit 的值默认是 1，含义是事务的自动提交是开启的。如果我们执行 set autocommit=0，这个命令会将这个线程的自动提交关掉。意味着如果你只执行一个 select 语句，这个事务就启动了，而且并不会自动提交。这个事务持续存在直到你主动执行 commit 或 rollback 语句，或者断开连接。

> begin/start transaction 命令并不是一个事务的起点，在执行到它们之后的第一个操作 InnoDB 表的语句，事务才算是真正启动

> 如果你想要马上启动一个事务，可以使用 start transaction with consistent snapshot 这个命令。需要注意的是这个命令在读已提交的隔离级别（RC）下是没意义的，和直接使用 start transaction 一个效果。

> 一个方法的事务开始是在第一句执行表操作的时候开始的
> 查询数据库当前事务：select * from information_schema.innodb_trx;

> https://mp.weixin.qq.com/s/LcqmB0DSNk-WqMMlxK8b5A

### spring 事务
spring事务和aop类似也是是在初始化后进行的
1. 通过`父类`AbstractAutoProxyCreator类中的postProcessAfterInitialization作为创建代理对象的入口
   1. 真正创建代理对象的方法为createProxy
2. 在@EnableTransactionManagement注解中实际上导入的类为`InfrastructureAdvisorAutoProxyCreator`beanName为`org.springframework.aop.config.internalAutoProxyCreator`，如果开启了aop，则这个类会被aop的类覆盖升级顺序
   1. InfrastructureAdvisorAutoProxyCreator (transaction)
   2. AspectJAwareAdvisorAutoProxyCreator （aop aspectj）
   3. AnnotationAwareAspectJAutoProxyCreator （aop aspectj 解析aspectj的注解）
3. 具体的代理逻辑就由层层的advice(MethodInterceptor)进行提供

---------------

spring事务是通过spring的aop实现的
通过代理对象来完成事务的管理,这里
然后调用自己写的数据库操作逻辑
最后在代理对象中完成数据库的提交或回滚操作
具体来说
1. 首先通过aop来判断spring的Bean是否需要创建代理对象来处理数据库事务
   1. 通过advice的ClassFilter和MethodMatch来判断对象是否需要生成代理对象
2. 在代理逻辑中首先通过spring的事务管理器（transactionManage）来创建数据库连接，并关闭自动提交
3. 设置传播机制，保存当前的数据库连接到当前线程的threadLocal中
4. 如果有新创建事务的需求，则在执行新事物时，将老的数据库连接暂时挂起，并将新的数据库连接设置到threadLocal中
5. 在执行完新的数据库操作后，将事务提交并将挂起的数据库连接重新设置到当前线程的threadLocal中
6. 最后将数据库连接进行提交

### spring 事务源码分析
@EnableTransactionManagement
导入了类：TransactionManagementConfigurationSelector这个类是一个ImportSelector
通过ImportSelector又导入了2个类：
AutoProxyRegistrar：（如果注入的是ImportBeanDefinitionRegistrar类型的bean，在registrar的registerBeanDefinitions方法中，可以获取@EnableTransactionManagement的注释的类上的所有的注解的注释信息）
ProxyTransactionManagementConfiguration中
TransactionAttributeSource 负责解析事务注解，及过滤类（是否需要事务）
TransactionAttributeSourcePointcut 负责过滤需要进行事务管理的类或方法
TransactionInterceptor类负责处理事务逻辑

```sql
show status like '%Threads_connected%'
```
### 传播机制
1. propagation.REQUIRE_NEW
2. Propagation.NESTED   ->   savepoint

TransactionAspectSupport


### 判断事务生效情况
首先要判断是否是一个事务
如果是同一个事务中，那么事务要么是都回滚，要么是都提交
如果是是在事务中嵌套事务，那么就可以存在一个事务成功，一个事务失败的情况


