### spring 异步
AsyncConfigurationSelector 导入类
默认是 proxy 模式
解析  @EnableAsync 的配置信息
导入的类为 ProxyAsyncConfiguration 对应的配置类的bean 且是 full类型的
在ProxyAsyncConfiguration中通过@Bean生成后置处理器 AsyncAnnotationBeanPostProcessor实例
这个实例在初始化的过程中通过setBeanFactory来添加AsyncAnnotationAdvisor

* 创建代理对象 proxyFactory.getProxy
`AbstractAdvisingBeanPostProcessor`
    |
`AbstractBeanFactoryAwareAdvisingPostProcessor`
    |
`AsyncAnnotationBeanPostProcessor`

在初始化bean的时候，会注册`AsyncAnnotationBeanPostProcessor`,通过初始化后的方法对bean进行代理逻辑处理，成代理对象

* 创建代理逻辑 Advice
代理逻辑则是 AnnotationAsyncExecutionInterceptor 提供的
`AsyncExecutionInterceptor`
    |
`AnnotationAsyncExecutionInterceptor`