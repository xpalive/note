### spring 属性注入循环依赖解决
1. spring中的属性注入的循环依赖是通过三个Map进行解决的
    > Map(earlySingletonObjects、singletonFactories、singletonObjects)

https://blog.csdn.net/qq271859852/article/details/105181422
### spring 构造方法循环依赖解决
1. spring中的构造方法注入依赖的循环是通过@Lazy进行解决的
    > org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory#createBeanInstance
    
### bean的生命周期
填充属性 -> 初始化(initializeBean)
           aware回调 -> 初始化前 -> 初始化 -> 初始化后

### 循环依赖 org.springframework.beans.factory.support.AbstractBeanFactory#doGetBean
1. 单例缓存
   org.springframework.beans.factory.support.DefaultSingletonBeanRegistry#getSingleton(String beanName, ObjectFactory<?> singletonFactory)
   在以上方法中将Bean缓存到单例池中

2. 单例工厂缓存
   org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory#doCreateBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
   在以上方法中 通过判断 创建的bean是单例Bean 并且允许循环依赖，且bean是在创建中的
   那么就将创建bean的 factory 接口函数缓存到 第三级缓存中
   addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));

3. 早期单例缓存
   org.springframework.beans.factory.support.DefaultSingletonBeanRegistry#getSingleton(String beanName, boolean allowEarlyReference)
   调用单例工厂,并移除,在调用ObjectFactory.getObject的时候还会检测是否需要创建代理对象
   保存早期的单例Bean

![img.png](image/circular_relationship.png)
