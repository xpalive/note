### @Autowired @Resource 区别
@Autowired 是先根据类型再根据名字去找Bean
@Resource 是现根据名字，再根据类型去找Bean

### @Lazy 工作方式

### @Configuration

### @AutoConfigureBefore

### @ConditionalOnClass

### @ConditionalMissingBean

### Advisor
DefaultAdvisorAutoProxyCreator

### 处理
AutowiredAnnotationBeanPostProcessor -> SmartInstantiationAwareBeanPostProcessor
在bean初始化完成后再进行处理


### @Import
ImportSelect


### ImportBeanDefinitionRegistrar
registerBeanDefinitions 

### Transaction 事务
AutoProxyRegistrar 处理BeanDefinition的注册
ProxyTransactionManagementConfiguration 处理事务逻辑

### InfrastructureAdvisorAutoProxyCreator -> AbstractAdvisorAutoProxyCreator


### AbstractAutoProxyCreator -> SmartInstantiationAwareBeanPostProcessor



