### spring 配置文件
> 在启动配置中心的时候，由于配置中心里面的配置，比如git配置等等，在不同的环境中是不一样的，因此需要在外部指定，
刚开始选择使用spring.config.location来指定外部配置文件，如：
`-Dspring.config.location=C:\\Users\\XB\Desktop\\config\\config-test.yml`
结果config-test.yml这个文件直接取代了我项目本地的application.yml文件，导致有些本地配置中独有的配置也没有被加载进来。

> 使用spring.config.additional-location替代spring.config.location
`-Dspring.config.additional-location=C:\\Users\\XB\Desktop\\config\\config-test.yml`

### 配置文件加载顺序
bootstrap.yml > logback.xml > application.yml

https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.external-config