`@ControllerAdvice` 是 Spring 提供的一个用于全局异常处理的机制。当你使用 `@ControllerAdvice` 定义了全局异常处理类时，它会拦截并处理应用程序中的异常，返回你定义的错误响应。这意味着，任何由 `@ControllerAdvice` 捕获的异常都不会再经过默认的错误处理机制，比如 Tomcat 的 `StandardWrapperValve`。

### 了解 `StandardWrapperValve`

`StandardWrapperValve` 是 Tomcat 内部处理请求和异常的一个组件。它的主要作用是处理 HTTP 请求和响应，并在请求处理过程中遇到异常时进行处理。具体来说，它负责调用 Servlet 容器内的处理逻辑并管理请求的生命周期。

在没有自定义异常处理的情况下，Tomcat 会处理 Servlet 容器中的错误，并将错误信息传递给客户端。然而，当你在 Spring Boot 中使用 `@ControllerAdvice` 来处理异常时，Spring 的异常处理机制会接管错误处理流程。

### 为什么 `@ControllerAdvice` 处理异常后不会经过 `StandardWrapperValve`

1. **Spring 的异常处理机制**: 使用 `@ControllerAdvice` 定义的异常处理逻辑会在 Spring 的控制器方法中捕获异常。这意味着，Spring 的异常处理机制已经接管了错误处理，`@ControllerAdvice` 会在 Spring 的层面上处理异常并生成响应。

2. **Servlet 容器的处理**: 当 Spring 处理了异常并生成了响应，Servlet 容器（如 Tomcat）不会再干预这些异常处理流程。因为从 Servlet 容器的角度来看，Spring 已经处理了请求和异常，它不会再继续处理异常或调用 `StandardWrapperValve`。

3. **异常的传播**: 如果 Spring Boot 的 `@ControllerAdvice` 捕获了异常并返回了响应，异常处理流程就在 Spring 的层面完成了。因此，Tomcat 的 `StandardWrapperValve` 逻辑不会再被调用，因为它的责任是处理尚未被应用层处理的异常。

### 示例

假设你在 `@ControllerAdvice` 中定义了一个异常处理方法：

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        // 记录异常
        logger.error("An error occurred: ", ex);

        // 返回自定义的错误响应
        ErrorResponse errorResponse = new ErrorResponse("Internal Server Error", ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
```

在这个示例中，当发生异常时，`@ControllerAdvice` 会捕获这个异常并返回一个自定义的错误响应。由于异常已经被 `@ControllerAdvice` 处理，Tomcat 的 `StandardWrapperValve` 就不会再处理这个异常。

### 解决方案和建议

- **自定义错误处理**: 如果你需要在自定义的 `@ControllerAdvice` 之外进行特定的错误处理，可以考虑通过自定义 `ErrorController` 实现来处理特定的 HTTP 错误页面。

- **日志记录**: 确保在 `@ControllerAdvice` 中适当记录异常信息，以便调试和排查问题。

- **测试和验证**: 在开发过程中，确保通过测试验证异常处理逻辑是否按照预期工作，避免遗漏错误处理。

### 总结

当你使用 `@ControllerAdvice` 进行全局异常处理时，它会拦截并处理异常，导致默认的错误处理机制（如 Tomcat 的 `StandardWrapperValve`）不会再处理这些异常。Spring 的异常处理机制在处理异常时会覆盖 Servlet 容器的默认行为。