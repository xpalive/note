### 版本说明
https://github.com/alibaba/spring-cloud-alibaba/wiki/版本说明

#### 版本
1. spring boot 2.6.12  
2. spring cloud 2021.0.5  
3. spring cloud alibaba 2021.0.5.0  
4. nacos 2.2.0  
5. sentinel 1.8.6  
6. spring cloud gateway 3.1.4  
#### 遇到的问题
问题1：
```text
No spring.config.import property has been defined

Action:

Add a spring.config.import=nacos: property to your configuration.
	If configuration is not required add spring.config.import=optional:nacos: instead.
	To disable this check, set spring.cloud.nacos.config.import-check.enabled=false.
```
添加依赖包
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bootstrap</artifactId>
</dependency>
```
问题2：nacos配置文件发布应用无法更新
应该将spring.application.name=nacos-provider配置在bootstrap.yml的配置文件中


配置对应的请求bean
```java
@Configuration
public class RestTemplateConfig {
    @Bean
    @LoadBalanced 
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

问题4：Client not connected, current status:STARTING
引入了错误的包spring-cloud-starter-netflix-ribbon 导致报错
spring cloud 2020.0.0 版本移除了netflix-ribbon的支持，转而使用spring-cloud-loadbalancer
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
    <version>2.2.10.RELEASE</version>
</dependency>
```
添加loadbalancer包
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-loadbalancer</artifactId>
</dependency>
```

问题5：bootstrap.yml的解析需要spring-cloud-starter-bootstrap包的支持
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bootstrap</artifactId>
</dependency>
```

问题6：nacos自动注册需要有serviceId，否则无法注册
```text
NacosServiceRegistryAutoConfiguration 自动注册类，创建所需要的bean
NacosServiceRegistry  用于nacos注册服务的类
NacosRegistration  注册服务需要的属性
NacosAutoServiceRegistration spring cloud的AbstractAutoServiceRegistration的子类，用于接入到spring的生命周期中
AbstractAutoServiceRegistration在spring-cloud-common中
```
详见
```java
public class NacosServiceRegistry implements ServiceRegistry<Registration> {
    public void register(Registration registration) {
        if (StringUtils.isEmpty(registration.getServiceId())) {
            log.warn("No service to register for nacos client...");
        }
    }
}
```

