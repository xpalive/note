### 线程安全问题
如果是多个线程操作一个对象的时候，涉及到属性，那么就需要考虑线程安全问题。

### 线程数量设置
cpu密集型： cpu核数+1
IO密集型： 2cpu核数+1

`最佳线程数 = cpu 核数 * [1 + (I/O耗时 / cpu耗时)]`

### 中断线程
https://www.jianshu.com/p/c6a83dc45658

### 线程池
```text
ThreadPoolExecutor executor = 
    new ThreadPoolExecutor(16, 64, 10, TimeUnit.SECONDS, 
        new ArrayBlockingQueue<>(1024), new NameTreadFactory());

重写 线程工厂可以看到线程创建过程,这样写有点简陋
static class NameTreadFactory implements ThreadFactory {
        private final AtomicInteger mThreadNum = new AtomicInteger(1);
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "my-thread-" + mThreadNum.getAndIncrement());
            log.info(t.getName() + " has been created");
            return t;
        }
    }
```

```java
public class Demo{
    private int coreNum = Runtime.getRuntime().availableProcessors();
    private int poolSize = coreNum * 2;
    public void demo(){
        ThreadPoolExecutor executor = new ThreadPoolExecutor(poolSize, poolSize, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(1024), new NameTreadFactory());
    }
}
```

### 关闭线程池
```java
public class Demo{
    public void demo(){
        ThreadPoolExecutor executor = new ThreadPoolExecutor(poolSize, poolSize, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(1024), new NameTreadFactory());
        // 关闭线程池（通过Thread.interrupt()方法来实现），线程池中未完成的任务继续执行，队列中的任务也会执行，但不能添加新任务
        executor.shutdow();
        // 关闭线程池（通过Thread.interrupt()方法来实现），线程池中未完成的任务继续，如果线程没有sleep,wait,Condition,定时锁等,
        // interrupt方法是无法中断的，忽略队列中的任务，执行同时终止
        executor.shutdowNow(); 
        // 
        executor.awaitTermination(10, TimeUnit.SECONDS); //阻塞状态，并返回线程当前中断状态
    }
}
```

https://blog.csdn.net/weixin_43227523/article/details/118854093  线程池核心线程数的设置
https://www.cnblogs.com/jpfss/p/11016169.html  计算线程池最佳线程数

