### window 下java版本切换
VJava.bat
```shell
@echo off
java -version
pause
```
CJava.bat
```shell
@echo off
@echo ------------------------------------------------
@echo 当前Java版本为:
call TJava.bat
@echo ------------------------------------------------
@echo 输入要使用的java版本对应的选项:
@echo 选项   含义
@echo 8      切换环境为JDK8
@echo 9      切换环境为JDK9
@echo 11     切换环境为JDK11
@echo ------------------------------------------------
set /P choose=请输入选择:
IF "%choose%" EQU "8" (
    REM 修改JAVA_HOME环境变量为%JAVA_HOME_8%,
    setx "JAVA_HOME" "%JAVA_HOME_8%" /m
    echo 已经修改 "JAVA_HOME" 为 %JAVA_HOME_8%
) ELSE IF "%choose%" EQU "9" (
    setx "JAVA_HOME" "%JAVA_HOME_9%" /m
    echo 已经修改 "JAVA_HOME" 为 %JAVA_HOME_9%
REM setx "Path" "%cd%;%path%" /m
) ELSE IF "%choose%" EQU "11" (
    setx "JAVA_HOME" "%JAVA_HOME_11%" /m
    echo 已经修改 "JAVA_HOME" 为 %JAVA_HOME_11%
)
pause
```