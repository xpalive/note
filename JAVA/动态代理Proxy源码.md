### java动态代理
通过 java.lang.reflect.Proxy的newInstance方法创建代理对象

通过java.lang.reflect.Proxy.getProxyClass0获取代理类

再通过获取到的代理类的构造方法，通过反射进行代理类的实例化

#### 如何获取到代理类呢
在proxyClassCache中获取，如果获取不到则创建
这个proxyClassCache对象实例化方法：proxyClassCache = new WeakCache<>(new KeyFactory(), new ProxyClassFactory());
终点关注 keyFactory()和ProxyClassFactory()
在ProxyClassFactory类中的apply方法中
byte[] proxyClassFile = ProxyGenerator.generateProxyClass(
proxyName, interfaces, accessFlags); 通过ProxyGenerator进行代理类的生成
