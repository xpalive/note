### nvm
官方网址：https://github.com/nvm-sh/nvm
1. 执行安装脚本
```shell
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash
```
2. 配置环境变量
在 ~/.bashrc 中添加环境变量
```shell
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

3. 通过1中的脚本安装，会自动将环境变量配置好


### node 安装稳定版本的node并使用
nvm install --lts
nvm use --lts

### vite
npm list -g vite  检查vite
npm install -g vite  全局安装vite

npm list --save-dev vite  检查vite
npm install vite --save-dev  项目中使用vite

