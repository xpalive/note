### nodejs
node -v 查看node版本
npm -v 查看npm版本
### nvm配置环境变量
NVM_HOME      : C:\Users\xiongping\AppData\Roaming\nvm
NVM_SYMLINK   : C:\Program Files\nodejs


### 启动服务
打开项目dist目录
http-server

### nvm 使用
nvm ls 查看版本
nvm use 使用指定版本

### npm 使用
npm cache verify 清楚cache
npm install 安装依赖
安装http-server
npm i http-server -g

### vscode 
安装vue插件 vuter

### node切换源
切换源
npm config set registry https://registry.npm.taobao.org --global
验证
npm config get registry

### nvm安装
https://github.com/coreybutler/nvm-windows/releases
1. nvm 安装，注意选择nodejs的link目录及安装目录
2. 添加配置项，设置国内源
   1. node_mirror: https://npm.taobao.org/mirrors/node/
   2. npm_mirror: https://npm.taobao.org/mirrors/npm/
3. 配置环境变量
   1. NVM_HOME  (nvm安装路径)
   2. NVM_SYMLINK  (nodejs映射路径)
4. 相关查看命令
   1. nvm -v 查看当前版本
   2. nvm --config
   3. nvm list 查看已安装node版本列表
      1. nvm list available 查看可以装的node版本列表
      2. nvm ls-remote
   4. nvm install 版本号 下载对应node版本
   5. nvm use 版本号 切换node版本
   6. nvm on 开启nvm
   7. nvm off 关闭nvm
   8. nvm
5. 使用node前需要先使用nvm use 命令

### node 版本
1. node常用版本
   1. 16.12.0
   2. 12.8.0
2. node命令
   1. node -v 查看当前node版本




      
      
      
      
      
      