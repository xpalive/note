在 `npm` 中，**不加标志**、`-g` 和 `--save-dev` 代表了不同的安装方式，分别针对包的安装位置和用途。以下是三者的区别：

### 1. **不加标志（默认安装）**

命令：`npm install <package>`

- **安装位置**：将包安装到**当前项目**的 `node_modules` 目录。
- **记录位置**：包会被添加到项目的 `package.json` 文件的 `dependencies` 部分。
- **用途**：默认情况下，包被认为是项目在生产环境下运行时需要的依赖。
- **适用场景**：安装项目运行时所必需的库，例如 `express`、`react` 等。

**示例**：
```bash
npm install lodash
```
`lodash` 会安装到当前项目的 `node_modules` 目录下，并且会被记录在 `package.json` 的 `dependencies` 中。

---

### 2. **全局安装（`-g`）**

命令：`npm install -g <package>`

- **安装位置**：将包安装到系统的**全局环境**，而不是项目的 `node_modules` 目录。
- **记录位置**：包**不会**被记录在项目的 `package.json` 中。
- **用途**：全局安装的包可以在任何地方通过命令行使用，适合安装工具或 CLI（命令行工具），例如 `npm`、`eslint`、`vite` 等。
- **适用场景**：你需要在多个项目或系统范围内使用该工具，而不仅限于一个项目。

**示例**：
```bash
npm install -g vite
```
`vite` 会安装到系统的全局环境，你可以在任何地方运行 `vite` 命令。

---

### 3. **开发依赖（`--save-dev`）**

命令：`npm install --save-dev <package>`

- **安装位置**：将包安装到当前项目的 `node_modules` 目录中。
- **记录位置**：包会被记录在 `package.json` 文件的 `devDependencies` 部分。
- **用途**：`--save-dev` 表示该包是**开发时**需要的依赖，而不是生产环境中运行时所需的依赖。
- **适用场景**：用于安装开发工具，如编译器、测试框架、代码检查工具等。这些工具在生产环境中不需要使用，例如 `webpack`、`eslint`、`jest`。

**示例**：
```bash
npm install eslint --save-dev
```
`eslint` 会安装到项目的 `node_modules` 目录，并记录在 `package.json` 的 `devDependencies` 中，只在开发时使用。

---

### 总结

| 选项              | 安装位置                    | 记录位置                          | 适用场景                                |
|-------------------|-----------------------------|-----------------------------------|----------------------------------------|
| **不加标志**      | 当前项目的 `node_modules`    | `package.json` 的 `dependencies`  | 生产环境和运行时所需的依赖             |
| **`-g`（全局安装）** | 系统的全局环境              | 不记录在 `package.json`           | 系统范围使用的 CLI 工具或命令行工具    |
| **`--save-dev`**  | 当前项目的 `node_modules`    | `package.json` 的 `devDependencies` | 开发环境所需的依赖，如测试工具、打包工具 |

### 选择依据
- **不加标志**：用于项目运行时的依赖。
- **`-g`**：用于全局工具，不与特定项目绑定。
- **`--save-dev`**：用于开发时的工具或库，生产环境中不需要。