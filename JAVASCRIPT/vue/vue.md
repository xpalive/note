1. 安装npm npm install
2. 安装依赖
3. 查看package.json
```json lines
{
  "scripts": {
    "dev": "vue-cli-service serve",
    "build:prod": "vue-cli-service build",
    "build:stage": "vue-cli-service build --mode staging",
    "preview": "node build/index.js --preview",
    "test:unit": "jest --clearCache && vue-cli-service test:unit",
    "test:ci": "npm run lint && npm run test:unit",
    "svgo": "svgo -f src/icons/svg --config=src/icons/svgo.yml"
  }
}
```
1. 启动 npm run dev
### vue3初始化项目
npm create vue@latest
安装 eslint
安装 prettier
npm install
npm run format
npm run dev