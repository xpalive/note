### env
1. 添加配置文件
.env.dev
```text
NODE_ENV='develop'
VITE_PUBLIC_PATH='http://192.168.10.121:8080'
```
.env.prod
```text
NODE_ENV='production'
VITE_PUBLIC_PATH='/'
```
2. package.json
添加 --mode dev
添加 --mode prod
"dev" "vite --host" 网络暴露
```text
  "scripts": {
    "dev": "vite --mode dev --host",
    "build": "vite build --mode prod",
    "preview": "vite preview",
    "lint": "eslint . --ext .vue,.js,.jsx,.cjs,.mjs --fix --ignore-path .gitignore",
    "format": "prettier --write src/"
  }
```
3. 设置axios
axios.defaults.baseURL = import.meta.env.VITE_PUBLIC_PATH;