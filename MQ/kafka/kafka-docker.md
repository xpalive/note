### docker image 选择
1. wurstmeister/kafka 镜像相对比较轻量，适合单机开发和测试使用，而 bitnami/kafka 镜像功能更加全面，包含了更多的 Kafka 工具和组件，并且可以在生产环境中使用。
2. 因此，如果是在开发和测试环境中使用 Kafka，可以选择 wurstmeister/kafka 镜像，而如果是在生产环境中使用 Kafka，则建议选择 bitnami/kafka 镜像。