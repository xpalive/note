### kafka
在Kafka中，conf文件夹中的文件包括以下文件：

connect-distributed.properties：这个文件是Kafka Connect分布式模式的配置文件。
connect-standalone.properties：这个文件是Kafka Connect独立模式的配置文件。
consumer.properties：这个文件是Kafka消费者的配置文件。
producer.properties：这个文件是Kafka生产者的配置文件。
server.properties：这个文件是Kafka Broker的配置文件。
这些文件的作用分别是：

connect-distributed.properties：配置Kafka Connect分布式模式运行时所需的参数，比如Kafka Broker的地址，Kafka Connect的工作目录等等。
connect-standalone.properties：配置Kafka Connect独立模式运行时所需的参数，比如Kafka Broker的地址，Kafka Connect的工作目录等等。
consumer.properties：配置Kafka消费者运行时所需的参数，比如Kafka Broker的地址，消费者组的名称等等。
producer.properties：配置Kafka生产者运行时所需的参数，比如Kafka Broker的地址，消息的压缩方式等等。
server.properties：配置Kafka Broker运行时所需的参数，比如Kafka Broker的ID，监听的端口，数据存储路径等等。

bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test --property-file config/producer.properties

### kafka 版本说明
Kafka 的版本号包含两部分：第一部分是 Scala 版本号，第二部分是 Kafka 的版本号。其中 Scala 版本号表示 Kafka 依赖的 Scala 版本，Kafka 版本号表示 Kafka 自身的版本。
例如，Kafka 2.13-2.8.1 的 Scala 版本是 2.13，Kafka 版本是 2.8.1。