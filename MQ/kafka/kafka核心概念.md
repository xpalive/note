

Kafka 的主要概念如下：

### 1. **Producer（生产者）**
- 负责将数据发布到 Kafka 的主题（Topic）中。
- 支持异步或同步的方式发送消息。

### 2. **Consumer（消费者）**
- 从 Kafka 的主题中拉取数据进行消费。
- 消费者通常属于某个消费者组（Consumer Group）。

### 3. **Consumer Group（消费者组）**
- 一个逻辑上的消费者集合，共同消费一个或多个主题的数据。
- 每个分区（Partition）只能被同一个消费者组内的一个消费者消费，实现了负载均衡。

### 4. **Topic（主题）**
- Kafka 中的基本消息类别，用于分类消息。
- 每个主题可以有多个分区（Partition）。

### 5. **Partition（分区）**
- 主题的物理分组，消息以分区为单位存储。
- 每条消息在分区中都有唯一的偏移量（Offset）。
- 分区允许 Kafka 的横向扩展。

### 6. **Offset（偏移量）**
- 消息在分区中的唯一标识。
- 消费者通过偏移量来追踪消息的消费进度。

### 7. **Broker（代理）**
- Kafka 的单个服务器实例，负责存储和转发数据。
- 多个 Broker 组成一个 Kafka 集群。

### 8. **Cluster（集群）**
- 由多个 Broker 组成，提供分布式的消息处理能力。

### 9. **Zookeeper**
- Kafka 依赖 Zookeeper 进行集群管理和元数据存储（新版本使用 Kafka Raft 替代）。
- 用于存储消费者的偏移量、Broker 的状态等。

### 10. **Replication（副本）**
- 每个分区可以有一个或多个副本，用于容错。
- 一个分区的主副本（Leader）负责读写操作，其余副本（Follower）仅用于数据同步。

### 11. **Leader 和 Follower**
- 每个分区的主副本称为 Leader，负责处理该分区的读写请求。
- 其他副本称为 Follower，与 Leader 保持数据同步。

### 12. **Producer Acknowledgement（生产者确认机制）**
- 确保消息成功发布到 Kafka，分为 `acks=0`、`acks=1` 和 `acks=all` 等模式。

### 13. **Retention Policy（保留策略）**
- Kafka 主题的消息存储时间策略。
- 可以设置消息的保留时间或存储空间上限。

### 14. **Log Segments（日志段）**
- 每个分区由多个日志段组成，便于高效管理消息存储。

### 15. **Streams API 和 Connect API**
- **Streams API**：支持构建实时流式处理应用程序。
- **Connect API**：用于连接 Kafka 和其他数据源或目标系统。

这些概念共同构成了 Kafka 的核心功能，实现了高吞吐、可扩展和分布式的消息处理系统。