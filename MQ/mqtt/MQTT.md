### MQTT 消息队列遥测传输 （Message queuing telemetry transport）
1. 工作在TCP/IP协议上
2. 工作在硬件性能底下或网络状况糟糕的情况下
3. 订阅/发布型消息协议

#### 特点
1. 使用发布/订阅消息模式，提供一对多的消息发布，解除应用程序耦合
2. 对负载内容屏蔽的消息传输
3. 使用TCP/IP提供网络连接
4. 有三总消息发布服务质量：
   1. Qos0 至多一次:表示只发送一次，接受者也只能收到一次，可能会发送失败，开销小
      1. sender   ------publish------> receiver 
   2. Qos1 至少一次:表示会发送多次，有重传机制，接受者会收到重复的消息，开销中
      1. sender   ------publish------> receiver
      2. sender   <------puback------ receiver
      3. 由于可能sender没有收到puback的包（丢失，规定时间内没有收到）导致sender重复发送，receiver重复收到消息,sender收到了puback是不会重复发送消息的
      4. puback中没有消息体（payload），在可变头中有一个包标识（packet identifier）与publish中的（packet identifier）一致
   3. Qos2 仅有一次:表示可能会发送多次，有重传机制，会保证接受者会收到消息，且不会收到重复的消息，开销大
      1. sender   ------publish<packet identifier p>------> receiver
      2. sender   <------pubrec<packet identifier p>------ receiver
      3. sender   ------pubrel<packet identifier p>------> receiver
      4. sender   <------pubcomp<packet identifier p>------ receiver
   4.![img.png](image/mqtt.png) 
5. 小型传输，开销很小（固定长度的头部是2个字节），协议交换最小化，降低网络流量
6. 使用Last Will和Testament特性通知有光各方客户端异常中断的机制

#### Qos降级
在MQTT协议中，从Broker到Subscriber的实际Qos = MIN(publish Qos，subscriber Qos)


#### 参考
https://www.jianshu.com/p/e2cc66f96d89
https://www.emqx.com/zh/mqtt-guide#mqtt-basics
https://github.com/iamvoxer/mqttserver
https://blog.csdn.net/zwjzone/article/details/131112929
https://www.emqx.com/zh/mqtt-guidee