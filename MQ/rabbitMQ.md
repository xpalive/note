### command
service rabbitmq-server start
service rabbitmq-server stop
service rabbitmq-server status

1. 开启插件
rabbitmq-plugins enable rabbitmq_management

#### create a user
rabbitmqctl add_user full_access s3crEt
`rabbitmqctl add_user xiongping 123456`
#### tag the user with "administrator" for full management UI and HTTP API access
rabbitmqctl set_user_tags full_access administrator
`rabbitmqctl set_user_tags xiongping administrator`

### Command Line Tools
https://www.rabbitmq.com/cli.html

### 消息介绍
1. work模式，一个消息队列被多个work消费，同一条消息只能被一个work消费
2. 发布订阅模式，一个消息队列被多个work消费，同一条消息可以被每个work消费消费
3. 路由模式，根据路由来指定消费者
4. 主题模式，根据通配符来指定消费者
5. ttl队列+死信队列