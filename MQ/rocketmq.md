### rocketmq
rocketmq 启动时jvm的内存参数配置的比较大，需要根据实际情况修改对应的启动文件
bin/runserver.sh
bin/runbroker.sh

启动 namesrv
nohup bash bin/mqnamesrv &
启动 broker
nohup bash bin/mqbroker -n 192.168.10.81:9876 &
-n 指定要注册的nameserver的地址及端口 
-c 指定配置文件
需要自动创建topic则需要修改 conf/broker.conf文件添加autoCreateTopicEnable=true
多网卡的时候需要指定 brokerIP1 为外网地址

关闭namesrv
bash bin/mqshutdown namesrv
bash bin/mqshutdown broker

bin/mqadmin clusterList -n 192.168.10.81:9876


### 消息介绍
1. 顺序消息
2. 广播消息
3. 延迟消息
4. 批量消息
5. 过滤消息
6. 事务消息