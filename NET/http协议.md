### http 三次握手四次挥手
SYN(synchronous 建立链接)
ACK(acknowledgement 确认)
PSH(push 传输)
FIN(finish 介绍)
RST(reset 重置)
URG(urgent 紧急)
Sequence number(顺序号码)
Acknowledge number(确认号码)
#### 三次握手
```mermaid
sequenceDiagram
    participant c as Client
    participant s as Server
    
    c->>s: SYN=1, seq=x
    s->>c: SYN=1, seq=y, ack=x+1
    c->>s: ack=y+1, seq=x+1 

```
1. 第一次握手：客户端给服务段发送一个SYN报文，并发送一个顺序号码x
2. 第二次握手：服务段给客户端回复一个SYN报文，并发送一个顺序码y，同时发送ack=x+1
3. 第三次握手：客户端给服务段回复一个SYN报文，并发送ack=y+1，同时发送seq=x+1

三次握手是为了确保双方建立了连接
#### 四次挥手
```mermaid
sequenceDiagram
    participant c as Client
    participant s as Server
    
    c->>s: FIN=1, seq = x
    s->>c: FIN=1, ack=x+1, seq=y
    s->>c: FIN=1, ack=x+1, seq=z
    c->>s: FIN=1, ack=z+1, seq=h
```
1. 第一次挥手：客户端打算断开连接，向服务器发送FIN报文(FIN标记位被设置为1，1表示为FIN，0表示不是)，FIN报文中会指定一个序列号，之后客户端进入FIN_WAIT_1状态。
也就是客户端发出连接释放报文段(FIN报文)，指定序列号seq = u，主动关闭TCP连接，等待服务器的确认。

2. 第二次挥手：服务器收到连接释放报文段(FIN报文)后，就向客户端发送ACK应答报文，以客户端的FIN报文的序列号 seq+1 作为ACK应答报文段的确认序列号ack = seq+1 = u + 1。
接着服务器进入CLOSE_WAIT(等待关闭)状态，此时的TCP处于半关闭状态(下面会说什么是半关闭状态)，客户端到服务器的连接释放。客户端收到来自服务器的ACK应答报文段后，进入FIN_WAIT_2状态。

3. 第三次握手：服务器也打算断开连接，向客户端发送连接释放(FIN)报文段，之后服务器进入LASK_ACK(最后确认)状态，等待客户端的确认。
服务器的连接释放(FIN)报文段的FIN=1，ACK=1，序列号seq=m，确认序列号ack=u+1。

4. 第四次握手：客户端收到来自服务器的连接释放(FIN)报文段后，会向服务器发送一个ACK应答报文段，以连接释放(FIN)报文段的确认序号 ack 作为ACK应答报文段的序列号 seq，以连接释放(FIN)报文段的序列号 seq+1作为确认序号ack。
之后客户端进入TIME_WAIT(时间等待)状态，服务器收到ACK应答报文段后，服务器就进入CLOSE(关闭)状态，到此服务器的连接已经完成关闭。

客户端处于TIME_WAIT状态时，此时的TCP还未释放掉，需要等待2MSL后，客户端才进入CLOSE状态。
![四次挥手](image/tcp.png)
1. 主动关闭方发送 FIN 给被动关闭方，进入 FIN_WAIT_1 状态，此时处于半关闭状态。
2. 被动关闭方收到 FIN 后，发送 ACK 给主动关闭方，进入 CLOSE_WAIT 状态。
3. 被动关闭方发送 FIN 给主动关闭方，进入 LAST_ACK 状态，此时处于半关闭状态。
4. 主动关闭方收到 FIN 后，发送 ACK 给被动关闭方，进入 TIME_WAIT 状态。
5. 被动关闭方收到 ACK 后，进入 CLOSED 状态。
6. 主动关闭方在 TIME_WAIT 状态等待一段时间后，进入 CLOSED 状态。


四次挥手是为了确保双方的数据都发送完毕

### 什么是2MSL
Maximum segment lifetime 分片数据（报文）在网络中的最大存活时间
在RFC 793中定义MSL通常为2分钟

在linux中修改MSL
> cat /proc/sys/net/ipv4/tcp_fin_timeout

### 什么是半关闭
TCP的半关闭(half-close)，半关闭是指：TCP提供了连接的一方在结束它的发送后还能接受来自另一端数据的能力。
TCP的半打开(half-open)，半打开

### 描述

| 标识    | ASCII | 描述                     | 字符  |
|-------|-------|------------------------|-----|
| CR    | 13    | Carriage return 回车     | \r  |
| LF    | 10    | Line feed character 换行 | \n  |
| SP    | 30    | Horizontal space 空格    |     |
| COLON | 58    | COLON 冒号               |     |

### chunked介绍
http协议通常使用Content-Length来标识body的长度，在服务器端需要先申请对应长度的buff，然后再赋值。
如果需要一边生产数据以便发送数据，就需要使用”Transfer-Encoding: chunked“来代替Content-Length来对数据分块

### 压缩开启压缩的优缺点
优点：
1. 减少流量
2. 提升加载速度
缺点
3. 服务器需要更错的cpu资源进行计算，降低服务器的整体吞吐量
4. 服务其需要更多的内存资源，如数据为1k的情况下，压缩后为250B则需要多申请250B的内存空间
5. 客户端需要消耗更多的cpu进行资源解压

### 其他
1. 当浏览器与服务器建立连接时，会在客户端本地可用范围内的端口中随机获取一个端口用于和服务器建立连接  
2. 确认连接的五要素：协议，源ip，源端口，目标ip，目标端口

### 参考
https://cloud.tencent.com/developer/article/2094245