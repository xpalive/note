### GPU CPU

1. 所有的语言跑对应的指令来完成GPU或CPU所对应的能力 将编程语言通过解析器转化为对应平台的机器指令，GPU和CPU都有对应的指令 将数据和参数传入对应的指令，让GPU或CPU计算好后，再取出结果

工具一般有CUDA,Opencl,DirectCompute.

1. 用CUDA就是把C/C++代码编译成GPU的指令。再配合其他API调用代码，只能用在Nvidia的GPU。
2. OpenCL跟DirectCompute是不分显卡的。只要显卡硬件支持通用计算，实现了该驱动。CL的kernel是一种类C的语言。最终会被编译成GPU指令。至于C++
   Java也可以做的，也只是调用编译好的Kernel。往里面拷贝数据，穿参数。让GPU计算完后，CPU再取回数据。
3. 另外的SIMD指令加速，对于小型的数据，可以优先考虑。因为GPU加速，可能需要内存拷贝，使用开销相对较大。用SIMD的话，直接就是用CPU的特殊指令MMX SSE
   AVX加速。Java应该会有对应的包，C++用Intrinsic头文件。