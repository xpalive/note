### 将u盘格式化为window，linux，mac都识别的格式
在Ubuntu系统下  
sudo apt install exfat-utils  
查看U盘ID  
sudo fdisk -l  
格式化命令  
sudo mkfs.exfat -n xq_256 /dev/sda1  
fsck检查  
sudo fsck.exfat /dev/sda1



#### 参考
https://blog.csdn.net/qq_21950671/article/details/125384196