### 免密登录
> ssh-keygen -t rsa  
> ssh-copy-id root@192.168.10.80  

### ssh 登录
1.  首先，要确保CentOS7安装了  openssh-server，在终端中输入  yum list installed | grep openssh-server
1.  如果又没任何输出显示表示没有安装  openssh-server，通过输入  yum install openssh-server
/etc/ssh/sshd_config
```shell script
Port 22 # 打开端口
ListenAddress 0.0.0.0 
ListenAddress ::
PermitRootLogin yes #允许root登录
PasswordAuthentication yes #使用密码登录
```
1. 启动sshd服务
```shell script
sudo service sshd start/restart
ps -ef | grep sshd # 确保服务启动
```
