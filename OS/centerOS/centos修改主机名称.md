### 修改主机名称
> 注：如何修改linux主机名才能生效?  
  1.hostname +主机名 (这个只是做为暂时的,重启后将恢复到原来的名字.)  
  2.很多人说修改/etc/hosts文件,其实这个文件里的主机名只是为来提供给dns解析的.如果你用不上dns,只需要修改主机名,那修改这个没用.  
  3..修改这个文件etc/sysconfig/network这个文件里的主机名.(重启后生效)  
>

/etc/sysconfig/network 作用

查看主机名称  
```shell script
hostname / hostnamectl  
```

修改主机名  
/etc/hostname
```shell script
${hostname}
```

更新hosts文件
/etc/hosts
```shell script
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
```
```shell script
127.0.0.1   localhost ENV01 localhost4 localhost4.localdomain4
::1         localhost ENV01 localhost6 localhost6.localdomain6
```
