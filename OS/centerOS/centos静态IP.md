### 静态IP
/etc/sysconfig/network-scripts
> BOOTPROTO="static" # 使用静态IP地址，默认为dhcp 
  IPADDR="192.168.10.80" # 设置的静态IP地址
  NETMASK="255.255.255.0" # 子网掩码 
  BROADCAST="192.168.10.255" # 广播地址 
  GATEWAY="192.168.10.1" # 网关地址 
  DNS1="192.168.10.1"
  DNS2="61.177.7.1"  
>
> 重启网络服务 service network restart
