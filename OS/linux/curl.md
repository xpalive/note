#### curl

https://www.cnblogs.com/panxuejun/p/10574038.html

#### 指定http代理IP和端口
curl -x http_proxy://113.185.19.192:80 http://aiezu.com/test.php
#### 指定为https代理
curl -x HTTPS_PROXY://113.185.19.192:80 http://aiezu.com/test.php

#### 指定代理用户名和密码，basic认证方式
curl -x aiezu:123456@113.185.19.192:80 http://aiezu.com/test.php
curl -x 113.185.19.192:80 -U aiezu:123456 http://aiezu.com/test.php
curl -x 113.185.19.192:80 --proxy-user aiezu:123456 http://aiezu.com/test.php
 
#### 指定代理用户名和密码，ntlm认证方式
curl -x 113.185.19.192:80 -U aiezu:123456 --proxy-ntlm http://aiezu.com/test.php
 
#### 指定代理协议、用户名和密码，basic认证方式
curl -x http_proxy://aiezu:123456@113.185.19.192:80 http://aiezu.com/test.php