### 通过 $? 获取上一条命令的执行结果，若为0则是成功
```shell
if (test $? -ne 0)
then
//statements//
fi
```
或
```shell
ret=$?
if [ $ret -ne 0 ]; then
echo "In If"
else
echo "In Else"
fi
```
