### es 支持矩阵
https://www.elastic.co/cn/support/matrix
### es 安装配置（开箱即用）
1. 下载es
2. tar -zxvf elasticsearch-7.8.1-linux-x86_64.tar.gz
3. cd elasticsearch-7.8.1
4. ./bin/elasticsearch   # 启动es
5. ./bin/elasticsearch -d   # 后台启动es

To run Elasticsearch as a daemon, specify -d on the command line, and record the process ID in a file using the -p option
`./bin/elasticsearch -d -p pid`
Log messages can be found in the $ES_HOME/logs/ directory.

To shut down Elasticsearch, kill the process ID recorded in the pid file:
`pkill -F pid`

Elasticsearch loads its configuration from the $ES_HOME/config/elasticsearch.yml file by default.

elasticsearch.yml for configuring Elasticsearch
jvm.options for configuring Elasticsearch JVM settings
log4j2.properties for configuring Elasticsearch logging

### es 启动
创建组
groupadd elsearch
创建用户
useradd elsearch -g elsearch -p elasticsearch
修改文件夹权限
chown -R elsearch:elsearch elasticsearch
切换用户启动
su elsearch
${ES_HOME}/bin/elasticsearch

### 获取当前es信息
GET /

### 使用root启动 （由于es可以接受用户输入的脚本并执行，为了系统安全，所以不允许root启动）
> 使用root 启动 bin/elasticsearch -Des.insecure.allow.root=true
> 或
> vim bin/elasticsearch（添加ES_JAVA_OPTS="-Des.insecure.allow.root=true"）

### es 端口号
-9200 http协议，主要是用于外部通讯
-9300 tcp协议，主要是集群间通信，SpringBoot连接ES等

### 问题
ulimit -Hu
ulimit -Su
https://blog.csdn.net/qq_40251815/article/details/99761908

vim /etc/security/limits.conf
```shell
esyonghu soft nofile 65536
esyonghu hard nofile 68836
esyonghu soft nproc 4096
esyonghu hard nproc 4096
```
vim /etc/sysctl.conf
```shell
vm.max_map_count=655360
sysctl -p 生效
```


#### 虚拟机安装es无法访问
修改配置文件`${ES_HOME}/config/elasticsearch.yml`,添加参数如下
```yaml
network.host: 0.0.0.0
http.port: 9200
transport.host: localhost
transport.tcp.port: 9300
```

### es 数据存储
memory buffer -> file system cache -> disk
translog - disk 数据保障机制

### 安装目录
/usr/local
