### 解压kibana

### 启动kibana 允许root启动
${KIBANA_HOME}/bin/kibana --allow-root

### 配置kabana
${KIBANA_HOME}/config/kibana.yml
server.port: 5601
server.host: "0.0.0.0"
elasticsearch.url: ["http://es-ip:9200"]
kibana.index:".kibana"

### kibana启动
创建组
groupadd kibana
创建用户
useradd kibana -g kibana -p kibana
修改文件夹权限
chown -R kibana:kibana kibana
切换用户启动
su kibana
${KIBANA_HOME}/bin/kibana

或
nohup ./kibana --allow-root > /dev/null 2>&1 &  # 后台启动

### 关闭kibana
netstat -tunlp | grep 5601

### 查询ES
navigation -> management -> dev tools


### 安装目录
/usr/local