禁用开机启动
systemctl disable firewalld.service
停止
systemctl stop firewalld.service

查看状态
firewall-cmd --state 

添加端口
firewall-cmd --add-port=80/tcp --permanent
移除端口
firewall-cmd --remove-port=80/tcp --permanent

重载
firewall-cmd --reload

