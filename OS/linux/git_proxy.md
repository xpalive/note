git clone https://github.com/xpalive/spring-framework.git
git clone git@github.com:xpalive/spring-framework.git

git config --global http.github.com.proxy socks5://192.168.10.120:7890

curl -x http_proxy://192.168.10.120:7890 https://www.google.com

.ssh/config 配置
```
# 必须是 github.com  
Host github.com  
   HostName github.com  
   User git  
   # 走 HTTP 代理  
   # ProxyCommand socat - PROXY:127.0.0.1:%h:%p,proxyport=8080  
   # 走 socks5 代理（如小飞机 or V2xxx）  
   # ProxyCommand nc -v -x 127.0.0.1:1080 %h %p  
对于 Windows 用户，要使用 socks5 代理却没有 nc 的，可以将  
ProxyCommand nc -v -x 127.0.0.1:1080 %h %p  
换成 # -S 为 socks, -H 为 HTTP  
ProxyCommand connect -S 127.0.0.1:1080 %h %p  

> nc ncat netcat  
> 备注：linux版本socks有问题 ,可能是linux连接代理没有认证
> 备注：linux版本http没有问题
```
.gitconfig 配置
```
[user]  
    name = xiongping  
    email = xiongping@jszw.info  
[http "https://github.com"]  
    proxy = http://127.0.0.1:7890  
    # proxy = socks5://127.0.0.1:7890  
```

* git不存在[https]的配置

1.https://bin.zmide.com/?p=748  
2.https://gist.github.com/chuyik/02d0d37a49edc162546441092efae6a1  
3.https://segmentfault.com/a/1190000012034746  