#!/bin/bash
mkdir -p /etc/clash
# https://github.com/Dreamacro/clash/releases/download/v1.8.0/clash-linux-amd64-v1.8.0.gz
gunzip clash-linux-amd64-v1.8.0.gz
chmod +x clash-linux-amd64-v1.8.0
mv clash-linux-amd64-v1.8.0 /usr/bin/clash
echo "[Unit]
Description=clash
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Type=simple
User=root
Group=root
DynamicUser=true
ExecStart=/usr/bin/clash -d /etc/clash/
Restart=always
LimitNOFILE=512000

[Install]
WantedBy=multi-user.target
" >> /usr/lib/systemd/system/clash.service
