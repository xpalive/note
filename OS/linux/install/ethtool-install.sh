#!/bin/bash

#判断系统发行版
if [ -f /etc/redhat-release ]; then
DISTRO="centos"
elif cat /etc/issue | grep -q -E "Debian"; then
DISTRO="debian"
elif cat /etc/issue | grep -q -E "Ubuntu"; then
DISTRO="ubuntu"
else
echo "无法识别发行版,请检查!"
exit 1
fi

#安装ethtool
case "$DISTRO" in
centos)
yum -y install ethtool
;;
debian|ubuntu)
apt-get update
apt-get -y install ethtool
;;
*)
echo "不支持的发行版: $DISTRO"
exit 1
;;
esac

