#!/bin/bash
set -e
del_tar=N
case "$1" in
  'del_tar')
    del_tar=Y
    ;;
  *)
    del_tar=N
    ;;
esac
java_file_gz=jdk-8u301-linux-x64.tar.gz
java_unzip_dir=jdk1.8.0_301
java_path=/usr/local/java
if [ ! -d $java_path ];then
  mkdir -p $java_path
  echo "mkdir -> " $java_path
else
  echo "java_path is '$java_path'"
fi
mv $java_file_gz $java_path
tar -zxvf $java_path/$java_file_gz -C $java_path
if [ $del_tar == "Y" ]; then
  rm -rf $java_path/$java_file_gz
fi
echo 'export JAVA_HOME='$java_path/$java_unzip_dir'
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib
' >> /etc/profile
# 需要使用source java-install.sh来执行使最后一行生效
# 需要将安装包与脚本放置在同一目录下

-------------
卸载openjdk

#!/bin/bash
rpm -e --nodeps java-1.7.0-openjdk
rpm -e --nodeps java-1.7.0-openjdk-headless
rpm -e --nodeps java-1.8.0-openjdk
rpm -e --nodeps java-1.8.0-openjdk-headless
rpm -e --nodeps copy-jdk-configs
rpm -e --nodeps tzdata-java
rpm -e --nodeps javapackages-tools
rpm -e --nodeps python-javapackages