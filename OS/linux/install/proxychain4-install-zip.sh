#!/bin/bash
# proxychains4 curl https://api.ipify.org/?format=json
unzip proxychains-ng-master.zip
cd proxychains-ng-master
./configure && make && make install
# 如果报错权限问题则使用 sudo ./configure && sudo make && sudo make install
make install-config
sed -i 's|socks4|# socks4|' /usr/local/etc/proxychains.conf
echo 'socks5 127.0.0.1 7891'>>/usr/local/etc/proxychains.conf