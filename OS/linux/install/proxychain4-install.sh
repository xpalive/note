#!/bin/bash
# proxychains4 curl https://api.ipify.org/?format=json
git clone https://github.com/rofl0r/proxychains-ng.git
cd proxychains-ng
./configure && make && make install
make install-config
sed -i 's|socks4|# socks4|' /usr/local/etc/proxychains.conf
echo 'socks5 192.168.10.120 7890'>>/usr/local/etc/proxychains.conf