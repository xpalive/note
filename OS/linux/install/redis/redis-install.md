### redis 安装
1. 下载redis包： wget https://download.redis.io/redis-stable.tar.gz
2. 解压redis：tar -xzvf redis-stable.tar.gz
3. 进入目录：cd redis-stable
4. 安装：make
如果安装成功，那么在src目录下会生产redis-server 和redis-cli文件
如果要安装到/usr/local/bin中
5. 使用：make install


### redis 启动
./bin/redis-server ./redis.conf