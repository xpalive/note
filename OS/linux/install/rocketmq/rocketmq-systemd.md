/usr/lib/systemd/system/rmq_namesrv.service
```shell
[Unit]
Description=rmq
After=network.target

[Service]

#这里Type一定要写simple
Type=simple
Environment="JAVA_HOME=/usr/local/java/jdk1.8.0_301"
#ExecStart和ExecStop分别在systemctl start和systemctl stop时候调动
ExecStart=/root/app/rocketmq-all-4.7.1-bin-release/bin/mqnamesrv
ExecStop=/root/app/rocketmq-all-4.7.1-bin-release/bin/mqshutdown namesrv

[Install]
WantedBy=multi-user.target
```

----------------------

/usr/lib/systemd/system/rmq_broker.service
```shell
[Unit]
Description=rmq
After=network.target rmq_namesrv.service

[Service]
Type=simple
Environment="JAVA_HOME=/usr/local/java/jdk1.8.0_301"
ExecStart=/root/app/rocketmq-all-4.7.1-bin-release/bin/mqbroker -c /root/app/rocketmq-all-4.7.1-bin-release/conf/broker.conf
ExecStop=/root/app/rocketmq-all-4.7.1-bin-release/bin/mqshutdown broker

[Install]
WantedBy=multi-user.target
```

