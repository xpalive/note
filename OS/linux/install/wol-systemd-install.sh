#!/bin/bash
cat >/etc/systemd/system/wol.service<<EOF
[Unit]
Description=Configure Wake On LAN

[Service]
Type=oneshot
ExecStart=/sbin/ethtool -s eth0 wol g #请将eth0改为你的网络接口名称

[Install]
WantedBy=basic.target
EOF