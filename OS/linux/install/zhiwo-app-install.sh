#!/bin/bash

appdirpath=/data/app/nems
springprofileactive=dev
appname=nems
port=9100
if [ ! -d $appdirpath ];then
  mkdir -p $appdirpath
  mkdir -p $appdirpath/webserver
  mkdir -p $appdirpath/logs
  mkdir -p $appdirpath/upload
  echo "mkdir -> " $appdirpath
else
  echo "appdirpath is '$appdirpath'"
fi
echo 'spring profiles active is :'$springprofileactive
echo 'appname is :'$appname


echo "# In anywhere app-install.sh
version: \"3.9\"  # optional since v1.27.0
services:
  webapp:
    image: mamohr/centos-java:jdk8
    container_name: $appname
    restart: always
    ports:
      - \"$port:8886\"
    volumes:
      - $appdirpath/webserver:/webserver
      - $appdirpath/logs:/logs
      - $appdirpath/upload:/upload
      - /etc/localtime:/etc/localtime:ro
    command: >
      bash -c \"ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
      && java -Djava.security.egd=file:/dev/./urandom -jar /webserver/${appname}.jar --spring.profiles.active=${springprofileactive}\"
networks:
  default:
    external: true
    name: zhiwo-net
" >> $appdirpath/docker-compose.yml