```shell
echo "[Unit]
Description=zookeeper
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Type=forking
User=root
Group=root
ExecStart=/root/app/apache-zookeeper-3.5.8-bin/bin/zkServer.sh start /root/app/apache-zookeeper-3.5.8-bin/conf/zoo.cfg
ExecStop=/root/app/apache-zookeeper-3.5.8-bin/bin/zkServer.sh stop /root/app/apache-zookeeper-3.5.8-bin/conf/zoo.cfg
ExecReload=/root/app/apache-zookeeper-3.5.8-bin/bin/zkServer.sh restart /root/app/apache-zookeeper-3.5.8-bin/conf/zoo.cfg

[Install]
WantedBy=multi-user.target
" >> /usr/lib/systemd/system/zookeeper.service
```
