### ldd命令
查看依赖

```shell
ldd /usr/local/cmake-3.6.0-Linux-x86_64/bin/cmake

linux-vdso.so.1 (0x00007ffe9f78d000)
libidn.so.11 => not found
libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007fac0dd09000)
libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007fac0dc22000)
libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fac0d9fa000)
/lib64/ld-linux-x86-64.so.2 (0x00007fac0dd21000)
```

安装或查找libidn.so.11，将该文件拷贝到/lib/x86_64-linux-gnu/目录下

