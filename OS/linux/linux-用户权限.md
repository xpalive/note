### 查看用户
vim /etc/passwd
```shell
# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
```
7个字段含义
1. 用户名 （magesh）： 已创建用户的用户名，字符长度 1 个到 12 个字符。
2. 密码（x）：代表加密密码保存在 `/etc/shadow 文件中。
3. 用户 ID（506）：代表用户的 ID 号，每个用户都要有一个唯一的 ID 。UID 号为 0 的是为 root 用户保留的，UID 号 1 到 99 是为系统用户保留的，UID 号 100-999 是为系统账户和群组保留的。
4. 群组 ID （507）：代表群组的 ID 号，每个群组都要有一个唯一的 GID ，保存在 /etc/group文件中。
5. 用户信息（2g Admin - Magesh M）：代表描述字段，可以用来描述用户的信息（LCTT 译注：此处原文疑有误）。
6. 家目录（/home/mageshm）：代表用户的家目录。
7. Shell（/bin/bash）：代表用户使用的 shell 类型。

### 文件同步
rsync