### 正确的修改时区的方式
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

### 错误的修改时区的方式
通过cp命令覆盖/etc/localtime

### 原因
通过cp命令来修改/etc/localtime时修改的是文件内容，而不是文件
那么date命令是通过/etc/localtime的内容来确定时区的
而java是通过/etc/localtime的文件名来确定时区的

当/etc/localtime 软连接到 /usr/share/zoneinfo/Asia/Shanghai java 取的是 Asia/Shanghai
如果使用cp命令来修改/etc/localtime文件，那么就可能修改的是时区文件的内容而文件名还是原来的文件名 那么java取的值就不正确了

### java启动指定时区 针对 -jar 好像无效
java -Duser.timezone=GMT+08 -jar xxx.jar
java -Duser.timezone=Asia/Shanghai -jar xxx.jar

#### 查看时间
系统时间 date
硬件时间 hwclock -r
系统时间同步到硬件时间 hwclock --systohc
yum install -y ntpdate 
网络时间同步 ntpdate 0.asia.pool.ntp.org