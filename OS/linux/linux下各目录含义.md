### 目录结构类比windows
/usr             C:/windows/  

/usr/lib         C:/windows/System32  

/usr/local       C:/Program Files

/opt             D:/Software

/usr/src         系统源码目录

/usr/local/src   用户源码目录

/bin 用来贮存用户命令。/usr/bin 也被用来贮存用户命令。  
  
/sbin 许多系统命令（例如 shutdown）的贮存位置。/usr/sbin 中也包括了许多系统命令。  
  
/root 根用户（超级用户）的主目录。  
  
/mnt 该目录中通常包括系统引导后被挂载的文件系统的挂载点。比如，默认的光盘挂载点是 /mnt/cdrom/。可以为U盘建一个挂载点：mkdir /mnt/usb。  
  
/boot 包括内核和其它系统启动期间使用的文件，最大不超过200M。通常我为 /boot 建一个primary分区是1G。  
  
/lost+found 被 fsck 用来放置零散文件（没有名称的文件）。  
  
/lib 包含许多被 /bin 和 /sbin 中的程序使用的库文件。目录 /usr/lib 中含有更多用于用户程序的库文件。/lib 目录下放置的是 /bin 和 /sbin 目录下程序所需的库文件。简单说，/lib 是内核级的；/usr/lib 是系统级的；/usr/local/lib 是用户级的。仅仅被 /usr 目录下的程序所使用的共享库不必放到 /lib 目录下。只有 /bin 和 /sbin 下的程序所需要的库有必要放到 /lib 目录下。  
  
/dev 贮存设备文件。  
  
/etc 包含许多配置文件和目录。  
  
/var 用于贮存variable（或不断改变的）文件，例如日志文件和打印机假脱机文件，虚拟机镜像文件等。  
  
/usr 包括与系统用户直接有关的文件和目录，例如应用程序及支持它们的库文件。  
  
/proc 一个虚拟的文件系统（不是实际贮存在磁盘上的），它包括被某些程序使用的系统信息。  
  
/initrd 用来在计算机启动时挂载 initrd.img 映像文件的目录以及载入所需设备模块的目录。不要删除 /initrd 目录。如果你删除了该目录后再重新引导 Red Hat Linux 时，你将无法引导你的计算机。  
  
/tmp 用户和程序的临时目录。 /tmp 给予所有系统用户读写权。  
  
/home 用户主目录的默认位置。  
  
/opt 可选文件和程序的贮存目录。该目录主要被第三方开发者用来简易地安装和卸装他们的软件包。  

在传统的unix系统中，/usr 通常只包含系统发行时自带的程序，而/usr/local 则是本地系统管理员用来自由添加程序的目录。这里有一条严厉而牢固的规则：除非在里面创建目录，unix发行版不得使用 /usr/local 。

对于Linux发行版，如 RedHat， Debian 等等，一个可能的规定是：/usr 目录只能由发行版的软件包管理工具负责管理，而对 /usr/local 却没有这样做。正是因为采用这种方式，软件包管理工具的数据库才能知道在 /usr 目录内的每一个文件。下面演示 Ubuntu 系统下如何安装JDK和配置环境变量。 

因此JDK可能比较合适的位置是 /usr/local/lib 下。我一般安装到 /usr/local/Java 下，这样避免搞乱 /usr/local/lib 目录。