### logrotate 日志切割
**1. 安装logrotate**
```shell
yum -y install logrotate crontabs
# 配置文件
/etc/cron.daily/logrotate
/etc/logrotate.conf # 主配置文件
/etc/logrotate.d # 配置目录
```
logrotate的配置文件是/etc/logrotate.conf，通常不需要对它进行修改。日志文件的轮循设置在独立的配置文件中，它（们）放在/etc/logrotate.d/目录下。

**2. logrotate 定时任务**

```shell
# cat /etc/cron.daily/logrotate
# logrotate需要的cron任务应该在安装时就自动创建了，我把cron文件的内容贴出来，以供大家参考。
#!/bin/sh

/usr/sbin/logrotate /etc/logrotate.conf
EXITVALUE=$?
if [ $EXITVALUE != 0 ]; then
    /usr/bin/logger -t logrotate "ALERT exited abnormally with [$EXITVALUE]"
fi
exit 0
```

3. logrotate命令
```shell
# 测试配置
logrotate -d /etc/logrotate.d/nginx
# 手动执行轮转
logrotate -f /etc/logrotate.d/nginx
```

**工作机制：**  
logrotate 是通过 系统的定时任务（通常在 /etc/cron.daily/）定期执行的。每天，系统会检查并执行 logrotate，以确定是否需要对日志文件进行轮转。

当你修改或添加 /etc/logrotate.d/nginx 配置文件时，这些更改会在下次定期运行 logrotate 时自动生效。








https://www.cnblogs.com/clsn/p/8428257.html