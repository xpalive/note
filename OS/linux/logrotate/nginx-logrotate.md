这个 `logrotate` 配置文件用于管理 Nginx 日志文件的轮转和压缩，确保日志文件不会无限增长，影响系统的性能或占用磁盘空间。以下是配置的详细解释：

配置文件：/etc/logrotate.d/nginx
```bash
/usr/local/nginx/logs/*.log {
    daily
    missingok
    rotate 7
    compress
    delaycompress
    notifempty
    create 640 root root
    sharedscripts
    postrotate
        [ -f /usr/local/nginx/logs/nginx.pid ] && kill -USR1 `cat /usr/local/nginx/logs/nginx.pid`
    endscript
}
```

### 配置项详解：

1. **`/var/log/nginx/*.log`**：
    - 这行定义了要轮转的日志文件路径，`/var/log/nginx/*.log` 表示所有以 `.log` 结尾的 Nginx 日志文件，包括 `access.log` 和 `error.log`。

2. **`daily`**：
    - 表示日志文件每天轮转一次。即每天创建一个新的日志文件。

3. **`missingok`**：
    - 如果日志文件丢失（不存在），不报错并继续下一个文件，而不是终止轮转过程。这有助于避免由于日志文件不存在而中断的情况。

4. **`rotate 14`**：
    - 表示最多保留 14 个旧的日志文件。第 15 天时，最早的日志文件将被删除（即只保留最近 14 天的日志文件）。

5. **`compress`**：
    - 启用日志文件压缩，使用 `gzip` 压缩旧的日志文件。这会节省磁盘空间。

6. **`delaycompress`**：
    - 表示日志文件轮转时不会立即压缩最新的日志文件，而是等到下一次轮转时才压缩。这样可以确保最近的日志文件仍然是未压缩状态，以便随时查看。

7. **`notifempty`**：
    - 如果日志文件是空的（没有记录任何日志），则不进行轮转。这避免了创建空的压缩文件。

8. **`create 640 www-data adm`**：
    - 轮转后的新日志文件会使用 `640` 权限进行创建，文件所有者为 `www-data` 用户，所属组为 `adm`。
        - **`640`**：表示文件所有者有读写权限，组有读权限，其他人没有权限。
        - **`www-data`**：这是 Nginx 默认的用户。
        - **`adm`**：属于 `adm` 组的用户可以读取日志文件。

9. **`sharedscripts`**：
    - 表示如果有多个日志文件进行轮转，`postrotate` 脚本只运行一次，而不是对每个日志文件都执行一次。这在 Nginx 这种同时处理多个日志文件的情况中非常有用。

10. **`postrotate`**：
    - 这是一个脚本块，表示在日志文件轮转之后需要执行的命令。
    - **`[ -f /var/run/nginx.pid ] && kill -USR1 \`cat /var/run/nginx.pid\``**：
        - 该命令用于向 Nginx 发送 `USR1` 信号，通知它重新打开日志文件。这是 Nginx 的标准做法，用来在日志轮转后确保继续写入新的日志文件。
        - **`/var/run/nginx.pid`**：Nginx 运行时会在此文件中存储进程 ID (PID)。
        - **`kill -USR1 \`cat /var/run/nginx.pid\``**：向 Nginx 进程发送 `USR1` 信号，告诉它重新加载日志文件。

11. **`endscript`**：
    - 标识 `postrotate` 脚本块的结束。

### 工作流程：

1. 每天检查 `/var/log/nginx/*.log` 下的日志文件。
2. 如果日志文件存在且不是空文件，则对其进行轮转。
3. 保留最近 14 天的日志，超过 14 天的旧日志会被删除。
4. 轮转后的日志会被压缩，但延迟到下次轮转时才压缩最近的日志文件。
5. 创建新的日志文件时，设置文件权限为 `640`，并确保新文件由 `www-data` 用户拥有，`adm` 组成员可以读取。
6. 在日志轮转完成后，执行 `postrotate` 脚本，通知 Nginx 重新打开日志文件，确保它继续写入新的日志文件。

### 作用：

这个配置确保了 Nginx 的日志管理高效、可靠，避免日志文件占用过多磁盘空间，并通过压缩和轮转保持较小的日志文件大小。