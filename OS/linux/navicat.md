### navicat https://www.zhoubotong.site/post/79.html
快到期之前，可以把之前连接的数据库通过 文件->导出连接，备份之前的数据库连接即可，下次激活后可以直接导入连接。
Navicat Premium 16的试用期只有14天，执行下面两个命令，即可无限使用。
1、关闭Navicat程序
2、删除如下2个文件：

```shell
rm -rf ~/.config/navicat
rm -rf ~/.config/dconf/user
```

```shell
lsof | grep navicat | grep \\.config  #用于列出当前系统打开navicat的工具
```
再次重新启动navicat即可。