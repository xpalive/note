### nc ncat netcat

1. 传输文件 81 -> 82  
```shell script
nc -l 9999 > install-proxychains.sh # 接收文件 82 操作
nc 192.168.10.82 9999 < install-proxychains.sh # 传输文件 81 操作


```

2. 通过nc 测试端口联通
nc 默认情况下使用tcp协议可以通过以下命令进行测试
> nc -zv <hostname> <port>

测试udp端口需要使用-u选项
> nc -uvz <hostname> <port>

参数含义
-u: 使用UDP协议进行连接
-v: 启动详细模式，输出更多的信息，一边查看连接状态
-z: 零-I/O模式，只扫描端口而不发生任何数据。用于检测端口是否开放