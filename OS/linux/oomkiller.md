### Linux内核OOM killer机制
https://zhuanlan.zhihu.com/p/640734324

### 查看被OOM killer杀死的进程
egrep -i -r 'killed process' /var/log
> /var/log/syslog.1:May  6 10:02:51 iZuf66b59tpzdaxbchl3d4Z kernel: [1467990.340288] Killed process 17909 (procon) total-vm:5312000kB, anon-rss:4543100kB, file-rss:0kB

或
dmesg -T
> [1471454.635492] Out of memory: Kill process 17907 (procon) score 143 or sacrifice child
> [1471454.636345] Killed process 17907 (procon) total-vm:5617060kB, anon-rss:4848752kB, file-rss:0kB

或
dmesg
> [Wed May 15 14:03:08 2019] Out of memory: Kill process 83446 (machine) score 250 or sacrifice child
> [Wed May 15 14:03:08 2019] Killed process 83446 (machine) total-vm:1920560kB, anon-rss:1177488kB, file-rss:1600kB 

### 修改oom_adj的值来修改进程的权重
/proc/$PID/oom_adj 中的值为-17到+15，取值越高，越容易被杀掉
/proc/$PID/oom_score linux内核会通过oom_score的分值来杀进程，分值越高，被杀的优先级越高
oom_adj与oom_score 的分值成正比

### 通过进程名称修改oom_adj的值
pgrep -f "/usr/sbin/sshd" | while read PID; do echo -17 > /proc/$PID/oom_adj; done