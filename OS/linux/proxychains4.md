CentOS 7 安裝 proxychains 

安裝 proxychains
```shell script
cd ~
git clone https://github.com/rofl0r/proxychains-ng.git 
cd proxychains-ng 
./configure && make && make install  
make install-config
```

如果没有权限则可以使用
```shell
sudo ./configure && sudo make && sudo make install
```

配置完成后会提示
```shell
./tools/install.sh -D -m 644 libproxychains4.so /usr/local/lib/libproxychains4.so
./tools/install.sh -D -m 755 proxychains4 /usr/local/bin/proxychains4
./tools/install.sh -D -m 755 proxychains4-daemon /usr/local/bin/proxychains4-daemon
```

安装完成后会提示
```shell
./tools/install.sh -D -m 644 src/proxychains.conf /usr/local/etc/proxychains.conf
```


配置
```shell script
# vim /usr/local/etc/proxychains.conf
# vim /etc/proxychains4.conf
更改
socks4 127.0.0.1 9050
为
socks5 127.0.0.1 xxxx
xxxx为你shadowsocks本地代理的port
或 
http 192.168.10.122 7890
```

檢查是否代理成功
```shell script
proxychains4 curl https://api.ipify.org/?format=json
返回{"ip":"103.135.248.79"}则表示成功

proxychains4 bash install-docker.sh #这样可以代理脚本中的所有连接
```

#### 安装proxychains
```shell script
#!/bin/bash
git clone https://github.com/rofl0r/proxychains-ng.git
cd proxychains-ng
./configure && make && make install 
make install-config
sed -i 's|socks4|# socks4|' /usr/local/etc/proxychains.conf
echo 'socks5 192.168.10.120 7890'>>/usr/local/etc/proxychains.conf # 此处需要根据代理来实际的填写端口socks5 7891
```

### apt-get 安装
sudo apt install proxychains4
修改配置文件
sudo vim /etc/proxychains4.conf
添加代理
```
[ProxyList]
http 127.0.0.1 7890
```



### 设置、取消代理
export https_proxy=http://127.0.0.1:7890;export http_proxy=http://127.0.0.1:7890;export all_proxy=socks5://127.0.0.1:7890
unset https_proxy;unset http_proxy;unset all_proxy

### 可能出现的问题
1. 没有c语言编译环境：error: install a C compiler and library
sudo apt-get install gcc
2. 