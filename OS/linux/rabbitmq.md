1. 统一erlang.cookie 文件
    > scp /var/lib/rabbitmq/.erlang.cookie ${user}@${ip}:/var/lib/rabbitmq/.erlang.cookie
2. 添加rabbitmq集群节点 在m2执行
    > rabbitmqctl stop_app  暂停app  
   > rabbitmqctl join_cluster -ram rabbit@m2 加入集群
   > rabbitmqctl start_app  启动app   
   > systemctl restart rabbitmq-server.service  重启服务  
   > rabbitmqctl cluster_status  查看集群状态
   


安装 HAProxy
1. yuminstall haproxy -y
2. 配置文件 /etc/haproxy/haproxy.cfg
3. 启动 systemctl start haproxy
4. systemctl status haproxy.service

```text
maxconn 3000
...
# 对MQ集群进行监听
listen rabbitmq_cluster
    bind 0.0.0.0:5672
    option clitcpka
    timeout connect 1s
    timeout client 10s
    timeout server 10s
    balance roundrobin
    server node1 ip:5672 check inter 5s rise 2 fall 3
    server node2 ip:5672 check inter 5s rise 2 fall 3
#开启haproxy监控服务
listen http_front
    bind 0.0.0.0:1090
    stats refresh 30s
    stats url /haproxy_stats
    stats auth admin:admin    
    
```