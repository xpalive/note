### crontab linux 定时任务
crontab -e 编辑
crontab -l 查看
service cron restart 重启服务
systemctl status cron 检测cron状态
systemctl start cron 启动cron

如果你的crontab文件中仍然没有执行命令，则可能是cron服务出现了故障。你可以通过查看错误日志文件来找出更多信息。常用的日志文件有 /var/log/cron、/var/log/syslog 或 /var/log/messages。

cron中的命令可能需要加入 sudo bash 等
```shell
*/10 * * * * sudo bash /usr/local/bin/raspberry_job.sh > /dev/null 2>&1
```

### 查看代理
evn | grep -I proxy

### 查看磁盘空间  
> dh -f

### 查看内存空间
> top 后输入M
> free -h

### 取消蜂鸣
> 首先是命令行的  
> vim /etc/inputrc  
> 然后将set bell-style none前面的#删掉  
> :wq 保存退出  

> 然后是vim的  
> vim /etc/bashrc  
> 在开始的地方加上一句 setterm -blength 0  
> :wq
> source /etc/bashrc

### 免密登录
> ssh-keygen -t rsa  
> ssh-copy-id root@192.168.10.80  
> 如果没有 ssh-keygen 就需要安装 openssh  
> ssh-keygen -R 公钥信息清除

### 关闭防火墙
> systemctl stop firewall.service  

### 界面模式
更改模式命令：
> systemctl set-default graphical.target由命令行模式更改为图形界面模式  
> systemctl set-default multi-user.target由图形界面模式更改为命令行模式  
 
### 文件写入
> cat 22 >> 11 追加
> cat 22 > 11 覆盖



### 判断文件或文件夹是否存在
> https://www.jb51.net/article/186273.htm

### shell 脚本的建议
> 使用set -u 或者 set -o nounset
> 使用set -e
> https://www.cnblogs.com/my_life/articles/7085461.html?ivk_sa=1024320u~~

### scp
-P 可指定端口
-r 拷贝文件夹

拷贝文件至远程服务器
scp file username@remote_ip:/home/data
拷贝远程服务器文件至本地
scp username@remote_ip:/home/data file

scp -P 123 -r /dir user@remote_ip:/home/data/
将目录/dir 拷贝到远程/home/data/ 下

### 修改主机名字
hostnamectl set-hostname m1

### 判断
```shell
-e 表示只要filename存在,则为真,不管filename是什么类型,当然这里加了!就取反

-e filename 如果 filename存在,则为真
-d filename 如果 filename为目录,则为真
-f filename 如果 filename为常规文件,则为真
-L filename 如果 filename为符号链接,则为真
-r filename 如果 filename可读,则为真
-w filename 如果 filename可写,则为真
-x filename 如果 filename可执行,则为真
-s filename 如果文件长度不为0,则为真
-h filename 如果文件是软链接,则为真
```

### 命令补全
yum install -y bash-completion


### history
history 显示执行过的命令
history | grep 'hs' 查询包含hs的命令
```shell
# 例1
!hs:p 打印命令不执行
# 例2
!1:p 打印序号为1的命令不执行
!1 执行序号为1的命令
```

### bash 执行命令
bash -c "cmd string"
如果使用-c参数，bash会从第一个非选项参数后面的字符串中读取命令，如果字符串有多个空格，
第一个空格前面的字符串是要执行的命令，也就是$0,后面的是参数，即$1,$2...
`使用bash -c 注意点`
bash -c "cmd string" 接的是shell命令字符串，用双引号括起来
bash -c "/path/to/file" 写文件的绝对路径，用双引号括起来，且文件要具有可执行权限 

### 启动顺序
systemctl get-default
systemctl set-default graphical.target
systemctl set-default multi-user.target

### 启用sudo -s 免密
vim /etc/sudoers
-- 用户不需要密码
${user} ALL=(ALL) NOPASSWD:ALL
-- 用户组不需要密码
%admin ALL=(ALL) NOPASSWD:ALL
-- 某些命令不需要密码
${user} ALL=(ALL) NOPASSWD: /bin/kill,/bin/rm

注意：/etc/sudoers文件遵守的规则是通用覆盖规则，后面的规则会覆盖前面的规则
所以设置用户免密时需要放在文件的最后  

修改visudo命令的默认编辑器
在/etc/sudoers文件头部添加`Defaults editor=/usr/bin/vim`

### 端口查看
netstat -tpnl |grep 5432

### 查看cpu信息
cat /proc/cpuinfo | grep "model name" | uniq
cat /proc/cpuinfo | grep "cpu MHz" | uniq

### 修改权限
chmod  修改权限
chgrp  修改组
chown  修改所属用户

### 关于2>&1 &的详细解释[https://blog.csdn.net/astonqa/article/details/8252791]
1、首先，bash中0，1，2三个数字分别代表STDIN_FILENO、STDOUT_FILENO、STDERR_FILENO，即标准输入（一般是键盘），标准输出（一般是显示屏，准确的说是用户终端控制台），标准错误（出错信息输出）。

2、输入输出可以重定向，所谓重定向输入就是在命令中指定具体的输入来源，譬如 cat < test.c 将test.c重定向为cat命令的输入源。输出重定向是指定具体的输出目标以替换默认的标准输出，譬如ls > 1.txt将ls的结果从标准输出重定向为1.txt文本。有时候会看到如 ls >> 1.txt这类的写法，> 和 >> 的区别在于：> 用于新建而>>用于追加。即ls > 1.txt会新建一个1.txt文件并且将ls的内容输出到新建的1.txt中，而ls >> 1.txt则用在1.txt已经存在，而我们只是想将ls的内容追加到1.txt文本中的时候。
3、默认输入只有一个（0，STDIN_FILENO），而默认输出有两个（标准输出1 STDOUT_FILENO，标准错误2 STDERR_FILENO）。因此默认情况下，shell输出的错误信息会被输出到2，而普通输出信息会输出到1。但是某些情况下，我们希望在一个终端下看到所有的信息（包括标准输出信息和错误信息），要怎么办呢？

   对了，你可以使用我们上面讲到的输出重定向。思路有了，怎么写呢？ 非常直观的想法就是2>1（将2重定向到1嘛），行不行呢？试一试就知道了。我们进行以下测试步骤：

1）mkdir test && cd test   ; 创建test文件夹并进入test目录

2）touch a.txt b.c c       ; 创建a.txt b.c c 三个文件

3）ls > 1                  ; 按我们的猜测，这句应该是将ls的结果重定向到标准输出，因此效果和直接ls应该一样。但是实际这句执行后，标准输出中并没有任何信息。

4）ls                      ; 执行3之后再次ls，则会看到test文件夹中多了一个文件1

5）cat 1                   ; 查看文件1的内容，实际结果为：1 a.txt b.c c     可见步骤3中 ls > 1并不是将ls的结果重定向为标准输出，而是将结果重定向到了一个文件1中。即1在此处不被解释为STDOUT_FILENO，而是文件1。

4、到了此时，你应该也能猜到2>&1的用意了。不错，2>&1就是用来将标准错误2重定向到标准输出1中的。此处1前面的&就是为了让bash将1解释成标准输出而不是文件1。至于最后一个&，则是让bash在后台执行。


### 抓包命令 tcpdump
tcpdump -i any tcp port 8081 -w ws.pcap

### pkill 命令
pkill -9 tcpdump