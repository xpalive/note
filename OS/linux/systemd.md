### Type字段/指令
> simple（默认值）：ExecStart字段启动的进程为主进程  
forking：ExecStart字段将以fork()方式启动，此时父进程将会退出，子进程将成为主进程  
oneshot：类似于simple，但只执行一次，Systemd 会等它执行完，才启动其他服务  
dbus：类似于simple，但会等待 D-Bus 信号后启动  
notify：类似于simple，启动结束后会发出通知信号，然后 Systemd 再启动其他服务  
idle：类似于simple，但是要等到其他任务都执行完，才会启动该服务。一种使用场合是为让该服务的输出，不与其他服务的输出相混合

> 例如：由于zookeeper自身的启动命令是shell脚本启动jar，属于forking一类。  


#### forking类型
> 使用Type=forking时，要求ExecStart启动的命令自身就是以daemon模式运行的。
> 而以daemon模式运行的进程都有一个特性：总是会有一个瞬间退出的中间父进程，
> 例如，nginx命令默认以daemon模式运行，所以可直接将其配置为forking类型：

#### simple类型
> Type=simple是一种最常见的通过systemd服务系统运行用户自定义命令的类型，也是省略Type指令时的默认类型。
> Type=simple类型的服务只适合那些在shell下运行在前台的命令。也就是说，当一个命令本身会以daemon模式运行时，
> 将不能使用simple，而应该使用Type=forking。比如ls命令、sleep命令、非daemon模式运行的nginx进程
> 以及那些以前台调试模式运行的进程，在理论上都可以定义为simple类型的服务。


### systemd 环境变量
1、/etc/profile或者/etc/security/limit.d这些文件中配置的环境变量仅对通过pam登录的用户生效，而systemd是不读这些配置的，所以这就造成登录到终端时查看环境变量和手动启动应用都一切正常，但是systemd无法正常启动应用

2、如果环境变量只有某个服务使用，可以在服务里用  Environment  或 EnvironmentFile来单独为该服务的进程设置环境变量。
参考：http://www.jinbuguo.com/systemd/systemd.exec.html#%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F

3、如果需要给systemd配置全局的默认参数，全局的配置在/etc/systemd/system.conf和/etc/systemd/user.conf中。同时还会加载两个配置文件对应的目录中所有的.conf配置文件/etc/systemd/system.conf.d/.conf和/etc/systemd/user.conf.d/.conf，一般的服务单元使用system.conf即可。加载优先级system.conf最低，所以system.conf.d目录中的配置会覆盖system.conf的配置。
因为全局环境变量是设置的systemd（pid=1）进程的，而所有的服务进程都是该systemd的子进程(或孙子等)，所以对所有服务生效。


### 引用
http://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-part-two.html
https://www.junmajinlong.com/linux/index/#systemd
[systemd 环境变量]https://blog.csdn.net/z1026544682/article/details/108382900