### 解压
```shell
tar -xvf file.tar //解压 tar包
tar -xzvf file.tar.gz //解压tar.gz
tar -xjvf file.tar.bz2 //解压 tar.bz2
tar -xZvf file.tar.Z //解压tar.Z
unrar e file.rar //解压rar
unzip file.zip //解压zip

.tar 用 tar -xvf 解压
.gz 用 gzip -d或者gunzip 解压
.tar.gz和.tgz 用 tar -xzf 解压
.bz2 用 bzip2 -d或者用bunzip2 解压
.tar.bz2用tar -xjf 解压
.Z 用 uncompress 解压
.tar.Z 用tar -xZf 解压
.rar 用 unrar e解压
.zip 用 unzip 解压
```