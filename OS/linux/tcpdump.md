### tcpdump
抓包命令
```shell
tcpdump [option] [proto] [dir] [type]
```
option 可选参数：将在后边一一解释，对应本文 第四节：可选参数解析
proto 类过滤器：根据协议进行过滤，可识别的关键词有：upd, udp, icmp, ip, ip6, arp, rarp,ether,wlan, fddi, tr, decnet
type 类过滤器：可识别的关键词有：host, net, port, portrange，这些词后边需要再接参数。
direction 类过滤器：根据数据流向进行过滤，可识别的关键字有：src, dst，同时你可以使用逻辑运算符进行组合，比如 src or dst


tcpdump -iany -s0 port sip端口 or port  http端口 or port  统一设备端口 or port  kafka端口 -w device.pcap -v

### 参考
https://baijiahao.baidu.com/s?id=1671144485218215170&wfr=spider&for=pc