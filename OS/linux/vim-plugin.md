### plugin
vim-plugin
https://github.com/junegunn/vim-plug

gruvbox
https://github.com/morhetz/gruvbox

### 安装
1. vim-plugin 安装
```shell
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
2. 配置插件
在~/.vimrc文件中添加
```shell
" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-sensible'
Plug 'junegunn/seoul256.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
```
3. 安装插件
:PlugInstall

4. gruvbox 安装
在vim-plugin中添加 Plug 'morhetz/gruvbox'
5. 设置gruvbox
autocmd vimenter * ++nested colorscheme gruvbox