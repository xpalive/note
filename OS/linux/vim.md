### vim 字符集
:set ff
:set ff=unix

### vim 设置
> ~/.vimrc 或 /etc/vim/vimrc

vimrc 的注释 ” this is a comment in vimrc It doesn't have close quote
```
set syntax=on " 语法高亮
set noeb " 去掉输入错误的提示声音
set termencoding=utf-8
set encoding=utf-8
set fileencodings=utf-8
set number
set shiftwidth=4 " 缩进长度
set softtabstop=4 " 删除缩进的长度
set textwidth=200 " 文本宽度
colorscheme desert " 配色
```
[参数]https://www.cnblogs.com/XNQC1314/p/8692993.html
### vim 快捷键

####命令模式下
> u 撤回  
> ctrl+r 恢复撤回    
> i 输入  
> x 保存修改并退出  
> w 写入  
> q 退出  
> wq! 强制保存
> q! 强制退出
>

```text
1. 选定文本块。使用v进入可视模式，移动光标键选定内容。 

2.复制的命令是y，即yank（提起） ，常用的命令如下： 
    y      在使用v模式选定了某一块的时候，复制选定块到缓冲区用； 
    yy    复制整行（nyy或者yny ，复制n行，n为数字）； 
    y^   复制当前到行头的内容； 
    y$    复制当前到行尾的内容； 
    yw   复制一个word （nyw或者ynw，复制n个word，n为数字）； 
    yG    复制至档尾（nyG或者ynG，复制到第n行，例如1yG或者y1G，复制到档尾）  
    
3. 剪切的命令是d，即delete，d与y命令基本类似，所以两个命令用法一样，包括含有数字的用法.  
    d      剪切选定块到缓冲区； 
    dd    剪切整行 
    d^    剪切至行首 
    d$     剪切至行尾 
    dw    剪切一个word 
    dG     剪切至档尾  
    
4. 粘贴的命令式p，即put（放下） 
    p      小写p代表贴至游标后（下），因为游标是在具体字符的位置上，所以实际是在该字符的后面 
    P      大写P代表贴至游标前（上） 
    整行的复制粘贴在游标的上（下）一行，非整行的复制则是粘贴在游标的前（后）
5. 替换
:%s+/mydata/+/docker_app/+g 全文替换   (分隔符可以是任意符号+,/,#,@)
转义符为\ （/ -> \/ , " -> \" , \ -> \\ ,' -> \'）
```

```shell
:set number #显示行号 
:set nonumber # 关闭行号
:set background=dark # 设置背景颜色
:set guifont=consolas:h14 # 设置字体为 consolas 字号为 14
:set history=700 # 设置保存命令的行数
:set autoread # 设置当文件变化时，自动读取新文件
:set wrap 或:set nowrap # 启动折行或禁止折行
:set ff=unix 或 :set ff=dos # 切换文件格式为Unix格式，每行以\n结尾；切换文件格式为Windows格式，每行以\r\n结尾
:set encoding=utf-8 # 设置 Vim 展示文本时的编码格式
:set [keyword]? # 查看当前状态的命令，如:set ff?
:set ignorecase 或:set noignorecase # 忽略或不忽略大小写的查找
:set hlsearch或:set nohlsearch # 打开或关闭高亮显示搜索结果
:set incsearch # 逐步搜索模式，对当前键入的字符进行搜索而不必等待键入完成
:set wrapscan # 重新搜索，在搜索到文件头或尾时，返回继续搜索，默认开启
```

参考
https://blog.csdn.net/weixin_55000908/article/details/123836966