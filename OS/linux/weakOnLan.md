### weak on lan
> 网络唤醒电脑 本质是通过网卡收到唤醒数据包进行电脑唤醒

### 步骤
1. 设置路由器，绑定网络唤醒的电脑ip（最重要的是绑定mac地址）
2. 在主板bios中打开网络环境的配置（weak on lan -> enable）

#### 系统端配置
##### Ubuntu 20.04.6 os
1. ip add 命令查看网卡mac地址及网卡名称
2. 使用ethtool 开启网络唤醒功能
3. 查看网卡网络唤醒状态 sudo ethtool $网卡名称$ | grep weak-on 
   1. 安装[ethtool-install](/OS/linux/install/ethtool-install.sh)
4. 安装systemd服务启动脚本[wol-systemd](/OS/linux/install/wol-systemd-install.sh)
5. 启动服务
   1. sudo systemctl enable wol.service
   2. sudo systemctl start wol.service

##### windows 10
华硕主板设置：高级 -> 高级电源管理
erp ready 设置 S5
cec ready 设置 开启
有PCI-E/PCI设备唤醒 设置 开启







