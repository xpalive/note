### 使用 -O 选项以其他名称保存下载的文件
要以其他名称保存下载的文件，使用-O选项，后跟指定名称即可：

wget -O redis.tar.gz https://download.redis.io/releases/redis-6.0.8.tar.gz


### 使用 -P 选项将文件下载到指定目录
默认情况下，wget将下载的文件保存在当前工作目录中，使用-P选项可以将文件保存到指定目录下，例如，下面将将文件下载到/usr/software目录下：

wget -P /usr/software https://download.redis.io/releases/redis-6.0.8.tar.gz


### 使用 -c 选项断点续传
当我们下载一个大文件时，如果中途网络断开导致没有下载完成，我们就可以使用命令的-c选项恢复下载，让下载从断点续传，无需从头下载。

wget -c https://download.redis.io/releases/redis-6.0.8.tar.gz


### 使用 -b 选项在后台下载
我们可以使用-b选项在后台下载文件：

wget -b https://download.redis.io/releases/redis-6.0.8.tar.gz


默认情况下，下载过程日志重定向到当前目录中的wget-log文件中，要查看下载状态，可以使用tail -f wget-log查看。

### 使用 -i 选项下载多个文件
如果先要一次下载多个文件，首先需要创建一个文本文件，并将所有的url添加到该文件中，每个url都必须是单独的一行。

vim download_list.txt

然后使用-i选项，后跟该文本文件：

wget -i download_list.txt


### 使用 --limit-rate 选项限制下载速度
默认情况下，wget命令会以全速下载，但是有时下载一个非常大的资源的话，可能会占用大量的可用带宽，影响其他使用网络的任务，这时就要限制下载速度，可以使用--limit-rate选项。例如，以下命令将下载速度限制为1m/s：

wget --limit-rate=1m https://download.redis.io/releases/redis-6.0.8.tar.gz

### 使用 -U 选项设定模拟下载
如果远程服务器阻止wget下载资源，我们可以通过-U选项模拟浏览器进行下载，例如下面模拟谷歌浏览器下载。

wget -U 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.43 Safari/537.36' https://download.redis.io/releases/redis-6.0.8.tar.gz


### 使用 --tries 选项增加重试次数
如果网络有问题或下载一个大文件有可能会下载失败，wget默认重试20次，我们可以使用-tries选项来增加重试次数。

wget --tries=40 https://download.redis.io/releases/redis-6.0.8.tar.gz

通过FTP下载如果要从受密码保护的FTP服务器下载文件，需要指定用户名和密码，格式如下：

wget --ftp-user=<username> --ftp-password=<password> url