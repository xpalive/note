### 安装vim
卸载默认的
sudo apt-get remove vim-common
安装新的
sudo apt-get install vim

### 修改默认的pi账号密码
su passwd
or
sudo passwd root

### 免密登录
ssh-copy-id root@192.168.10.113

### 代理安装

### 安装rzsz命令
sudo apt-get update
sudo apt-get install lrzsz

### proxychain 安装
[proxychain安装](/OS/linux/install/proxychain4-install-zip.sh)

### java环境安装
[Java环境安装](/OS/linux/install/java-install.sh)

### 查看树莓派硬件版本
pinout
cat /proc/device-tree/model