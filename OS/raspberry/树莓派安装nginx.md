### 树莓派安装nginx
#### 更换清华源
> 清华园镜像网站：https://mirrors.tuna.tsinghua.edu.cn/
> 树莓派镜像：https://mirror.tuna.tsinghua.edu.cn/help/raspbian/

编辑 /etc/apt/sources.list 内容如下 
```shell
deb https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ bullseye main non-free contrib rpi
deb-src https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ bullseye main non-free contrib rpi
deb [arch=arm64] https://mirrors.tuna.tsinghua.edu.cn/raspbian/multiarch/ bullseye main
```
> 最新内容 https://mirror.tuna.tsinghua.edu.cn/help/raspbian/

编辑 /etc/apt/sources.list.d/raspi.list 内容如下
```shell
deb https://mirrors.tuna.tsinghua.edu.cn/raspberrypi/ bullseye main
```
> 最新内容 https://mirror.tuna.tsinghua.edu.cn/help/raspberrypi/

#### 查看linux信息
lsb_release -a
uname -a

#### 添加nginx源 具体步骤
> nginx 官方网站：https://nginx.org/en/linux_packages.html （Debian）

Install the prerequisites:
```shell
sudo apt install curl gnupg2 ca-certificates lsb-release debian-archive-keyring
```

Import an official nginx signing key so apt could verify the packages authenticity. Fetch the key:
安装证书钥匙
```shell
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor \
| sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null
```
Verify that the downloaded file contains the proper key:
验证秘钥
```shell
gpg --dry-run --quiet --no-keyring --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg
```
The output should contain the full fingerprint 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 as follows:
```shell
pub   rsa2048 2011-08-19 [SC] [expires: 2024-06-14]
573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62
uid                      nginx signing key <signing-key@nginx.com>
```
If the fingerprint is different, remove the file.

To set up the apt repository for stable nginx packages, run the following command:
安装nginx源
```shell
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
http://nginx.org/packages/debian `lsb_release -cs` nginx" \
| sudo tee /etc/apt/sources.list.d/nginx.list
```
If you would like to use mainline nginx packages, run the following command instead:

echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] \
http://nginx.org/packages/mainline/debian `lsb_release -cs` nginx" \
| sudo tee /etc/apt/sources.list.d/nginx.list
Set up repository pinning to prefer our packages over distribution-provided ones:

echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" \
| sudo tee /etc/apt/preferences.d/99nginx
To install nginx, run the following commands:

sudo apt update
sudo apt install nginx


#### 报错
```
Q：Skipping acquire of configured file 'nginx/binary-armhf/Packages' as repository 'http://nginx.
org/packages/mainline/debian bullseye InRelease' doesn't support architecture 'armhf'

A：这个错误消息表示你的树莓派设备架构（architecture）为 'armhf'，但是你正在尝试从 nginx.org 的源中安装软件包，而该源不支持 'armhf' 架构。通常，Nginx 官方源主要支持 x86 架构，而不支持 
ARM 架构的设备
```

```shell
W: GPG error: https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian bullseye InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 9165938D90FDDD2E
E: The repository 'https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian bullseye InRelease' is not signed.

W: GPG error: https://mirrors.tuna.tsinghua.edu.cn/raspbian/multiarch bullseye InRelease: The following 
signatures couldn't be verified because the public key is not available: NO_PUBKEY E77FC0EC34276B4B`

上述错误通过添加key解决
gpg --keyserver  keyserver.ubuntu.com --recv-keys E77FC0EC34276B4B
gpg --export --armor E77FC0EC34276B4B | sudo apt-key add -

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9165938D90FDDD2E
```
