### 安装步骤
安装依赖环境
sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install libtool
sudo apt-get install sysv-rc-conf
sudo apt-get install libpcre3 libpcre3-dev
sudo apt-get install libperl-dev
sudo apt-get install zlib1g-dev
sudo apt-get install openssl libssl-dev

创建目录
mkdir -p /usr/local/nginx

下载并解压
zlib-1.3.tar.gz(https://zlib.net/zlib-1.3.tar.gz)

配置
./configure --prefix=/usr/local/nginx --with-http_ssl_module --with-pcre --with-zlib=../zlib-1.3

编译
make

安装
sudo make install

### 参考
https://blog.csdn.net/Hallo_ween/article/details/107836013