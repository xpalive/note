### 安装搜狗拼音
1. sudo apt update   更新源
2. sudo apt install fcitx 安装fcitx输入法框架
3. setting -> Region and language -> Manage installed Languages -> keyboard input method system set:fcitx 设置输入法框架为fcitx
4. sudo cp /usr/share/applications/fcitx.desktop /etc/xdg/autostart/ 设置开机启动
5. sudo apt purge ibus 卸载ibus输入框架
6. 下载搜狗输入法
7. sudo dpkg -i sogouxxxxxxxx.deb 安装
8. sudo apt install libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2
9. sudo apt install libgsettings-qt1
10. 重启电脑配置搜狗输入法，注意 only show current language 配置项

reference
https://shurufa.sogou.com/linux/guide

### ctrl + shift + f 冲突问题
vim ~/.config/sogoupinyin/conf/env.ini  修改文件中 ShortCutFanJian=0
vim ~/.config/fcitx/conf/fcitx-chttrans.config  修改文件中 Hotkey=CTRL_SHIFT_F 为任意值CTRL_SHIFT_P_P
重新登录（log out and log back in）
