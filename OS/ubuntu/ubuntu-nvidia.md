### 查看显卡型号
ubuntu-drivers devices
### 禁用nouveau
```shell
# （1）验证是否禁用 nouveau
lsmod | grep nouveau

# （2）若有输出，说明没有禁用，进行以下操作禁用
sudo gedit /etc/modprobe.d/blacklist.conf

# （3）在文件末尾中添加两条：
blacklist nouveau
options nouveau modeset=0

# （4）更新配置
sudo update-initramfs -u  

# （5）重启系统

# （6）查看是否禁用 nouveau，若无输出，则已禁用
lsmod | grep nouveau
```

### nvidia驱动更新
```shell
# （1）添加ppa源
sudo add-apt-repository ppa:graphics-drivers/ppa

#（2）更新apt-get
sudo apt update

# （3）查看更新后可以安装的显卡驱动版本
ubuntu-drivers devices  

# （4）安装推荐的显卡驱动
sudo apt-get install nvidia-430
```
### 查看显卡驱动
nvidia-smi
### 启动显卡驱动配置界面
nvidia-settings 

### 安装官方驱动
下载官方驱动脚本  
执行命令  
sudo ./NVIDIA-Linux-x86_64-470.57.02.run -no-x-check -no-nouveau-check -no-opengl-files  
参数说明：
1. –no-x-check：表示安装驱动时不检查X服务，非必需，我们已经禁用图形界面。
2. –no-nouveau-check：表示安装驱动时不检查nouveau，非必需，我们已经禁用驱动。
3. –no-opengl-files：表示只安装驱动文件，不安装OpenGL文件。这个参数不可省略，否则会导致登陆界面死循环，英语一般称为”login loop”或者”stuck in login”。

#### 问题
在安装NVIDIA显卡驱动文件时出现报错，查看日志文件
1. 可能是gcc版本不正确，安装高版本的gcc即可

参考
https://blog.csdn.net/m0_37605642/article/details/119651996