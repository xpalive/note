### 设置root密码,初次使用Ubuntu没有root密码，使用如下命令设置密码
sudo passwd 

### 配置域名
https://blog.csdn.net/agq358/article/details/122250405

### sudo免密配置
vim /etc/sudoers
添加配置 xiongping ALL=(ALL) NOPASSWD: ALL
如果不生效可能是被其他配置给覆盖了，如：
%sudo	ALL=(ALL:ALL) AL
或者是 /etc/sudoers.d 目录下的配置文件
可以将配置放在最后一行

### ras秘钥生成,免密登录
```shell
ssh-keygen -t rsa -C xpalive@gmail.com
ssh-copy-id root@192.168.10.80
ssh-keygen -R 公钥信息清除
```

### 查看系统信息
1. cat/etc/os-release
2. lsb_release -a
3. uname -a

### 查看cpu信息
1. cat /proc/cpuinfo |grep MHz|uniq 

### 更新软件
sudo apt update
sudo apt list --upgradable
sudo apt upgrade -y

### 查询、安装、卸载、软件
dpkg -l 查看安装的所有软件
dpkg -l | grep ftp
whereis ftp 查看ftp安装的路径
aptitude show ftp 查看ftp软件版本
sudo apt-get remove openjdk 卸载openjdk
sudo apt-get purge openjdk* 移除所有的openjdk包

### desktop categories
https://blog.csdn.net/shawzg/article/details/106943100

### 修改软件源（阿里源）
[阿里开发者社区][https://developer.aliyun.com]
[阿里镜像站][https://developer.aliyun.com/mirror/]

修改软件源内容，修改前先备份
cp /etc/apt/sources.list /etc/apt/sources.list.bak
vim /etc/apt/sources.list
替换默认的http://archive.ubuntu.com为https://mirrors.aliyun.com

替换方法:sudo sed -i 's/http:\/\/cn.archive.ubuntu.com/https:\/\/mirrors.aliyun.com/g' /etc/apt/sources.list

### 修改终端显示
~/.bashrc
修改一下内容
```shell
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:    \[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
# u 用户名
# h host
source ~/.bashrc
```

### 安装ant
下载可执行文件Binary distributions
解压
配置环境变量
vim /etc/profile
PTAH=$ANT_HOME/bin

### 安装cmake
下载可执行文件Binary distributions:cmake-3.24.2-linux-x86_64.tar.gz
解压
配置环境变量
vim /etc/profile
PTAH=$CMAKE_HOME/bin

注意：配置软链接 `ln -s [目标目录] [软连接地址]` 如果未配置无法在sudo下执行
ln -s $CMAKE_HOME/bin/cmake /usr/local/bin

### 使用sudo命令需要在 secure_path下
sudo vim /etc/sudoers

### 安装gcc-g++
sudo apt install build-essential
或者
sudo apt-get install gcc,sudo apt-get install g++

### 环境变量配置 
vim /etc/profile
PATH=$JAVA_HOME

export PATH

~/.bashrc
/etc/bashrc 

### 命令补全
   ubuntu : sudo apt-get install bash-complection
### ubuntu 开启ssh
安装服务 apt-get openssh-server
>报错 sshd: no hostkeys available -- exiting.ssh start
  ```shell script
  生成key
  ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
  ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
  重启服务
  service ssh restart
  ```
  ```shell script
    /etc/sshd_config 文件配置
    PermitRootLogin test 是否允许root用户登录 
    PubkeyAuthentication no
    AuthorizedKeysFile 屏蔽
    PasswordAuthentication yes  
  ```

> 修改/etc/ssh/sshd-config文件，  
> 将其中的PermitRootLogin no修改为yes，
> PubkeyAuthentication yes修改为no，
> AuthorizedKeysFile .ssh/authorized_keys前面加上#屏蔽掉，
> PasswordAuthentication no修改为yes就可以了。

#### 设置代理
export http_proxy=ip地址:端口号
export https_proxy=ip地址:端口号

unset http_proxy
unset https_proxy

### 创建用户
在创建用户时没有指定用户组，系统默认会创建一个同名的用户组，用户userone不存在，但是用户组userone已经存在，所以会有这个错误。
```shell
useradd: group docker exists - if you want to add this user to that group, use -g.
```
sudo useradd -g docker docker
sudo useradd -m -g docker docker
-m 添加home目录

### 新创建的用户只有$符号
修改 vim /etc/passwd 
找到用户记录，修改后面的指令`:/bin/sh` 修改为 `:/bin/bash`
修改默认配置 vim /etc/default/useradd
修改SHELL=/bin/bash

### 软件升级命令
```shell
sudo apt update
sudo apt --only-upgrade install google-chrome-stable
```

### Ubuntu 20.04 temporary failure in name resolution for wired
sudo vim /etc/resolv.conf
Add this line to the file: nameserver 1.1.1.1
sudo systemctl restart systemd-resolved.service 
修改上述文件重启后会失效，可以修改如下文件
sudo vim /etc/systemd/resolved.conf
DNS= 1.1.1.1

### Ubuntu 系统中汉字中文显示为日文字形（汉字显示不正确）
配置fontconfig
修改配置文件 /etc/fonts/conf.d/64-language-selector-prefer.conf
1. 备份配置文件 cp  /etc/fonts/conf.d/64-language-selector-prefer.conf  /etc/fonts/conf.d/64-language-selector-prefer.conf.bak
2. 修改配置文件的内容，将内容中 <family>Noto Sans Mono CJK SC</family> 放在最前面：
3. ```xml
   <prefer>
       <family>Noto Sans Mono CJK SC</family>
       <family>Noto Sans Mono CJK TC</family>
       <family>Noto Sans Mono CJK HK</family>
       <family>Noto Sans Mono CJK JP</family>
       <family>Noto Sans Mono CJK KR</family>
   </prefer>
   ```

### deb安装包安装
sudo dpkg -i sogouxxxxxxxx.deb 安装

### ubuntu下快速打开当前路径下的图形界面
nautilus . 或者 xdg-open .
