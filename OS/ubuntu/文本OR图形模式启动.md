### 文本OR图形模式切换
#### 界面模式
更改模式命令：
> systemctl set-default graphical.target由命令行模式更改为图形界面模式  
> systemctl set-default multi-user.target由图形界面模式更改为命令行模式
> systemctl get-default