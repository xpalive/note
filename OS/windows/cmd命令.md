### 格式化硬盘
> diskpart  进入管理磁盘命令  
> list disk  查看磁盘  
> select 1  选择磁盘  
> clean 删除磁盘
> 


### 管道过滤
findstr 命令
/i 忽略大小写
```shell
java -XX:PrintFlagsFinal | findstr /i PrintGC
```

### 端口占用 并杀死
netstat -ano | findstr "8080"
taskkill /f /t /im "5784"

### wmic
wmic process where (commandLine like '%vlc.exe%' and Caption = 'vlc.exe') get processid,caption,commandLine

wmic process where (commandLine like '%vlc.exe%' and Caption = 'vlc.exe') delete
wmic process where (commandline like '%zookeeper%' and not name='wmic.exe') delete
### tar
dos cmd bat 批处理