### 代理
~/.ssh/config
```text
# 必须是 github.com
Host github.com
   HostName github.com
   User git
   # 走 HTTP 代理
   # ProxyCommand socat - PROXY:127.0.0.1:%h:%p,proxyport=8080
   # 走 socks5 代理（如小飞机 or V2xxx）
   # ProxyCommand nc -v -x 127.0.0.1:1080 %h %p
```

>对于 Windows 用户，要使用 socks5 代理却没有 nc 的，可以将
ProxyCommand nc -v -x 127.0.0.1:1080 %h %p
换成 # -S 为 socks, -H 为 HTTP
ProxyCommand connect -S 127.0.0.1:1080 %h %p

如果你是在Linux下配置，那么应该为
> ProxyCommand nc --proxy-type socks5 --proxy 127.0.0.1:7891 %h %p  
> ncat和netcat有些参数上的区别，见  
> https://unix.stackexchange.com/questions/368155/what-are-the-differences-between-ncat-nc-and-netcat

单次clone 使用代理
clone: git clone -c http.proxy="127.0.0.1:xxxx" https://github.com/Gump8/xxxx.git    
fetch upstream: git -c http.proxy="127.0.0.1:xxxx" fetch upstream   
*注意： fetch 后面不能 -c，clone 是可以的。
