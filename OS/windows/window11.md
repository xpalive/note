### bring back classic context menu windows 11
1. search for regEdit and open Registry.
2. Navigate to the following path: HKEY_CURRENT_USER\SOFTWARE\CLASSES\CLSID
3. right-click the CLSID key, create key named: {86ca1aa0-34aa-4e8b-a509-50c905bae2a2}
4. right-click the new key, create key named: InprocServer32 and set the value to blank
5. restart the computer(important)