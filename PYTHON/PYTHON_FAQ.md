### ValueError: check_hostname requires server_hostname 这个错误怎么解决
设置代理
set http_proxy=http://127.0.0.1:7890 & set https_proxy=http://127.0.0.1:7890

### python 使用venv虚拟环境做环境隔离
python3.6及以上已默认安装
如果没有安装可以安装python3-venv
1. 创建虚拟环境
   python -m venv test_env
2. 启动虚拟环境
   linux: source ./test_env/bin/activate
   windows: ./test_env/Scripts/Activate.ps1

### venv环境
激活: .\Scripts\Activate.ps1
退出： deactivate
激活失败未授权问题
Set-ExecutionPolicy Unrestricted -Scope CurrentUser
恢复
Set-ExecutionPolicy Restricted -Scope CurrentUser