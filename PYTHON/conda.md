### conda简介
conda是一个开源包管理体系（python, R, Ruby, Lua, Scala, java, c/c++）  
[官方地址](https://docs.conda.io/projects/conda/en/latest/index.html)
#### conda,miniconda,anaconda的区别
conda < miniconda < anaconda
![img.png](images/conda.png)

#### conda 安装使用
下载[conda安装包](https://conda.io/projects/conda/en/latest/index.html)  
安装完成后，window系统下会创建conda应用（Anaconda prompt `miniconda3`）  
打开Anaconda prompt 开始使用conda

#### conda 配置文件
c:/users/用户/.condarc
```
proxy_servers:
  http: 127.0.0.1:7890
  https: 127.0.0.1:7890
```

#### conda 项目的配置文件 environment.yaml
libedit=3.1.20181209=hc058e9b_0

#### conda包搜索
https://anaconda.org/

`libedit`：这是包的名称，表示需要安装的软件包的标识符。  
`3.1.20181209`：这是包的版本号，表示要安装的软件包的版本。在这个例子中，版本号是 3.1.20181209。  
`hc058e9b_0`：这是构建版本号（Build），用来区分不同版本的相同软件包。构建版本号是由 conda 构建系统生成的，并包括哈希值或其他标识符。在这个例子中，构建版本号是 hc058e9b_0。  

#### window 下 打开 anaconda命令行
搜索anaconda应用

#### 命令
1. 查看信息 conda info 
2. 安装命令 conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia 
   同过-c 指定channel
3. 卸载命令 conda uninstall pytorch
4. 查看虚拟环境 conda info -e
5. 创建虚拟环境 conda create -n $env_name python=$python_version
6. 克隆虚拟环境 conda create -n $new_env_name --clone $old_env_name
7. 激活虚拟环境 conda activate $name
8. 退出虚拟环境 conda deactivate
9. 删除虚拟环境 conda remove --name open-mmlab --all
10. 列出镜像源 conda config --show channels
11. 移除镜像源 conda config --remove channels $source_url
12. 安装conda-pack conda install conda-pack
13. 打包虚拟环境 conda-pack -n $env -o $packageName.tar.gz
14. 移植虚拟环境 tar -xzvf $packageName.tar.gz -C $path_to_restore
15. 根据配置创建虚拟环境 conda env create -f environment.yaml
16. 导出当前虚拟环境配置 conda env export > environment.yaml
17. 更新当前虚拟环境 conda env update -f environment.yaml
18. 查看conda配置 conda config --show
19. 查看conda具体配置 conda config --get <config_key> -> eg:conda config --get channels
20. conda 设置代理 conda config --set proxy_servers.http http://127.0.0.1:7890 | conda config --set proxy_servers.https http://127.0.0.1:7890  
21. conda 移除代理 conda config --remove proxy_servers.http | conda config --remove proxy_servers.https

#### conda 速查表
Quick Start  
![img.png](images/conda_quick_start.png)  
Channels and Packages  
![img.png](images/conda_channels_and_packages.png)  
Working with Conda Environments  
![img.png](images/conda_working_with_conda_environments.png)  
Environment Management  
![img.png](images/conda_environment_management.png)  
Exporting Environments  
![img.png](images/conda_exporting_environments.png)  
Importing Environments  
![img.png](images/conda_importing_environments.png)  
Additional Hints  
![img.png](images/conda_additional_hints.png)  
### reference document
https://zhuanlan.zhihu.com/p/638540652