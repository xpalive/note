### 创建环境
使用conda创建环境
1. conda create -n deepRectangling-py3.6 python=3.6
2. conda activate deepRectangling-py3.6
3. conda install numpy==1.18.5
4. conda install tensorflow==1.13.1
5. 切换磁盘（cd /d d:\）

#### pycharm 环境配置
`File -> setting -> Project:项目名 -> Python Interpreter -> Add Interpreter
-> add Local Interpreter -> Conda Environment ->`  
Conda Executable: C:\Users\xiongping\Miniconda3\condabin\conda.bat    
use existing environment: 选择对应的环境  


#### 查看版本
```
python --version  
python -c "import numpy as np; print(np.__version__)"  
python -c "import tensorflow as tf; print(tf.__version__)"  
```
