### 命令
1. 升级pip命令 python -m pip install -upgrade pip
2. 使用临时源升级pip命令 python -m pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -upgrade pip
3. 设置源 pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
4. 列举当前环境安装的包 pip list  
5. 安装包命令 pip install $package_name=$version 不指定版本则下载最新版本
6. 使用临时源安装包命令 pip install -i https://pypi.tuna.tsinghua.edu.cn/simple $package_name=$version
7. pip install -r requirements.txt --proxy=代理服务器IP:端口号
8. pip install opencv-python==3.4.3.18 --proxy=127.0.0.1:7890

#### pip 包仓库地址
https://pypi.org/

#### 代理设置
```
vim /etc/profile：
    export http_proxy='http://代理服务器IP:端口号'
    export https_proxy='http://代理服务器IP:端口号'
source /etc/profile
或者
export http_proxy='http://代理服务器IP:端口号
export https_proxy='http://代理服务器IP:端口号'
```
单次
```shell
pip install -r requirements.txt --proxy=代理服务器IP:端口号
```

#### window下代理
在C:\User\用户目录下，新建pip文件夹，然后在该文件夹下新建pip.ini文件。填写如下内容：
```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
proxy     = http://XXXX.com:port
[install]
trusted-host=pypi.tuna.tsinghua.edu.cn
```


#### 报错 ValueError: check_hostname requires server_hostname
windows 关闭代理即可，原因不明
为什么使用代理会出现这样的错误呢，小编找到了蛛丝马迹：pip的较新的版本有这样的一个要求，
就是标记对pip仓库的信任，或者使用https进行数据传输（类似的问题小编在学习linux的时候也遇到过）。
而使用代理会导致pip认为你请求的仓库不是安全的，所以安装失败，所以解决方法就是关闭代理，或者在代理中设置相关内容