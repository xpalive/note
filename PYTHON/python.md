### python3.7+字符集问题(WINDOWS ENVIRONMENT)
1. 使用环境变量
   SET PTYHONUTF8=1
2. 使用命令参数
   python xxx.py -X utf8

the python utf-8 mode cna only be enabled at the python startup. Its value can be read from `sys.flags.utf8_mode`