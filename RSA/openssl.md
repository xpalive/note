### openssl 证书生成
#### 生成根证书私钥 pem
openssl genrsa -out privatekey.pem 2048
genrsa 使用RSA算法生成私钥
-out privatekey.pem 证书路径
2048 证书长度
#### 生成证书请求文件 csr
openssl req -new -key privatekey.pem -out privateca.csr
通过privateca.csr文件，到数字证书颁发机构（即CA）申请一个数字证书。CA会给你一个新的文件cacert.pem,这个文件就是数字证书

#### 生成证书文件
自己测试：
openssl req -new -x509 -key privatekey.pem -out cacert.pem -days 1095