### java中使用RSA做非对称加密
#### 概念
X.509 是定义公钥证书格式的标准。 因此，这种格式描述了其他信息中的公钥。
DER是最流行的编码格式，用于在文件中存储 X.509 证书、PKCS8 私钥等数据。这是一种二进制编码，无法使用文本编辑器查看生成的内容。
PKCS8是用于存储私钥信息的标准语法。可以选择使用对称算法对私钥进行加密。
该标准不仅可以处理 RSA 私钥，还可以处理其他算法。PKCS8 私钥通常通过 PEM 编码格式进行交换。
PEM是 DER 证书的 base-64 编码机制。PEM 还可以对其他类型的数据进行编码，例如公钥/私钥和证书请求。
PEM 文件还包含描述编码数据类型的页眉和页脚：
-----BEGIN PUBLIC KEY-----
...Base64 encoding of the DER encoded certificate...
-----END PUBLIC KEY-----

#### 通过openssl生成秘钥对（PEM文件）
openssl genrsa -out privatekey.pem 2048
#### 将私钥转为pkcs8格式
openssl pkcs8 -topk8 -inform PEM -in privatekey.pem -outform PEM -nocrypt -out privatekey_pkcs8.pem
#### 生成公钥
openssl rsa -in privatekey_pkcs8.pem -pubout -out publickey_pkcs8.pem