### arthas 诊断
1. java -jar arthas-boot.jar
2. watch com.zhiwo.biz.donate.service.impl.DonateOrderServiceImpl page "{params,returnObj}" -x 2 -b
   > -b 方法调用前
   > 在watch命令的结果里，会打印出location信息。location有三种可能值：AtEnter，AtExit，AtExceptionExit。对应函数入口，函数正常return，函数抛出异常。
#### arthas使用示例
> watch com.kedacom.ksign.ws.DeviceCtrlWs sendMessageWithoutCorrect '{params[1].Position.Zoom = '2.66',params, 
> returnObj, throwExp}' 'params[0] != null'  -n 5  -x 3

- '{params[1].Position.Zoom = '2.66',params, returnObj,>throwExp}' 表达式
  - params[1].Position.Zoom = '2.66' 修改第二个参数的值
  - params 打印参数
  - returnObj 打印返回值
  - throwExp 打印异常
- 'params[0] != null' 条件
  - 满足第一个参数不为null时才监测sendMessageWithoutCorrect方法
- 参数n 监测的次数
- 参数x 参数的深度

> watch com.kedacom.ksign.service.impl.Ar3dItemServiceImpl queryByPos '{params,returnObj,throwExp}'  -n 5  -x 3

> watch com.kedacom.ksign.ws.DeviceCtrlWs sendMessageWithoutCorrect '{params[1].Position.Zoom = '2.66',params, returnObj,throwExp}'  -x 3

> trace com.kedacom.ksign.ws.DeviceCtrlWs sendMessageWithoutCorrect '{params[1].Position.Zoom = '1.66',params, returnObj,throwExp}'  -n 5  -x 3


### arthas 学习计划
https://arthas.aliyun.com/doc/
