### idea中关闭Actions for URL
settings----> editor---->inlay Hints---->java---->URL path inlay 去掉Show hints

### 自定义注释
```text
* 
* $END$ @kdPublic
* @tag 蛛网服务
* @apiNote $param$
**/
```
获取参数脚本
groovyScript("if(\"${_1}\".length() == 2) {return '';} else {def result='\\n '; def params=\"${_1}\".replaceAll('[\\\\[|\\\\]|\\\\s]', '').split(',').toList();for(i = 0; i < params.size(); i++) {if(i<(params.size()-1)){result+='* @param ' + params[i]  + '\\n '}else{result+='* @param ' + params[i]}}; return result;}", methodParameters()); 


### 在linux下 gnome图形化环境中快捷键配置
Setting -> Keymap -> Main Menu -> Run -> Debugger Actions -> 
Debugging Actions -> Evaluate Expression... -> Quick Evaluate Expression

### Ubuntu下idea配置
1. 下载idea
2. 解压idea
3. 执行idea $idea_home/bin/idea.sh
4. 创建桌面快捷链接,并编辑内容
   touch idea.desktop
   vim idea.desktop
    ```shell
    [Desktop Entry]
    Name=IntelliJ IDEA
    Comment=Intellij IDEA
    Exec=/home/xiongping/app/idea-IC-222.4167.29/bin/idea.sh
    Icon=/home/xiongping/app/idea-IC-222.4167.29/bin/idea.png
    Terminal=false
    Type=Application
    Categories=Developer;
    ```
5. 添加执行权限 chmod u+x idea.desktop


#### ctrl + shift + f 冲突问题
vim ~/.config/sogoupinyin/conf/env.ini  修改文件中 ShortCutFanJian=0
vim ~/.config/fcitx/conf/fcitx-chttrans.config  修改文件中 Hotkey=CTRL_SHIFT_F 为任意值CTRL_SHIFT_P_P
重新登录（log out and log back in）

#### 配置启动命令
在~/.bashrc 下添加命令脚本
```shell
# 指定脚本目录
export IDEA_HOME=/home/xiongping/app/ideaIU-2024.1.4/idea-IU-241.18034.62/bin
# 添加到PTAH中
export PATH=$MAVEN_HOME/bin:$IDEA_HOME:$CLION_HOME:$PATH
```

