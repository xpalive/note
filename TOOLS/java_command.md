### NMT   Native Memory Tracking
1. 开启NMT，VM启动参数添加-XX:NativeMemoryTracking=[off | summary | detail]
* off: 默认关闭
* summary: 只统计各个分类的内存使用情况
* detail: Collect memory usage by individual call sites.

### 通过jcmd查看NMT报告
jcmd <pid> VM.native_memory [summary | detail | baseline | summary.diff | detail.diff | shutdown] [scale= KB | MB | GB]
* summary: 分类内存使用情况.
* detail: 详细内存使用情况，除了summary信息之外还包含了虚拟内存使用情况。
* baseline: 创建内存使用快照，方便和后面做对比
* summary.diff: 和上一次baseline的summary对比
* detail.diff: 和上一次baseline的detail对比
* shutdown: 关闭NMT

### VM 推出时打印NMT
通过添加如下参数 -XX:+UnlockDiagnosticVMOptions -XX:+PrintNMTStatistics

### 通过jstat -gcutil <pid> 获取gc详情