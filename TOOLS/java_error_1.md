### java排错案例1
####环境
centos 7
jdk8u333
#### 启动参数
java 
-Xms4g 
-Xmx4g 
-XX:G1HeapRegionSize=16m 
-XX:+UseG1GC 
-XX:G1ReservePercent=25 
-XX:InitiatingHeapOccupancyPercent=45 
-XX:+ParallelRefProcEnabled 
-XX:+HeapDumpOnOutOfMemoryError 
-Xloggc:/app/ksignalg/ar_gcdetail.log 
-XX:HeapDumpPath=/app/ksignalg/oom.hprof 
-XX:ErrorFile=/app/ksignalg/java_error.log 
-XX:NativeMemoryTracking=detail 
-jar ksign-alg.jar --spring.profiles.active=dev

#### 关键参数
-XX:NativeMemoryTracking=detail   本地内存监控开启
如果出现如下错误
```shell
Failed to write core dump. Core dumps have been disabled. To enable core dumping, 
try "ulimit -c unlimited" before starting Java again
```
执行
```shell
ulimit -c unlimited 命令
```

此时会生成 core.<pid> 文件

通过  jstack  $JAVA_HOME/bin/java  core.<pid> 就可以看到栈信息了

#### jvm crash的崩溃日志分析
![jvm crash analyzed](jvm_crash_analyzed.png)


https://blog.csdn.net/test_touch/article/details/84230704