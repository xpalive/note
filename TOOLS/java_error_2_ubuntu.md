### ubuntu下不生成core文件
安装工具
```shell
# sudo apt install systemd-coredump
# sudo apt install liblz4-tool
```
#### 开启生产core
```shell
# ulimit -c unlimited
```
#### 查询core
```shell
# coredumpctl list
```
#### 查看core文件的压缩包
```shell
# ls /var/lib/systemd/coredump
```
#### 使用liblz4-tool提供的命令解压lz4文件。
```shell
# lz4 -d /var/lib/systemd/coredump/<filename>
```
#### 使用命令生产出core文件 ，该core文件无法用jstack打开
```shell
coredumpctl dump [pid] -o core
```

journalctl --vacuum-time=1d 日志删除
