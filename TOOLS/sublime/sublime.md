### sublime text 显示文件编码格式
Preferences -> Settings

修改配置文件
```yaml
# display file encoding in the status bar
"show_encoding": false,
# display line endings in the status bar
"show_line_endings": false
```
