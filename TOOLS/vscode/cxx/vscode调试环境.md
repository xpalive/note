### vscode 目录结构
project
 |- .vscode
 |- build
 |- include
 |- lib
 |- src
 |- CMakeLists.txt

#### 在.vscode 目录下生成 launch.json
内容如下
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "g++ - 生成和调试活动文件",
            "type": "cppdbg",
            "MIMode": "gdb",
            "request": "launch",
            "program": "${workspaceFolder}/build/fisheyeDemo",
            "cwd": "${workspaceFolder}/bin",
            "miDebuggerPath": "/usr/bin/gdb",
            "logging": {
                "engineLogging": true
            }
        }
    ]
}
```
program 编译出来的可执行文件
cwd 编译的目录

### 插件安装
C/C++
C/C++ Extension Pack
Code Runner