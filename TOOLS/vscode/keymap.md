### 官方文档
https://code.visualstudio.com/docs/getstarted/tips-and-tricks

#### 摘要
基本
Ctrl+Shift+P 命令面板
Ctrl+P 文件面板

自定义
Ctrl+K Ctrl+S 打开键盘快捷方式
Ctrl+, 打开设置

文件和文件夹
Ctrl+` 终端
Ctrl+B 侧边栏
Ctrl+J 下面的面板

编辑技巧
加光标
Alt+Click 添加光标
Shift+Alt+Up or Down 往上或往下加光标（即Shift+Alt+Up 和 Shift+Alt+Down）
Ctrl+Shift+L 当前选词的位置同时加光标
Ctrl+D 下一个当前选词的位置加光标

文本选择和移动
Shift+Alt+鼠标拖动 块选择
Alt+滚动 5倍滚动速度
Alt+Up or Down 移动当前选中行
Shift+Alt+Left or Right 缩小或扩大选中文本的范围，字->词->句->段->页

代码折叠
Ctrl+Shift+[ or ] 代码折叠与展开
Ctrl+K Ctrl+0 or J 折叠或展开所有区域
Ctrl+K Ctrl+/ 折叠所有块注释

导航跳转
Ctrl+Shift+O 在文件中查找符号，命令面板 @:
Ctrl+T 所有文件中查找符号，命令面板 #
Ctrl+G 导航到特定行，命令面板 :
Ctrl+L 选择当前行
Ctrl + Home or End 导航到文件开头或结尾

预览
Ctrl+Shift+V Markdown文件预览
Ctrl+K V 并排显示Markdown编辑和预览

IntelliSense
Ctrl+Space 触发建议弹窗
Ctrl+Shift+F10 Peek查看代码、变量而不修改
F12 or Ctrl+Click 跳转到定义
"转到"->"返回" or Ctrl+Alt+- 返回之前的位置
Ctrl+鼠标悬停 查看类型定义
Shift+F12 查看引用
Shift+Alt+F12 打开引用视图
F2 重命名符号