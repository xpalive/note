### 问题
前言
通过Launcher下载的虚幻5.2后，在创建C++项目后，通过Rider打开工程，
引擎工程会出现莫名的红色警告，通过排查项目内容，发现错误都是因为缺少DirectX导致。
大部分错误在Build.cs文件中，警告部分是DirectX不存在，但是编译项目并不产生任何影响（非源码工程）。

Build.cs文件在虚幻引擎中的主要用途是告知UBT构建工程时，模块和库的引用关系说明，
编译可以通过说明不缺少库，而Rider报错，说明在上下文中找不到关系引入。
将本地引擎代码与官方Git源码做对比，主要检查Source/ThirdParty/Windows目录。
发现在DirectX目录相比引擎源码目录缺少Build.cs文件。

github中DirectX目录：
UnrealEngine/Engine/Source/ThirdParty/Windows/DirectX/DirectX.Build.cs
源码路径：
Epic Games/UE_5.2/Engine/Source/ThirdParty/Windows/DirectX/
缺少DirectX.build.cs

### 解决
将对应文件（DirectX.build.cs）拷贝到源码路径下，重启Rider并重新生成项目文件
Generate Visual Studio project files
并且需要通过rider通过.sln打开项目

### 参考
https://blog.csdn.net/ension_vip/article/details/135422601