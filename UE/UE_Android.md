### Android SDK要求
组件的环境变量  
Android SDK              ->  ANDROID_HOME                      
Java Development Kit     ->  JAVA_HOME                                
Ant Scripting Tool       ->  ANT_HOME               
Android NDK              ->  NDKROOT
参考: https://docs.unrealengine.com/5.0/zh-CN/android-development-basics-for-unreal-engine/  

### 环境变量的位置
ANDROID_HOME
将其设为安装Android SDK的目录；如果安装的是ADT，则为 sdk 目录。
其必须包含一个名为 platform-tools 的目录。

JAVA_HOME
将此设为JDK的安装路径。其名称应类似于 jdk1.8.0_121。
其必须为一个称为 bin，且其中包含 javac.exe 的目录。

ANT_HOME
将此设为解压Ant的路径。
其必须为一个称为 bin，且其中包含 ant.bat 的目录。

NDKROOT
将此设为解压NDK的路径。其名称应类似于 android-ndk-r11c。
其必须包含一个名为 ndk-build.cmd 的文件
