### 安装插件
在插件市场中安装，安装第三方插件

### 项目的.uproject文件添加插件
```json
{
	"FileVersion": 3,
	"EngineAssociation": "5.0",
	"Category": "",
	"Description": "",
	"Modules": [
		{
			"Name": "fisheye",
			"Type": "Runtime",
			"LoadingPhase": "Default"
		}
	],
	"Plugins": [
		{
			"Name": "ModelingToolsEditorMode",
			"Enabled": true,
			"TargetAllowList": [
				"Editor"
			]
		},
		{
			"Name": "OpenCV",
			"Enabled": true
		}
	,
		{
			"Name": "URoboVision",
			"Enabled": true
		}
	]
}
```

### 插件中互相引用
```json
{
	"FileVersion": 3,
	"Version": 1,
	"VersionName": "1.0",
	"FriendlyName": "URoboVision",
	"Description": "CameraActor that captures RGB-D data and sends it out over TCP.",
	"Category": "RobCoG",
	"CreatedBy": "http://robcog.org/",
	"CreatedByURL": "http://haidu.eu",
	"DocsURL": "https://github.com/robcog-iai/URoboVision",
	"MarketplaceURL": "",
	"SupportURL": "http://robcog.org/",
	"CanContainContent": true,
	"IsBetaVersion": false,
	"Installed": false,
	"Modules": [
		{
			"Name": "URoboVision",
			"Type": "Runtime",
			"LoadingPhase": "Default"
		}
	],
	"Plugins": [
		{
			"Name": "OpenCV",
			"Enabled": true
		}
	]
}
```

### 在项目的.cs文件中添加插件
```
using UnrealBuildTool;

public class fisheye : ModuleRules
{
	public fisheye(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] { "OpenCV", "URoboVision" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
```

#### 插件位置
C:\Program Files\Epic Games\UE_5.0\Engine\Plugins\Runtime\OpenCV\Source\ThirdParty\OpenCV\include\opencv2\core\utility.hpp