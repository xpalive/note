### 方法声明
1. 无论共有方法还是私有方法都需要在头文件(.h)中声明，再通过具体的类实现（.cpp）  
2. UFUNCTION(BlueprintCallable) 申明方法可以在蓝图中可以调用  
3. UFUNCTION(BlueprintNativeEvent) 申明方法是通过蓝图实现的，在创建蓝图时，需要选择对应的类作为父类  
4. UPROPERTY(EditAnywhere, Category = "RGB-D Settings") 申明可以在UE编辑器中可以编辑   
5. GENERATED_BODY() 宏会自动生成 AMyActor 类的成员变量、成员函数以及其他必要的代码，以支持 Unreal Engine 中的反射系统和蓝图功能。  
6. 

RGBDCamera.h
```
UCLASS() # 宏
class UROBOVSION_API ARGBDCamera ：public ACameraActor  # 继承 ACameraActor
{
    GENERATED_BODY() # 宏
public:
    // Sets default values for this actor's properties
	ARGBDCamera(); # 构造方法

	// Sets default values for this actor's properties
	virtual ~ARGBDCamera(); # 析构函数

    // 共有属性
    UPROPERTY(EditAnywhere, Category = "RGB-D Settings") # 宏 什么了在蓝图中可以编辑
    uint32 Width;
    
    UFUNCTION(BlueprintCallable) # 宏 申明了在蓝图中可以调用
	void CameraImageCatch();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override; # 生命周期的钩子方法

	// Called when the game ends
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override; # 生命周期的钩子方法

	// Called every frame
	virtual void Tick(float DeltaSeconds) override; # 生命周期的钩子方法

	// Change the framerate on the fly
	void SetFramerate(const float NewFramerate); # 自定义方法
private:

    // 私有属性
    // Camera capture component for color images (RGB)
	USceneCaptureComponent2D* ColorImgCaptureComp;
	
	// 私有方法
	void hasClient();
}
```

### * 号操作符
在C++中，* 操作符可以用于两种不同的情况：

解引用操作符：当 * 操作符出现在一个指针变量前面时，它被用作解引用操作符，表示取得指针指向的对象。
这意味着 * 操作符将返回指针指向的对象的值。  
```
int* ptr = &some_int;
int value = *ptr; // 解引用操作符，获取指针指向的整数值
```
乘法操作符：当 * 操作符出现在两个数值之间时，它被用作乘法操作符。
```
int result = 5 * 3; // 乘法操作符，计算 5 乘以 3 的结果
```
申明变量时：当* 放在类型和变量名之间时，表示申明了一个指针
```
int* ptr = &some_int;
```