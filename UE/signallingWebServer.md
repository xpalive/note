### config.json
```json lines
{
	"UseFrontend": false,
	"UseMatchmaker": true,
	"UseHTTPS": false,
	"UseAuthentication": false,
	"LogToFile": true,
	"LogVerbose": true,
	"HomepageFile": "/Public/player.html",
	"AdditionalRoutes": {},
	"EnableWebserver": true,
	"MatchmakerAddress": "10.67.11.36",
	"MatchmakerPort": "9999",  
	"PublicIp": "10.68.8.19",  //自己的端口
	"HttpPort": 81,
	"HttpsPort": 443,
	"StreamerPort": 801,
	"SFUPort": 8001
}
```