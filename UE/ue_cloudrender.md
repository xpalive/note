### 解决NVIDIA GeForce系列显卡NVENC并发Session数目限制问题
https://blog.csdn.net/TracelessLe/article/details/113755792

### ue 启动配置
"D:\Unreal Projects\TestFirstProject.exe" -WebApiHttpServerPort=30010 -PixelStreamingIP=10.67.11.36 -PixelStreamingPort=18888 -RCWebControlEnable -RCWebInterfaceEnable -RenderOffScreen -AllowPixelStreamingCommands

```bat
@echo off
start C:\test\ue\ue5TestCube\Windows\UnrealRenderer.exe -PixelStreamingURL="ws://127.0.0.1:8888" -AllowPixelStreamingCommands -RenderOffScreen -SceneConfigPath="C:\test\ue\SceneConfig.json" -WebApiHttpServerPort=30010
```