### 插件中的资产烘焙
Project Settings -> Project -> Packaging -> Packaging -> Advanced -> Additional Asset Directories to Cook

### 插件中的Content内容打包
打开Project.uplugin文件
添加 "CanContainContent":true
```json
{
  "FileVersion": 3,
  "FriendlyName": "BLUI",
  "CanContainContent": true
}
```

打开Project.Build.cs
定义打包路径
private string ContentPath
{
get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../Content/")); }
}
拷贝路径下的内容
var ContentFilesToStage = Directory.GetFiles(Path.Combine(ContentPath, ""), "*", SearchOption.AllDirectories);
stageFiles(ContentFilesToStage);