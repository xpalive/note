https://blog.csdn.net/m0_51819222/article/details/122794620

### unreal engine 安装信息
C:\Program Files (x86)\Epic Games
C:\Program Files\Epic Games
C:/ProgramData/Epic/EpicGamesLauncher/VaultCache/
C:\users\xiongping\appdata\local\unrealengine
C:\Users\xiongping\AppData\Local\Temp

### visual studio
visual studio ide
C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise
download cache
C:\ProgramData\Microsoft\VisualStudio\Packages
share component tools and sdk
C:\Program Files (x86)\Microsoft Visual Studio\Shared


### directory
demo
|-.vs             visual studio 缓存，可以删除           
|-Binaries        加载C++类的二进制数据，可以删除                 
|-Config          配置文件目录               
|-Content         内容文件夹
|-Intermediate    编辑中间过程中的产物，可以删除                     
|-Saved           备份文件，自动保存，不建议删除              
|-Source          C++类的文件目录               
|-demo.sln        vs解决方案文件                         
|-demo.uproject    ue的解决方案           


### generate visual studio project files
如果右键菜单没有该选项可以使用命令行代替
%UE_ROOT%\%ENGINE_VERSION%\\Binaries\DotNET\UnrealBuildTool\UnrealBuildTool.exe -projectfiles -project="项目.uproject" -game -rocket -progress
C:\Program Files\Epic Games\UE_5.0\Engine\Binaries\DotNET\UnrealBuildTool\UnrealBuildTool.exe -projectfiles -project="C:\Users\xiongping\work\unrealProjects\cover\cover.uproject" -game -rocket -progress

### ue config
#### 打包后的配置目录
[project_name]\Saved\Config\Windows

#### 远程控制
网络服务器的地址默认设置为127.0.0.1，只能由运行虚幻引擎会话的电脑获取。
为了允许其他设备通过远程控制API访问你的虚幻会话框，
请将项目 DefaultEngine.ini 文件中的 DefaultBindAddress 改为你的设备的IP地址。
打包后:Engine.ini
[HTTPServer.Listeners]
DefaultBindAddress=0.0.0.0