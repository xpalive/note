### 蓝图支持
现在，无需离开 Visual Studio 即可查看 Unreal Engine 蓝图引用和资产属性。  
若要查看蓝图引用，请单击当前显示在带有蓝图引用的 C++ 函数、类和属性顶部的 CodeLens 提示。  
若要查看在 Unreal Engine Blueprints 资产中修改的基类中的属性，请选择 C++ 类或属性的蓝图引用以打开 Unreal Engine 资产检查器。  
若要试用此功能，请确保 Visual Studio 安装程序中的“使用 C++ 的游戏开发”工作负载中选择了“针对 Unreal Engine 的 IDE 支持”。 还需要 Unreal Engine 市场上提供的 Visual 
Studio 集成工具。  
