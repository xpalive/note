### WebRtc基本概念及协议介绍
术语
Signaling channel(信令通道)
a) 一种资源，使应用程序可以通过交换信令消息来发现，建立，控制和终止对等连接
b) 信令消息是两个应用程序相互交换以建立对等连接的元数据。该元数据包括本地媒体信息，例如媒体编解码器和编解码器参数，以及两个应用程序可能相互连接以进行实时流传输的可能的网络候选路径
c) 信令通道通常由信令服务器提供

Peer(对等节点)
a) 接入同一webrtc房间（逻辑划分的管理单元）的进行实时双向流传输的任何设备或应用程序

Session Traversal Utilities for NAT (STUN)
a) 一种协议，用于发现您的公共地址(公网IP)并确定路由器中可能阻止与对等方直接连接的任何限制条件
b) Stun服务由STUN服务器提供

Traversal Using Relays around NAT (TURN)
a) 通过打开与TURN服务器的连接并通过该服务器中继所有信息来绕过对称NAT限制的服务器
b) 当P2P链接建立失败，仍需要保证功能可用，这里就需要TURN服务器对所有数据进行中继。建立relay模式的链接
c) TURN服务由TURN服务器提供

relay模式即使用TURN服务

Session Description Protocol (SDP)
a) 一种描述连接的多媒体内容的标准，例如分辨率，格式，编解码器，加密等，以便一旦数据传输，两个对等方就可以相互理解

SDP Offer
a) 发起建立会话链接的SDP消息(主动发送方)，生成会话描述以创建或修改会话
b) 比如想要和一个对等节点建立链接。首先就得告诉对端，自身终端信息（分辨率，格式，编解码器，加密），而为了描述自身终端信息产生SDP 就被称为SDP offer

SDP Answer
a) answer SDP由响应者响应邀约者发送的offer SDP
b) Answer主要包含终端协商结果(answer端终端信息)。例如，如果offer中的所有音频和视频流都被接受

Interactive Connectivity Establishment (ICE)
a) 一个允许您的Web浏览器(webrtc终端)与对等方连接的框架

ICE Candidate(Candidate)
a) 一种能与对等方用来通信的一种方法(方式)
————————————————
版权声明：本文为CSDN博主「不太能吃」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/user_jiang/article/details/120492163