### cuda 安装
1. 进入cuda官网，找到对应的cuda版本：https://developer.nvidia.com/cuda-downloads
2. 查看电脑的NVIDIA版本，查看支持的最大cuda版本： `cmd： nvidia-smi`，这里cuda版本问12.0
   ![img.png](images/cuda-version.png)
3. 点击官网中历史版本下载，找到对应版本的cuda（11.8）
   ![img.png](images/cuda-toolkit-version.png)
4. 下载完成后使用管理员身份运行安装
5. 检查系统兼容性
6. 许可协议 -> 同意并继续
7. 自定义安装 -> 将Visual Studio Integration 取消勾选
8. 选择安装路径：(将开发环境和samples分开目录) 
   1. cuda development -> c:\cuda\11.8\cuda1
   2. cuda documentation -> c:\cuda\11.8\cuda1
   3. samples -> c:\cuda\11.8\cuda2
9. 检查环境变量 
   1. CUDA_PATH=c:\cuda\11.8\cuda1
   2. CUDA_PATH_V11_8=c:\cuda\11.8\cuda1
   3. NVCUDASAMPLES_ROOT=c:\cuda\11.8\cuda2
   3. NVCUDASAMPLES11_8_ROOT=c:\cuda\11.8\cuda2
10. 检查cuda是否安装成功`nvcc --version`
11. 查看cuda设置的环境变量`set cuda`

注意：11.8版本没有samples

#### 环境验证
1. nvidia-smi
2. nvcc --version