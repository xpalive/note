## 计算机视觉基础库
MMCV由OpenMMLab开发和维护，支持多种平台，包括Linux、Windows和macOS。这个库提供了图像和视频处理、图像和标注结果可视化、图像变换、多种CNN网络结构、高质量实现的常见CUDA算子等功能。
### 安装命令
pip install --proxy=http://127.0.0.1:7890 mmcv==2.1.0 -f https://download.openmmlab.com/mmcv/dist/cu118/torch2.1/index.html