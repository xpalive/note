### 安装教程
https://github.com/open-mmlab/mmtracking/blob/master/docs/en/install.md
https://mmtracking.readthedocs.io/zh-cn/latest/install.html#id2

### 安装conda
[conda教程](../../PYTHON/conda.md)

### PyTorch and torchvision 安装
安装之前需要现安装[cuda](cuda.md)
根据cuda的版本安装对应的PyTorch和torchvision
根据[教程](https://pytorch.org/) 选择对应的命令
`conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia`

pytorch 1.13.1
pytorch-cuda 11.8
pytorch-mutex 1.0
torchvision 0.14.1
torchaudio 0.13.1

### VOT Challenge（可选）
是一个挑战平台（用于比赛，研究等）

### 安装mmcv-full
[mmcv](mmcv.md) 安装参考

### 安装MMDetection
[MMDetection](mmdet.md)

### 克隆MMTracking代码并安装
git clone https://github.com/open-mmlab/mmtracking.git
cd mmtracking

pip install -r requirements/build.txt
pip install -v -e .  # or "python setup.py develop"

### 安装额外依赖
#### MOTChallenge
pip install --proxy=http://127.0.0.1:7890 git+https://github.com/JonathonLuiten/TrackEval.git
#### LVIS
pip install --proxy=http://127.0.0.1:7890 git+https://github.com/lvis-dataset/lvis-api.git
#### TAO
pip install --proxy=http://127.0.0.1:7890 git+https://github.com/TAO-Dataset/tao.git

#### 一个成功的安装案例
```shell
conda create -n open-mmlab python=3.7 -y
conda activate open-mmlab

conda install pytorch==1.6.0 torchvision==0.7.0 cudatoolkit=10.1 -c pytorch -y

pip install git+https://github.com/votchallenge/toolkit.git (optional)
# install the latest mmcv
pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/cu101/torch1.6.0/index.html

# install mmdetection
pip install mmdet==2.28.2

# install mmtracking
git clone https://github.com/open-mmlab/mmtracking.git
cd mmtracking
pip install -r requirements/build.txt
pip install -v -e .
pip install git+https://github.com/JonathonLuiten/TrackEval.git
pip install git+https://github.com/lvis-dataset/lvis-api.git
pip install git+https://github.com/TAO-Dataset/tao.git
```

#### 查看版本
##### pip
pytorch 版本 pip show torch
torchvision 版本 pip show torchvision
torchaudio 版本 pip show torchaudio
##### conda
pytorch 版本 conda list pytorch
torchvision 版本 conda list torchvision
torchaudio 版本 conda list torchaudio
##### 代码查看
import torch
print(torch.__version__)

