### 操作
#### 基本操作
1. shift + 鼠标中键： 位移场景
2. 鼠标中键： 旋转场景
3. 滚动鼠标中键： 缩放场景
4. G： 位移对象（xyz、alt 归零）
5. R： 旋转对象（xyz、alt 归零）
6. S： 缩放对象（xyz、alt 归零）
7. shift+a： 添加
#### 其他
1. ctrl+alt+Q：进入到四视图视角
2. view -> align view -> align active camera to view ：设置当前视角为相机视角（ctrl+alt+0（小键盘的0））
3. ~：可调出各个视图的选择
4. G：移动，alt+G：移动归零，G+X/YZ：对应的X/Y/Z轴移动；
5. R：旋转，alt+R：旋转角度归零，R+X/YZ：对应的X/Y/Z轴旋转，R+X+45：沿着X轴旋转45度；
6. S：缩放，alt+S：缩放归零，S+X/YZ：对应的X/Y/Z轴缩放，S+X+5：沿着X轴缩放5倍；
7. a：全选，a+a：取消全选；
8. x：删除；

