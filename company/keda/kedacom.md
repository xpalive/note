### 2022.03.22 苏州科达 创新资源中心web前端技术部 工号 KD020412
1. 邮箱账号：
    xiongping@kedacom.com / 959M5eD9
    邮箱配置：pop3.kedacom.com / stmp.kedacom.com
2. oa账号:
    https://oa.kedacom.com/portal   
    xiongping@kedacom.com / 190007  959M5eD9-XP
3. 科达网络接入终端
    使用科达oa账号
    
4. 公司无线网络使用说明
    苏州总部:电脑无线热点:kedacomnet 密码:kedacom6545;   
    苏州园区:电脑无线热点:kedacomNet-YQ 密码:kedacom6545;  
    上海徐汇2007号和2071号:电脑无线热点:kedacom.Inc-5G 密码:kdckdckdc;  
    嘉定南翔: 8 楼电脑无线热点:hl807* 密码:Hongliu93;
            13 楼:Hongliu 密码:kedacom.com;

5. 密码找回自助操作
    https://yunpan.kedacom.com/yunwei/

6. 知识库
    https://oa.kedacom.com/confluence/pages/viewpage.action?pageId=14089869 入职指南
    
7. 资料发布平台
    https://cms.kedacom.com:2443/index?view=1151056965616640

8. 超融合AR实景平台有没有测试地址
    管理端地址 http://10.68.8.115:8081/ty-ksign/ ksign/123456
    前端地址 http://10.68.8.115:8086/

9. 科达天行账号
    xiongping@kedacom.com / xiongping

10. web前端技术部内部资料
    https://oa.kedacom.com/confluence/pages/viewpage.action?pageId=13632418
11. es客户端
    http://10.68.8.117:9000/

12. postman：
    xiongping@kedacom.com
    xiongping-kedacom
    959M5eD9-XP
13. 虚拟机 root/xiongping
    账密root/123456  
    账密root/123456