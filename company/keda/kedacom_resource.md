### 部门资源
球机：IPC425、IPC489 部门独享设备

RTK设备测量经纬度及海拔

屏幕坐标与GPS互转 cesium算法

### Cesium聚簇实现-kdbush-像素聚合
https://blog.csdn.net/wqy248/article/details/99865420
https://blog.csdn.net/wqy248/article/details/100531854

### geometry 包 地理信息、空间坐标
```xml
<dependencys>
    <dependency>
        <groupId>com.vividsolutions</groupId>
        <artifactId>jts</artifactId>
        <version>${jts.version}</version>
    </dependency>
    <dependency>
        <groupId>org.wololo</groupId>
        <artifactId>jts2geojson</artifactId>
        <version>${jts2geojson.version}</version>
    </dependency>
</dependencys>
```

### java关键字
strictfp 使得平台移植性更强

### 执行外部命令
通过apache.commons.exec执行外部命令

### PTZ
P: pan ,摄像头与正北方向的夹角
T: tilt ,摄像头与水平方向的夹角
Z: zoom ,摄像头缩放

### FOV
视场角在光学工程中又称视场，视场角的大小决定了光学仪器的视野范围。
视场角又可用FOV表示，其与焦距的关系如下：h = f*tan\[Theta]；
像高 = EFL*tan (半FOV)；EFL为焦距；FOV为视场角。

### MSE (media source extensions) 媒体资源扩展
https://www.w3.org/TR/media-source/#introduction
允许JavaScript创建媒体的播放对象，用来播放流式音视频数据

### WASM (WebAssembly)
Assembly 是指汇编代码
图像处理方面
适合cpu密集型
可以通过移植一些代码，如go、tinygo、c/c++、c#等 到web端使用
一般通过请求获取代码二进制数据，通过加载来使用
1. 获取 .wasm 二进制文件，将它转换成类型数组或者 ArrayBuffer（WebAssembly 模块可以使用 ES6 模块(使用<script type="module">)加载）
2. 将二进制数据编译成一个 WebAssembly.Module
3. 使用 imports 实例化这个 WebAssembly.Module，获取 exports。

http://webassembly.org.cn/getting-started/js-api/

### chrome timing
for further reference, please consider following sections:
https://zhuanlan.zhihu.com/p/359995869

因为有“队头阻塞”，浏览器对每个域名最多开 6 个并发连接（HTTP/1.1），当页面里链接很多的时候就必须排队等待（Queued、Queueing），这里它就等待了 1.62 秒，然后才被浏览器正式处理；
浏览器要预先分配资源，调度连接，花费了 11.56 毫秒（Stalled）;
连接前必须要解析域名，这里因为有本地缓存，所以只消耗了 0.41 毫秒（DNS Lookup）；
与网站服务器建立连接的成本很高，总共花费了 270.87 毫秒，其中有 134.89 毫秒用于 TLS 握手，那么 TCP 握手的时间就是 135.98 毫秒（Initial connection、SSL）；
实际发送数据非常快，只用了 0.11 毫秒（Request sent）；
之后就是等待服务器的响应，专有名词叫 TTFB（Time To First Byte），也就是“首字节响应时间”，里面包括了服务器的处理时间和网络传输时间，花了 124.2 毫秒；
接收数据也是非常快的，用了 3.58 毫秒（Content Dowload）。
从这张图你可以看到，一次 HTTP“请求 - 响应”的过程中延迟的时间是非常惊人的，总时间 415.04 毫秒里占了差不多 99%。

### APM
application performance monitor

### OLTP OLAP
online transaction processing
联机事务处理 - 对于数据库来说就是（增删改为主）
online analytical processing
联机分析处理 - 对与数据库来说就是（查询为主）

### 摄像头设备
20                     PTZ设备/非全双工设备                    高架桥
10.255.251.142         PTZ设备/全双工设备                     金山东路路口
180 PTZ设备/非全栓工设备                                       篮球场
179                    PTZ设备/非全双工设备                    篮球场
114云台                 PTZ设备/非全双工设备                    云台


### chrome 文档
https://developer.chrome.com/

### BIM GIS
BIM (building information modeling)
GIS (geographic information system)
PTT (push to talk
)
