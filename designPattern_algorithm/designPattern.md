### 设计模式分类
#### 创建型模式
1. 工厂模式
2. 抽象工厂模式 
3. 单例模式[https://blog.csdn.net/weixin_39713814/article/details/111009079]
4. 建造者模式
5. 原型模式
#### 结构型模式
1. 适配器模式
2. 装饰器模式
3. 代理模式
4. 外观模式
5. 桥接模式
6. 组合模式
7. 享元模式
#### 行为型模式
1. 策略模式
2. 模板方法模式
3. 观察者模式
4. 迭代子模式
5. 责任链模式
6. 命令模式
7. 备忘录模式
8. 状态模式
9. 访问者模式
10. 中介者模式
11. 解释器模式

#### 并发型模式、线程池模式

### 设计模式原则
1. 开闭原则 -> 抽象化是开闭原则的关键
2. 里氏代换原则 -> 所有引用基类（父类）的地方必须能透明地使用其子类的对象
3. 依赖倒置原则 -> 抽象不应该依赖于细节，细节应当依赖于抽象。换言之，要针对接口编程，而不是针对实现编程
4. 接口隔离原则 -> 使用多个专门的接口，而不使用单一的总接口，即客户端不应该依赖那些它不需要的接口
5. 迪米特法则 -> 一个实体应当尽量少的与其他实体之间发生相互作用，使得系统功能模块相对独立。（如果需要可以创建一个协调者作为中间类）
6. 单一职责原则 -> 一个类只负责一个功能领域中相应的职责


### 参考
[设计模式](https://blog.csdn.net/ttxs99989/article/details/81844135)  
[开闭原则](https://blog.csdn.net/lovelion/article/details/7537584)  
[里氏代换原则](https://blog.csdn.net/lovelion/article/details/7540445)  
[依赖倒置原则](https://blog.csdn.net/lovelion/article/details/7562783)  
[接口隔离原则](https://blog.csdn.net/lovelion/article/details/7562842)  
[迪米特法则](https://blog.csdn.net/lovelion/article/details/7563445)  
[单一职责原则](https://blog.csdn.net/lovelion/article/details/7536542)  