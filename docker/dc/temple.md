```yaml
version: "3.9"  # optional since v1.27.0
services:
  # 前提是宿主机需要添加时区链接 ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
  # - /etc/localtime:/etc/localtime:ro 这个配置可以解决docker容器中date函数的返回值，但是java获取的时间依旧不正确
  # 可以通过查看容器中的/etc/localtime文件得知，对应的时区没有变化
  # 需要在容器中重新添加链接 ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
  # command 执行多条命令如下
  webapp:
    build:
      context: /data/app/baijingtai/
      dockerfile: Dockerfile
      network: zhiwo-net
    ports:
      - "8886:8886"
    volumes:
      - /data/app/baijingtai/upload:/upload
      - /data/app/baijingtai/logs:/logs
      - /etc/localtime:/etc/localtime:ro 
    command: >
      bash -c "ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
      && java -Djava.security.egd=file:/dev/./urandom -jar /baijingtai/baijingtai.jar --spring.profiles.active=prod,boc,alipay,wechat"
  mysql:
    image: mysql:5.7.34
    container_name: mysql
    restart: always
    ports:
      - "12670:3306"
    volumes:
      - /data/mysql/datadir:/var/lib/mysql
      - /data/mysql/log:/var/log/mysql
      - /data/mysql/conf.d:/etc/mysql/conf.d
      - /data/mysql/mysql.conf.d:/etc/mysql/mysql.conf.d
      - /etc/localtime:/etc/localtime:ro
    environment:
      #  docker-compose 中如果使用 $ 符号，则需要使用$$表示$
      - MYSQL_ROOT_PASSWORD=$$PLuq5%aYSU9^tkp
      - MYSQL_ROOT_HOST=%
      - TZ=Asia/Shanghai
    networks:
      - zhiwo-net
  nginx:
    image: nginx:1.19.10
    container_name: nginx
    restart: always
    ports:
      - "80:80"
      - "443:443"
      - "8081:8081"
    volumes:
      - /data/nginx/log:/var/log/nginx:rw
      - /data/nginx/config:/etc/nginx/conf.d:rw
      - /data/html:/usr/share/nginx/html:ro
      - /data/app/baijingtai/upload:/usr/share/nginx/app/baijingtai/upload:ro
      - /etc/letsencrypt:/etc/letsencrypt
      - /etc/localtime:/etc/localtime:ro
    networks:
      - zhiwo-net
networks:
  zhiwo-net:
    name: zhiwo-net
```