


#### docker compose
docker-compose.yml
##### mysql
```yaml
services:
  mysql:
    image: mysql:5.7.35
    container_name: mysql01
    restart: always
    ports:
      - "3306:3306"
    volumes: 
      - /mydata/mysql01/datadir:/var/lib/mysql
      - /mydata/mysql01/conf.d:/etc/mysql/conf.d
      - /etc/localtime:/etc/localtime:ro
     environment:
      - MYSQL_ROOT_PASSWORD=123@abcd
networks:
  default:
    external: true
    name: bear-net
```
> docker exec -it some-mysql bash
> docker logs some-mysql

##### redis
```yaml
services:
  redis:
    image: redis:6.2.5
    container_name: redis01
    restart: always
    ports:
      - "6379:6379"
    volumes: 
      - /mydata/redis01/conf:/usr/local/etc/redis
      - /mydata/redis01/data:/data
      - /etc/localtime:/etc/localtime:ro
    command: ["redis-server", "/usr/local/etc/redis/redis.conf"]
  networks:
    default:
      external: true
      name: bear-net
```