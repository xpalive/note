### 命令
docker-compose start stop 对应的名称为compose.yaml 中配置的名称，与用docker-compose ps 查询出来的名字无关
例子：docker-compose start mysql01



#### compose.yaml
```yaml
services:
  mysql01:
    ports:
      - 3306:3306
  redis01:
```