### 配置 /etc/docker/daemon.json 文件
```json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "max-file": "3"
  }
}
```

> 通过 docker inspect -f '{{.HostConfig.LogConfig}}' {{container}} 来查看是否生效了  
>
> sudo systemctl restart docker 需要重启docker服务