#### 注意服务器安装的是docker engine

#### 安装docker centos7  v:19.03.9
```shell script
#!/bin/bash
yum remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine

yum install -y yum-utils

yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce-19.03.9 docker-ce-cli-19.03.9 containerd.io

systemctl start docker
```
#### docker 自动重启
systemctl enable docker.service 

#### docker compose install
```shell script
#!/bin/bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
# ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

#### docker daemon

```shell
vim /etc/docker/daemon.json
{
  "storage-driver": "overlay2",
  "graph": "/var/lib/docker",
  "log-driver": "json-file",
  "log-opts": {
        "max-size": "200m",
        "max-file": "7"
  },
  "debug": false,
  "userns-remap": "1000:1000"
} 

# ls -lh $(find /var/lib/docker/containers/ -name *-json.log) 查看docker日志大小
```

#### docker log
https://blog.csdn.net/ruoxiyun/article/details/101366581

#### ubuntu docker engine install
安装docker文档
https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
后续的安装步骤
https://docs.docker.com/engine/install/linux-postinstall/