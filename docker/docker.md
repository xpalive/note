### 使用docker时需要注意权限问题
mysql中由于日志目录映射到宿主机导致权限问题，可能会导致mysql启动不了
```
Could not open file '/var/log/mysql/error.log' for error logging: Permission denied
```
需要修改宿主机的目录权限

