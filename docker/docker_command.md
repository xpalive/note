### docker command

##### docker 补全
> 2.1 安装 bash-completion  
> 安装完成之后重启系统或者重新登录 shell。如果安装成功。键入 docker p 后，再 Tab 键可显示候选项  
> 此时，我们运行例如 docker run 之类的命令，键入镜像的首字母，镜像名称依然无法自动补全。  
> /etc/bash_completion.d/docker-compose 通过docker-compose 完善镜像补全  
```shell script
sudo yum install -y bash-completion
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.24.1/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
source /etc/bash_completion.d/docker-compose
```

> 需要编译文件 source /usr/share/bash-completion/bash_completion
> 需要编译文件 source /usr/share/bash-completion/completions/docker
```shell script
/usr/share/bash-completion/completions/docker
/usr/share/bash-completion/completions/docker-compose
```

##### 关闭防火墙后需要重启docker服务
```shell
systemctl restart docker
[root@localhost ~]# docker run -d -p 9000:80 centos:httpd /bin/sh -c /usr/local/bin/start.sh
d5b2bd5a7bc4895a973fe61efd051847047d26385f65c278aaa09e4fa31c4d76
docker: Error response from daemon: driver failed programming external connectivity on endpoint quirky_allen (6bda693d1143657e46bee0300276aa05820da2b21a3d89441e820d1a274c48b6): (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 9000 -j DNAT --to-destination 172.17.0.2:80 ! -i docker0: iptables: No chain/target/match by that name.
(exit status 1)).

[root@localhost ~]# docker start d5b2bd5a7bc4 
Error response from daemon: driver failed programming external connectivity on endpoint quirky_allen (4127da7466709fd45695a1fbe98e13c2ac30c2a554e18fb902ef5a03ba308438): (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 9000 -j DNAT --to-destination 172.17.0.2:80 ! -i docker0: iptables: No chain/target/match by that name.
(exit status 1))
Error: failed to start containers: d5b2bd5a7bc4
```
> docker端口映射或启动容器时报错 driver failed programming external connectivity on endpoint quirky_allen  
> docker服务启动时定义的自定义链DOCKER由于 centos7 firewall 被清掉  
> firewall的底层是使用iptables进行数据过滤，建立在iptables之上，这可能会与 Docker 产生冲突。  
> 当 firewalld 启动或者重启的时候，将会从 iptables 中移除 DOCKER 的规则，从而影响了 Docker 的正常工作。  
> 当你使用的是 Systemd 的时候， firewalld 会在 Docker 之前启动，但是如果你在 Docker 启动之后再启动 或者重启 firewalld ，你就需要重启 Docker 进程了。  
> 重启docker服务及可重新生成自定义链DOCKER
> 

### docker cp
```shell
docker cp [OPTIONS] container:src_path dest_path
docker cp [OPTIONS] dest_path container:src_path

# 如果要复制文件夹
docker cp container:src_path/ dest_path

```
### docker network
```shell script
docker network create bear-net
```

### docker command line (CLI -> command line interface)
参考文档： https://docs.docker.com/engine/reference/commandline/cli/

```shell
# 根据状态查询容器
docker ps --filter status=created --filter status=exited --format "{{.ID}}"  
# 删除镜像
docker rmi ${docker images -f "dangling=true" -q}

# 压缩镜像
docker save mysql > mysql.tar
# 加载镜像 
docker load < mysql.tar
```

### 配置文件
/etc/docker/daemon.json
```json
{
  "exec-opts" : ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "50m",
    "max-file": "3"
  },
  "proxies":{
	  "http-proxy": "http://192.168.10.122:7890",
	  "https-proxy": "http://192.168.10.122:7890"
  }
}

```
需要在配置文件中设置代理才可以通过代理下载dockerhub的镜像，通过proxychains4 下载不了
### 命令行
sudo systemctl daemon-reload
sudo systemctl restart docker
