### 注意
docker-compose 中如果使用 $ 符号，则需要使用$$表示$  
docker是可以映射文件的，和映射目录一致,在nginx的docker hub文档中有，且已证明可以，可以在nginx.conf中加入特殊标识，并进入容器中查看  
```shell
# https://hub.docker.com/_/nginx?tab=description&page=1&name=1.20
$ docker run --name my-custom-nginx-container -v /host/path/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx
```

##### mysql
```yaml
services:
  mysql:
    image: mysql:8.0.27
    container_name: mysql01
    restart: always
    ports:
      - "3306:3306"
    volumes:
      - /mydata/mysql01/datadir:/var/lib/mysql
      - /mydata/mysql01/conf.d:/etc/mysql/conf.d
      - /etc/localtime:/etc/localtime:ro # 解决时区问题
    environment:
      - MYSQL_ROOT_PASSWORD=123@abcd
      - TZ=Asia/Shanghai
networks:
  default:
    external: true
    name: bear-net
```
##### redis
```yaml
services:
  redis01:
    image: redis:6.2.5
    container_name: redis01
    restart: always
    ports:
      - "6379:6379"
    volumes: 
      - /mydata/redis01/conf:/usr/local/etc/redis
      - /mydata/redis01/data:/data
      - /etc/localtime:/etc/localtime:ro
    command: ["redis-server", "/usr/local/etc/redis/redis.conf"]
  networks:
    default:
      external: true
      name: bear-net
```