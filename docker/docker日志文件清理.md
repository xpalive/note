### docker 日志文件清理
查看磁盘空间占用
> df -h

查看当前目录下磁盘占用
> du -d1 -h ./ | sort
> 通过该命令找到占用磁盘较大的文件

根据文件目录找到对应的容器
> docker ps -q | xargs docker inspect --format '{{.State.Pid}}, {{.Id}}, {{.Name}}, {{.GraphDriver.Data.WorkDir}}' | 
> grep xxxxxx(overlay2的文件路径)