### k8s
```shell script
kubectl

kubectl get all
kubectl get all -owide # 查看资源

kubectl create deployment nginx --image=nginx # 创建 
kubectl create deployment nginx --image=nginx --dry-run -o yaml # 创建资源清单 
kubectl expose deployment nginx --port=80 --type=NodePort # 暴露端口

kubectl scale --replicas=5 deployment my-tomcat

#同 docker-compose
kubectl apply -f ${file.yaml}

#滚动升级
kubectl set image deployment my-tomcat tomcat=tomcat:8
#同 docker inspect
kubectl describe pod ${pod}

kubectl get all

kubectl exec -it ${pod} -- sh
```


deployment 部署计划

pod k8s中的最小单元-逻辑主机（容器-同docker中的container）

service 对k8s中一组pod的一个服务资源

