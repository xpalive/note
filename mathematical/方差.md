### 方差含义
方差表示一项事物的稳定性，方差越小越稳定

### 泛化能力
泛化能力（generalization ability）是指机器学习算法对新鲜样本的适应能力。
学习的目的是学到隐含在数据背后的规律，对具有同一规律的学习集以外的数据，经过训练的网络也能给出合适的输出，该能力称为泛化能力。