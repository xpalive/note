### nginx 安装
安装方式
源码编译=>Nginx (1.版本随意 2.安装复杂 3.升级繁琐 4.规范 5.便于管理)
epel仓库=>Nginx (1.版本较低 2.安装简单 3.配置不易读 4.不能灵活配置)
官方仓库=>Nginx (1.版本较新 2.安装简单 3.配置易读 4.不能灵活配置)

### 依赖的环境
1. gcc安装
```shell
yum install -y gcc-c++
```
2. pcre pcre-devel 安装
   PCRE(Perl Compatible Regular Expressions) 是一个Perl库，包括 perl 兼容的正则表达式库。
   nginx 的 http 模块使用 pcre 来解析正则表达式，所以需要在 linux 上安装 pcre 库，
   pcre-devel 是使用 pcre 开发的一个二次开发库。nginx也需要此库。
```shell
yum install -y pcre pcre-devel
```
3. zlib 安装
   zlib 库提供了很多种压缩和解压缩的方式， nginx 使用 zlib 对 http 包的内容进行 gzip ，所以需要在 Centos 上安装 zlib 库。
```shell
yum install -y zlib zlib-devel
```
4. OpenSSL 安装
   OpenSSL 是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及 SSL 协议，并提供丰富的应用程序供测试或其它目的使用。
   nginx 不仅支持 http 协议，还支持 https（即在ssl协议上传输http），所以需要在 Centos 安装 OpenSSL 库。
```shell
yum install -y openssl openssl-devel
```

### 安装
#### 添加nginx用户和组
```shell
groupadd -r nginx
# -r：创建系统群组

useradd -r -g nginx -s /bin/false -M nginx
# -r ：创建系统用户
# -g 组名 ：手工指定用户的初始组
# -s shell ：手工指定用户的登录 Shell 
# -M：不创建用户的主目录

# 查看
[root@nginx ~]# id nginx
uid=997(nginx) gid=995(nginx) groups=995(nginx)
```
#### nginx编译安装
```shell
# 源码下载
[root@nginx ~]# wget -c https://nginx.org/download/nginx-1.20.1.tar.gz
# 解压并进入解压目录
[root@nginx ~]# tar -zxvf nginx-1.20.1.tar.gz
[root@nginx ~]# cd nginx-1.20.1
# ./configure 预编译，主要用于检测系统基准环境库是否满足，生成makefile文件
[root@nginx nginx-1.20.1]# mkdir -p /usr/local/nginx
./configure \
 --prefix=/usr/local/nginx \  # 安装目录
 --user=nginx \				  # 用户
 --group=nginx \			  # 用户组 
 --with-http_stub_status_module \         # --with-模块名 表示指定安装其他模块，有许多模块nginx是自带安装的，那么此时无需也不能指定。如果想查看下载的nginx源码包自带哪些模块，可以查看解压后的目录（不是安装目录）的auto目录下的options文件，带YES的表示默认安装时候自带的模块。这些模块使用-V是查看不到的。
 --with-http_ssl_module \
 --with-stream \
 --with-http_auth_request_module
 
# Nginx解压目录中的auto目录下的options文件中可以查看默认编译的模块
[root@nginx ~]# cat nginx-1.20.1/auto/options | grep YES --color

# 编译和安装
[root@nginx nginx-1.20.1]# make && make install

```

### 将nginx加入环境变量
```shell
# 将nginx加入环境变量
[root@nginx nginx-1.20.1]# vim /etc/profile.d/nginx.sh
export PATH=/usr/local/nginx/sbin:$PATH
[root@nginx nginx-1.20.1]# source /etc/profile.d/nginx.sh

# 使用nginx
nginx -t  			# 验证配置文件；无法验证其它文件的情况
nginx -s reload 	# 重新加载；可以重启其它文件启动的情况
nginx -s stop 		# 快速停止
nginx -s quit  		# 正常停止
nginx -v    		# 查看nginx版本
nginx -V 	   		# 查看版本及编译时的参数
nginx -c FILENAME  	# 使用指定的配置文件启动
```

### 设置nginx的systemctl启动方式
```shell
# 设置nginx的systemctl启动方式
[root@nginx nginx-1.20.1]# vim /usr/lib/systemd/system/nginx.service
----
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]	# 服务运行参数的设置
Type=forking  # 设置进程的启动类型，后台运行
PIDFile=/usr/local/nginx/logs/nginx.pid  # 守护进程的PID文件，必须是绝对路径。强烈建议在 Type=forking 的情况下明确设置此选项。 systemd 将会在此服务启动后从此文件中读取主守护进程的PID 。systemd 不会写入此文件，但会在此服务停止后删除它(若存在)。
ExecStartPre=/usr/bin/rm -f /usr/local/nginx/logs/nginx.pid  # 设置在执行 ExecStart= 之前/后执行的命令行。语法规则与 ExecStart= 完全相同。 如果设置了多个命令行，那么这些命令行将以其在单元文件中出现的顺序依次执行。
ExecStartPre=/usr/local/nginx/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf
ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf  # 在启动该服务时需要执行的命令行(命令+参数)
ExecReload=/usr/local/nginx/sbin/nginx -s reload  # 这是一个可选的指令，用于设置当该服务被要求重新载入配置时所执行的命令行。
ExecStop=/usr/local/nginx/sbin/nginx -s quit  # 这是一个可选的指令，用于设置当该服务被要求停止时所执行的命令行。
PrivateTmp=true  # 给服务分配临时空间

[Install]
WantedBy=multi-user.target  # 服务用户的模式
----
# 重载systemctl
[root@nginx nginx-1.20.1]# systemctl daemon-reload

# 使用systemctl管理nginx
systemctl status nginx
systemctl start nginx
systemctl stop nginx
systemctl enable nginx
systemctl disable nginx

```

### 管理nginx
```shell
# 此时可以通过两种方式来管理nginx服务，但是两种方式不能混用，否则会报错或者无法达到预期的效果。比如：
# 使用第一种方式启动nginx服务
[root@nginx nginx]# nginx
[root@nginx nginx]# ps -aux | grep [n]ginx
root       6080  0.0  0.0  46096  1144 ?        Ss   11:18   0:00 nginx: master process nginx
nginx      6081  0.0  0.0  46544  1892 ?        S    11:18   0:00 nginx: worker process

# 使用第二种方式查看nginx服务却显示未启动……
[root@nginx nginx]# systemctl status nginx	
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

Jul 03 11:17:53 nginx nginx[5994]: nginx: [error] open() "/usr/local/nginx/logs/nginx.pid" failed (2: No such file or directory)
Jul 03 11:17:53 nginx systemd[1]: nginx.service: control process exited, code=exited status=1
Jul 03 11:17:53 nginx systemd[1]: Unit nginx.service entered failed state.
Jul 03 11:17:53 nginx systemd[1]: nginx.service failed.
Jul 03 11:18:19 nginx systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jul 03 11:18:19 nginx nginx[6035]: nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
Jul 03 11:18:19 nginx nginx[6035]: nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
Jul 03 11:18:19 nginx systemd[1]: Started The nginx HTTP and reverse proxy server.
Jul 03 11:18:30 nginx systemd[1]: Stopping The nginx HTTP and reverse proxy server...
Jul 03 11:18:30 nginx systemd[1]: Stopped The nginx HTTP and reverse proxy server.
Hint: Some lines were ellipsized, use -l to show in full.
```

如果想安装其他模块或者禁用某些自带模块，或自定义指定nginx相关文件的安装位置等其他，可以参考如下相关参数
./configure 相关参数：
```shell
--prefix=path
# 定义将保留服务器文件的目录。此相同目录还将用于设置的所有相对路径 configure（库源路径除外）和nginx.conf配置文件中。/usr/local/nginx默认情况下设置为目录。

--sbin-path=path
# 设置nginx可执行文件的名称。此名称仅在安装期间使用。默认情况下，文件名为 prefix/sbin/nginx。

--modules-path=path
# 定义将在其中安装nginx动态模块的目录。默认情况下使用prefix/modules目录。

--conf-path=path
# 设置nginx.conf配置文件的名称。如果需要，可以通过在命令行参数中指定nginx来始终使用其他配置文件来启动它 。默认情况下，文件名为 。 -c fileprefix/conf/nginx.conf

--error-log-path=path
# 设置主要错误，警告和诊断文件的名称。安装后，可以始终nginx.conf使用error_log伪指令在配置文件中 更改文件名 。默认情况下，文件名为 prefix/logs/error.log。

--pid-path=path
# 设置nginx.pid将存储主进程的进程ID 的文件名。安装后，可以始终nginx.conf使用pid伪指令在配置文件中 更改文件名 。默认情况下，文件名为 prefix/logs/nginx.pid。

--lock-path=path
# 为锁定文件的名称设置前缀。安装后，可以始终nginx.conf使用lock_file伪指令在配置文件中 更改该值 。默认情况下，值为 prefix/logs/nginx.lock。

--user=name
# 设置一个非特权用户的名称，其凭据将由工作进程使用。安装后，可以始终nginx.conf使用用户指令在配置文件中 更改名称 。默认用户名是nobody。

--group=name
# 设置其凭据将由工作进程使用的组的名称。安装后，可以始终nginx.conf使用用户指令在配置文件中 更改名称 。默认情况下，组名称设置为非特权用户的名称。

--build=name
# 设置一个可选的nginx构建名称。

--builddir=path
# 设置构建目录。

--with-select_module 和 --without-select_module
# 启用或禁用构建允许服务器使用该select()方法的模块。如果平台似乎不支持kqueue，epoll或/ dev / poll等更合适的方法，则会自动构建此模块。

--with-poll_module 和 --without-poll_module
# 启用或禁用构建允许服务器使用该poll()方法的模块。如果平台似乎不支持kqueue，epoll或/ dev / poll等更合适的方法，则会自动构建此模块。

--with-threads 
# 启用线程池的使用 。

--with-file-aio
# 支持 在FreeBSD和Linux上使用 异步文件I / O（AIO）。

--with-http_ssl_module 
# 启用构建将HTTPS协议支持添加 到HTTP服务器的模块的功能。默认情况下未构建此模块。需要OpenSSL库来构建和运行此模块。

--with-http_v2_module
# 支持构建一个模块，该模块提供对HTTP / 2的支持 。默认情况下未构建此模块。

--with-http_realip_module
# 支持构建ngx_http_realip_module 模块，该 模块将客户端地址更改为在指定的标头字段中发送的地址。默认情况下未构建此模块。

--with-http_addition_module
# 允许构建ngx_http_addition_module 模块，该 模块在响应之前和之后添加文本。默认情况下未构建此模块。

--with-http_xslt_module 和 --with-http_xslt_module=dynamic
# 支持构建ngx_http_xslt_module 模块，该 模块使用一个或多个XSLT样式表转换XML响应。默认情况下未构建此模块。该libxml2的和 的libxslt库需要构建和运行此模块。

--with-http_image_filter_module 和 --with-http_image_filter_module=dynamic
# 支持构建ngx_http_image_filter_module 模块，该 模块可以转换JPEG，GIF，PNG和WebP格式的图像。默认情况下未构建此模块。

--with-http_geoip_module 和 --with-http_geoip_module=dynamic 
# 支持构建ngx_http_geoip_module 模块，该 模块根据客户端IP地址和预编译的MaxMind数据库创建变量 。默认情况下未构建此模块。

--with-http_sub_module
# 支持构建ngx_http_sub_module 模块，该 模块通过将一个指定的字符串替换为另一个指定的字符串来修改响应。默认情况下未构建此模块。

--with-http_dav_module
# 支持构建ngx_http_dav_module 模块，该 模块通过WebDAV协议提供文件管理自动化。默认情况下未构建此模块。

--with-http_flv_module
# 支持构建ngx_http_flv_module 模块，该 模块为Flash Video（FLV）文件提供伪流服务器端支持。默认情况下未构建此模块。

--with-http_mp4_module
# 支持构建ngx_http_mp4_module 模块，该 模块为MP4文件提供伪流服务器端支持。默认情况下未构建此模块。

--with-http_gunzip_module
# 支持为不支持“ gzip”编码方法的客户端构建ngx_http_gunzip_module 模块，该 模块使用“ Content-Encoding: gzip” 解压缩响应。默认情况下未构建此模块。

--with-http_gzip_static_module
# 支持构建ngx_http_gzip_static_module 模块，该 模块支持发送.gz扩展名为“ ”的预压缩文件，而不是常规文件。默认情况下未构建此模块。

--with-http_auth_request_module
# 允许构建ngx_http_auth_request_module 模块，该 模块基于子请求的结果实现客户端授权。默认情况下未构建此模块。

--with-http_random_index_module
# 支持构建ngx_http_random_index_module 模块，该 模块处理以斜杠（' /'）结尾的请求，并从目录中选择一个随机文件作为索引文件。默认情况下未构建此模块。

--with-http_secure_link_module
# 启用构建 ngx_http_secure_link_module 模块。默认情况下未构建此模块。

--with-http_degradation_module
# 启用构建 ngx_http_degradation_module模块。默认情况下未构建此模块。

--with-http_slice_module
# 支持构建ngx_http_slice_module 模块，该 模块将请求拆分为子请求，每个子请求返回一定范围的响应。该模块提供了更有效的大响应缓存。默认情况下未构建此模块。

--with-http_stub_status_module
# 支持构建ngx_http_stub_status_module 模块，该 模块提供对基本状态信息的访问。默认情况下未构建此模块。

--without-http_charset_module
# 禁用构建ngx_http_charset_module 模块，该 模块将指定的字符集添加到“ Content-Type”响应头字段中，并且可以将数据从一个字符集转换为另一个字符集。

--without-http_gzip_module
# 禁用构建可压缩 HTTP服务器响应的模块。zlib库是构建和运行此模块所必需的。

--without-http_ssi_module
# 禁用构建 处理通过SSI（服务器端包含）命令的 ngx_http_ssi_module模块的响应。

--without-http_userid_module
# 禁用构建ngx_http_userid_module 模块，该 模块设置适用于客户端标识的cookie。

--without-http_access_module
# 禁用构建ngx_http_access_module 模块，该 模块允许限制对某些客户端地址的访问。

--without-http_auth_basic_module
# 禁用构建ngx_http_auth_basic_module 模块，该 模块允许通过使用“ HTTP基本身份验证”协议验证用户名和密码来限制对资源的访问。

--without-http_mirror_module
# 禁用构建ngx_http_mirror_module 模块，该 模块通过创建后台镜像子请求来实现原始请求的镜像。

--without-http_autoindex_module
# 禁用构建 ngx_http_autoindex_module 模块，以处理以斜杠（' /'）结尾的请求，并在ngx_http_index_module模块找不到索引文件的情况下生成目录列表 。

--without-http_geo_module
# 禁用构建ngx_http_geo_module 模块，该 模块创建的变量的值取决于客户端IP地址。

--without-http_map_module
# 禁用构建ngx_http_map_module 模块，该 模块创建的变量的值取决于其他变量的值。

--without-http_split_clients_module
# 禁用构建ngx_http_split_clients_module 模块，该 模块创建用于A / B测试的变量。

--without-http_referer_module
# 禁用构建ngx_http_referer_module 模块，该 模块可以阻止对“ Referer”标头字段中具有无效值的请求的站点访问。

--without-http_rewrite_module
# 禁用构建允许HTTP服务器 重定向请求并更改请求URI的模块。构建和运行此模块需要PCRE库。

--without-http_proxy_module
# 禁用构建HTTP服务器 代理模块。

--without-http_fastcgi_module
# 禁用构建 将请求传递到FastCGI服务器的 ngx_http_fastcgi_module模块。

--without-http_uwsgi_module
# 禁用构建 将请求传递到uwsgi服务器的 ngx_http_uwsgi_module模块。

--without-http_scgi_module
# 禁用构建 将请求传递到SCGI服务器的 ngx_http_scgi_module模块。

--without-http_grpc_module
# 禁用构建 将请求传递到gRPC服务器的 ngx_http_grpc_module模块。

--without-http_memcached_module
# 禁用构建ngx_http_memcached_module 模块，该 模块从memcached服务器获取响应。

--without-http_limit_conn_module
# 禁用构建ngx_http_limit_conn_module 模块，该 模块限制每个键的连接数，例如，单个IP地址的连接数。

--without-http_limit_req_module
# 禁用构建ngx_http_limit_req_module 模块，该 模块限制每个密钥的请求处理速率，例如，来自单个IP地址的请求的处理速率。

--without-http_empty_gif_module
# 禁用构建发出单像素透明GIF的模块 。

--without-http_browser_module
# 禁用构建ngx_http_browser_module 模块，该 模块创建的变量的值取决于“ User-Agent”请求标头字段的值。

--without-http_upstream_hash_module
# 禁用构建实现哈希 负载平衡方法的模块 。

--without-http_upstream_ip_hash_module
# 禁用构建实现ip_hash 负载平衡方法的模块 。

--without-http_upstream_least_conn_module
# 禁用构建实现了minimum_conn 负载平衡方法的模块 。

--without-http_upstream_keepalive_module
# 禁用构建一个模块来提供 对上游服务器连接的缓存。

--without-http_upstream_zone_module
# 禁用构建模块，该模块可以将上游组的运行时状态存储在共享内存 区域中。

--with-http_perl_module 
--with-http_perl_module=dynamic
# 支持构建 嵌入式Perl模块。默认情况下未构建此模块。

--with-perl_modules_path=path
# 定义一个目录，该目录将保留Perl模块。

--with-perl=path
# 设置Perl二进制文件的名称。

--http-log-path=path
# 设置HTTP服务器的主请求日志文件的名称。安装后，可以始终nginx.conf使用access_log伪指令在配置文件中 更改文件名 。默认情况下，文件名为 prefix/logs/access.log。

--http-client-body-temp-path=path
# 定义用于存储包含客户端请求正文的临时文件的目录。安装后，可以始终nginx.conf使用client_body_temp_path 指令在配置文件中 更改目录 。默认情况下，目录名为 prefix/client_body_temp。

--http-proxy-temp-path=path
# 定义一个目录，用于存储包含从代理服务器接收到的数据的临时文件。安装后，可以始终nginx.conf使用proxy_temp_path 指令在配置文件中 更改目录 。默认情况下，目录名为 prefix/proxy_temp。

--http-fastcgi-temp-path=path
# 定义一个目录，用于存储包含从FastCGI服务器接收到的数据的临时文件。安装后，可以始终nginx.conf使用fastcgi_temp_path 指令在配置文件中 更改目录 。默认情况下，目录名为 prefix/fastcgi_temp。

--http-uwsgi-temp-path=path
# 定义一个目录，用于存储带有从uwsgi服务器接收到的数据的临时文件。安装后，可以始终nginx.conf使用uwsgi_temp_path 指令在配置文件中 更改目录 。默认情况下，目录名为 prefix/uwsgi_temp。

--http-scgi-temp-path=path
# 定义一个目录，用于存储带有从SCGI服务器接收到的数据的临时文件。安装后，可以始终nginx.conf使用scgi_temp_path 指令在配置文件中 更改目录 。默认情况下，目录名为 prefix/scgi_temp。

--without-http
# 禁用HTTP服务器。

--without-http-cache
# 禁用HTTP缓存。

--with-mail
--with-mail=dynamic
# 启用POP3 / IMAP4 / SMTP 邮件代理服务器。

--with-mail_ssl_module
# 支持构建一个模块，该模块 向邮件代理服务器添加 SSL / TLS协议支持。默认情况下未构建此模块。需要OpenSSL库来构建和运行此模块。

--without-mail_pop3_module
# 在邮件代理服务器中 禁用POP3协议。

--without-mail_imap_module
# 在邮件代理服务器中 禁用IMAP协议。

--without-mail_smtp_module
# 在邮件代理服务器中 禁用SMTP协议。

--with-stream
--with-stream=dynamic
# 支持构建 用于通用TCP / UDP代理和负载平衡的 流模块。默认情况下未构建此模块。

--with-stream_ssl_module
# 支持构建一个模块，该模块 向流模块添加 SSL / TLS协议支持。默认情况下未构建此模块。需要OpenSSL库来构建和运行此模块。

--with-stream_realip_module
# 启用构建ngx_stream_realip_module 模块的功能，该 模块将客户端地址更改为PROXY协议标头中发送的地址。默认情况下未构建此模块。

--with-stream_geoip_module
--with-stream_geoip_module=dynamic
# 支持构建ngx_stream_geoip_module 模块，该 模块根据客户端IP地址和预编译的MaxMind数据库创建变量 。默认情况下未构建此模块。

--with-stream_ssl_preread_module
# 支持构建ngx_stream_ssl_preread_module 模块，该 模块允许从ClientHello 消息中提取信息， 而无需终止SSL / TLS。默认情况下未构建此模块。

--without-stream_limit_conn_module
# 禁用构建ngx_stream_limit_conn_module 模块，该 模块限制每个键的连接数，例如，单个IP地址的连接数。

--without-stream_access_module
# 禁用构建ngx_stream_access_module 模块，该 模块允许限制对某些客户端地址的访问。

--without-stream_geo_module
# 禁用构建ngx_stream_geo_module 模块，该 模块创建的变量值取决于客户端IP地址。

--without-stream_map_module
# 禁用构建ngx_stream_map_module 模块，该 模块创建的变量值取决于其他变量的值。

--without-stream_split_clients_module
# 禁用构建ngx_stream_split_clients_module 模块，该 模块创建用于A / B测试的变量。

--without-stream_return_module
# 禁用构建ngx_stream_return_module 模块，该 模块向客户端发送一些指定的值，然后关闭连接。

--without-stream_upstream_hash_module
# 禁用构建实现哈希 负载平衡方法的模块 。

--without-stream_upstream_least_conn_module
# 禁用构建实现了minimum_conn 负载平衡方法的模块 。

--without-stream_upstream_zone_module
# 禁用构建模块，该模块可以将上游组的运行时状态存储在共享内存 区域中。

--with-google_perftools_module
# 允许构建ngx_google_perftools_module 模块，该 模块可以使用Google Performance Tools对nginx工作进程进行 性能分析。该模块适用于Nginx开发人员，默认情况下未构建。

--with-cpp_test_module
# 启用构建 ngx_cpp_test_module模块。

--add-module=path
# 启用外部模块。

--add-dynamic-module=path
# 启用外部动态模块。

--with-compat
# 启用动态模块兼容性。

--with-cc=path
# 设置C编译器的名称。

--with-cpp=path
# 设置C预处理器的名称。

--with-cc-opt=parameters
# 设置将添加到CFLAGS变量的其他参数。在FreeBSD下使用系统PCRE库时， --with-cc-opt="-I /usr/local/include" 应指定。如果select()需要增加支持的文件数量，也可以在此处指定，例如： --with-cc-opt="-D FD_SETSIZE=2048"。

--with-ld-opt=parameters
# 设置将在链接期间使用的其他参数。在FreeBSD下使用系统PCRE库时， --with-ld-opt="-L /usr/local/lib" 应指定。

--with-cpu-opt=cpu
# 每个指定的CPU能够使建筑： pentium，pentiumpro， pentium3，pentium4， athlon，opteron， sparc32，sparc64， ppc64。

--without-pcre
# 禁用PCRE库的使用。

--with-pcre
# 强制使用PCRE库。

--with-pcre=path 
# 设置PCRE库源的路径。需要从PCRE站点下载并分发库分发（版本4.4 — 8.43） 。其余的由nginx的./configure和完成 make。该库对于location指令中的正则表达式支持和 ngx_http_rewrite_module 模块是必需的 。

--with-pcre-opt=parameters
# 为PCRE设置其他构建选项。

--with-pcre-jit
# 使用“及时编译”支持（1.1.12，pcre_jit指令）构建PCRE库 。

--with-zlib=path 
# 设置zlib库源的路径。需要从zlib站点下载库发行版（版本1.1.3-1.2.11） 并解压缩。其余的由nginx的./configure和完成 make。ngx_http_gzip_module模块需要该库 。

--with-zlib-opt=parameters
# 为zlib设置其他构建选项。

--with-zlib-asm=cpu
# 使得能够使用指定的CPU中的一个优化的zlib汇编源程序： pentium，pentiumpro。

--with-libatomic
# 强制使用libatomic_ops库。

--with-libatomic=path
# 设置libatomic_ops库源的路径。

--with-openssl=path
# 设置OpenSSL库源的路径。

--with-openssl-opt=parameters
# 为OpenSSL设置其他构建选项。

--with-debug
# 启用调试日志。

```
### 参考
https://blog.csdn.net/weixin_45880055/article/details/118399512