### nginx 配置
```
server {
    listen 8080;

    location / {
      proxy_pass http://api:8080/;
        
      proxy_set_header Host              $host;   # host地址
      proxy_set_header X-Real-IP         $remote_addr;  #真实IP地址
      proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for; 
      proxy_set_header X-Forwarded-Server $host;
      proxy_set_header X-Forwarded-Port  $server_port;  #转发端口 swagger没有配置该参数会导致丢失端口
    }
  }
```

### 说明
#### 语法规则：
= 开头表示精确匹配  
^~ 开头表示uri以某个常规字符串开头，理解为匹配url路径即可(非正则)  
~ 开头表示区分大小写的正则匹配  
~* 开头表示不区分大小写的正则匹配  
!~和!~*分别为区分大小写不匹配及不区分大小写不匹配的正则  
/ 通用匹配，任何请求都会匹配到,(/会最长路径匹配)

#### 优先级：
等号类型（=）的优先级最高。一旦匹配成功，则不再查找其他location的匹配项  
^~和通用匹配。使用前缀匹配，不支持正则表达式，如果有多个location匹配成功的话，不会终止匹配过程，会匹配表达式最长的那个(下方有例子)  
如果上一步得到的最长的location为^~类型，则表示阻断正则表达式，不再匹配正则表达式  
如果上一步得到的最长的location不是^~类型，继续匹配正则表达式，只要有一个正则成功，则使用这个正则的location，立即返回结果，并结束解析过程  