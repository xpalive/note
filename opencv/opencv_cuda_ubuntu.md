### 在ubuntu中通过源码编译开启opencv的cuda能力
1. 官方参考：https://docs.opencv.org/4.9.0/d2/de6/tutorial_py_setup_in_ubuntu.html
  > 安装相关依赖
  > - sudo apt-get install cmake
  > - sudo apt-get install gcc g++
  > - sudo apt-get install python3-dev python3-numpy
2. 下载opencv源码
  > - https://github.com/opencv/opencv/archive/refs/tags/4.9.0.zip
  > - https://github.com/opencv/opencv_contrib/archive/refs/tags/4.9.0.zip
3. 创建目录
```
  ----opencv  
        |-opencv-4.9.0  
        |-opencv_contrib-4.9.0  
        |-build
```
4. 操作步骤
```shell
   cd build
   cmake -D CMAKE_BUILD_TYPE=RELEASE \
      	-D CMAKE_INSTALL_PREFIX=/usr/local \
      	-D WITH_TBB=ON \
      	-D ENABLE_FAST_MATH=1 \
      	-D CUDA_FAST_MATH=1 \
      	-D WITH_CUBLAS=1 \
      	-D WITH_CUDA=ON \
      	-D WITH_CUDNN=ON \
      	-D BUILD_opencv_cudacodec=ON \
      	-D WITH_CUDNN=ON \
      	-D OPENCV_DNN_CUDA=OFF \
      	-D CUDA_ARCH_BIN=7.5 \
      	-D WITH_V4L=ON \
      	-D WITH_QT=OFF \
      	-D WITH_OPENGL=ON \
      	-D WITH_GSTREAMER=ON \
      	-D OPENCV_GENERATE_PKGCONFIG=ON \
      	-D OPENCV_PC_FILE_NAME=opencv.pc \
      	-D OPENCV_ENABLE_NONFREE=ON \
      	-D BUILD_opencv_python3=ON \
      	-D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.9.0/modules ../opencv-4.9.0 \
      	-D INSTALL_PYTHON_EXAMPLES=OFF \
      	-D INSTALL_C_EXAMPLES=OFF \
      	-D WITH_NVCUVID=OFF \
      	-D WITH_NVCUVENC=OFF \
      	-D BUILD_EXAMPLES=OFF
   make -j8
   make install
```

默认的安装路径为：/usr/local/lib/python3.10/dist-packages

#### 备注
CUDA_ARCH_BIN=7.5 具体的值参考https://blog.csdn.net/weixin_44733606/article/details/131721081    
-j8 可以通过 nproc 来查看cpu核心数来确认 -j的参数 如：-j16    
#### 参考文档
https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7    
https://blog.csdn.net/tracelessle/article/details/108209710

#### conda 环境安装opencv cuda
https://blog.csdn.net/weixin_36976685/article/details/132716720
主要是cmake编译选项需要修改

以下两个选项按照自己情况修改  
    -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules \  
    -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-12.2 \  

其中/home/lixin/anaconda3/envs/stereo 改成你自己的conda环境
```shell
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12 \
    -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.9.0/modules ../opencv-4.9.0 \
    -D PYTHON_DEFAULT_EXECUTABLE=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12/bin/python \
    -D PYTHON3_EXECUTABLE=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12/bin/python \
    -D PYTHON3_INCLUDE_DIR=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12/include/python3.10 \
    -D PYTHON3_PACKAGES_PATH=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12/lib/python3.10/site-packages \
    -D PYTHON3_LIBRARY=/home/xiongping/app/miniconda3/envs/opencv-python3.10.12/lib/libpython3.10.so \
    -D BUILD_opencv_java=OFF \
    -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-12.2 \
    -D WITH_OPENGL=ON \
    -D WITH_OPENCL=ON \
    -D WITH_OPENMP=OFF \
    -D INSTALL_C_EXAMPLES=OFF \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D WITH_CUDA=ON \
    -D WITH_CUDNN=ON \
    -D OPENCV_DNN_CUDA=ON \
    -D ENABLE_FAST_MATH=1 \
    -D CUDA_FAST_MATH=1 \
    -D CUDA_ARCH_BIN=7.5 \
    -D WITH_CUBLAS=1 \
    -D BUILD_EXAMPLES=ON \
    -D HAVE_opencv_python3=ON \
    -D BUILD_opencv_python2=OFF \
    -D WITH_NVCUVENC=OFF \
    -D WITH_NVCUVENC=OFF \
    -D OPENCV_DNN_CUDA=OFF \
    -D BUILD_TIFF=ON
```
编译 make -j8
安装 make install
