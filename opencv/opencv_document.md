### 生成opencv文档
https://docs.opencv.org/4.x/d4/db1/tutorial_documentation.html

#### 安装doxygen
在Ubuntu 22.04.1 LTS系统下
使用命令安装doxygen
```shell
sudo apt-get update
sudo apt-get install doxygen
```

#### 文档生成
创建文档目录
假设文档目录与源码目录在同级
源码目录假设为~/$open_src
文档目录假设为~/$open_doc
执行命令
```shell
cmake -DBUILD_DOCS=ON ../$opencv_src
make doxygen
```
文档生成目录
Open ~/$open_doc/doc/doxygen/html/index.html file in your favorite browser