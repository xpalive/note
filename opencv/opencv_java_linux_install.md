### 前置条件
需要Java环境 JAVA_HOME
需要ant环境 ANT_HOME

安装g++ gcc
安装cmake make

### 仅在centos7安装成功/ubuntu 22
需要注意权限问题，具体是什么权限问题，还未知
1. 在centos7下是使用root用户安装
2. 在ubuntu下使用的是普通用户安装，在ubuntu下切换到root用户，配置环境变量，source下/etc/profile,然后再使用cmake进行编译即可

下载opencv源码，可以通过git clone ，也可以直接下载zip包
这里以zip包举例,opencv-4.5.4.zip
下载zip包后，解压到目录下opencv-4.5.4,后续以$opencv_dir 指代当前opencv-4.5.4目录
```shell
cd $opencv_dir
mkdir build
mkdir build/install
cd build

cmake -D CMAKE_BUILD_TYPE=Release -D BUILD_SHARED_LIBS=OFF -D CMAKE_INSTALL_PREFIX=./install ../opencv-4.5.4
make -j8    ## -j8表示8个现场并行编译
make install  
```

### 输出结果中包含如下字段
![img.png](image/opencv-build-output.png)
openCV modules:
  to be built:   java 
   
reference
https://blog.csdn.net/wangxueying5172/article/details/122595155