+ [X] 网关接入
  + [ ] 流量染色
+ [X] nacos 接入
  + [X] 服务发现
  + [X] 配置中心
+ [ ] sentinel 接入
  + [ ] 服务限流
  + [ ] 服务降级
+ [ ] spring security 接入
+ [X] 负载均衡接入
  + [ ] 负载均衡轮训方案
  + [ ] 负载均衡一致性hash方案实现
  + [ ] 负载均衡权重方案实现
+ [ ] 消息中间件
  + [ ] kafka
  + [ ] rabbitMQ
  + [ ] rocketMQ
+ [ ] 分布式事务
+ [ ] 缓存一致性
+ [ ] 服务调用
  + [ ] openfeign
  + [ ] dubbo
+ [ ] spring cloud alibaba https://blog.csdn.net/weixin_67450855/article/details/128019011
+ [ ] elasticsearch 安装
+ [ ] redis 安装
+ [ ] mongodb 安装
+ [ ] skywalking 链路跟踪