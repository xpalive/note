+ [ ] nginx 通过软件方式搭建集群 https://blog.csdn.net/F15217283411/article/details/120801130
+ [X] 高质量的软件是否值得付出代价 https://www.cnblogs.com/BlogNetSpace/p/15638433.html
+ [X] 重构 https://baike.baidu.com/item/%E9%87%8D%E6%9E%84/2182519
+ [ ] DeferredImportSelector 和 ImportSelector的区别（Deferred 延迟解析）
+ [ ] java spi
+ [ ] spring 的condition注解
+ [ ] slam学习 https://www.zhihu.com/column/c_1309081426717884416
+ [ ] javacv 计算机视觉开发库 https://github.com/bytedeco
+ [ ] ES filesystem cache https://blog.csdn.net/ZGL_cyy/article/details/118230086
+ [ ] spring 抽象类注入bean
+ [ ] spring beanPostProcesses 创建及执行时机
+ [ ] spring 加载配置文件的时机
+ [ ] lombok 扩展 https://www.jianshu.com/p/6717d8ea9403
+ [ ] 高并发秒杀场景 https://blog.csdn.net/weixin_42324471/article/details/121076828
+ [ ] 线程池底层工作原理
+ [ ] 分布式CAP理论
+ [ ] 再次理解spring 事务
+ [ ] spring starter 加载流程图
+ [X] 写一个spring starter
+ [ ] redis过期监听
+ [ ] redis消息队列
+ [ ] redis数据分片
+ [ ] redis主从复制风暴
+ [ ] redis脑裂
+ [ ] redis读写锁
+ [ ] mybatis和spring整合后一级缓存失效
+ [ ] 解决数据库连接超时问题
    > The last packet successfully received from the server was 384,518 milliseconds ago.   
      The last packet sent successfully to the server was 384,531 milliseconds ago.).  
      Possibly consider using a shorter maxLifetime value.  
+ [ ] 完善简历
    > 针对简历中的知识点完成对于知识的补充
+ [ ] mysql
    > mysql
+ [ ] jvm 
    > 性能调优  
      垃圾回收器  
      垃圾回收算法  
+ [ ] 搭建redis环境
    > 主从，持久化
+ [ ] 限流使用 sentinel
+ [ ] 注册中心/配置中心使用 nacos
+ [ ] dubbo 使用
+ [ ] java.util.ServiceLoader
    > ServiceLoader<AbstractEngineAuditParallelService> loader = ServiceLoader.load(AbstractEngineAuditParallelService.class);  
      long count = StreamSupport.stream(loader.spliterator(), false).count();
+ [ ] ConcurrentHashMap
    > https://blog.csdn.net/justloveyou_/article/details/72783008  
    分段锁  
      读写锁                    
+ [ ] synchronized
    > 同对象，同方法，值不同锁不同  
      同对象，同方法，一把锁  
      不同对象，同方法，一把锁  
      https://www.cnblogs.com/codebj/p/10994748.html  
      https://blog.csdn.net/cgsyck/article/details/105986284

+ [ ] lettuce
    > https://blog.csdn.net/weixin_45145848?type=blog   lettuce 
