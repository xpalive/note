### 数字孪生

### 区块链

### 物联网

### AR+GIS
1. AR地图
2. AR测图
3. AR定位
4. AR分析

> ARCore/ARKit/VINS + OpenCV
> SLAM
> IMU
> AI(TensorFlow)
> 空间矩阵变换原理
> 视觉校准和惯性校准算法

### AR地图
AR地图是以视频为底图，通过坐标的精确匹配后，将视频图像和二三维地理空间信息进行实时融合现在，达到增强现实（Augmented Reality）的效果

### 位置定位
室外：GPS
室内：wifi 蓝牙 rfid AR惯导定位（SLAM+IMU）

符号化渲染场景

### 相机标定
https://wenku.baidu.com/view/59fad8f31a5f312b3169a45177232f60dccce75a.html
https://wenku.baidu.com/view/162b1ac8e87101f69f31953b?aggId=162b1ac8e87101f69f31953b