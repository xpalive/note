### 集群部署
1. 添加`myid`到`dataDir`下
2. myid中的数值为节点id
3. server.x=0.0.0.0:2001:3001 
   1. 其中x就是myid中的数值
   2. ip为本机网卡ip地址，在阿里云中需要写成0.0.0.0来绑定对应端口
   3. 2001为Quorum 端口
   4. 3001为选举Leader 端口
4. 配置如下
```shell
# The number of milliseconds of each tick
tickTime=2000
# The number of ticks that the initial 
# synchronization phase can take
initLimit=10
# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
syncLimit=5
# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
dataDir=/root/apache-zookeeper-3.5.8-bin/data
# the port at which the clients will connect
clientPort=2181
# the maximum number of client connections.
# increase this if you need to handle more clients
#maxClientCnxns=60
#
# Be sure to read the maintenance section of the 
# administrator guide before turning on autopurge.
#
# http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
#
# The number of snapshots to retain in dataDir
#autopurge.snapRetainCount=3
# Purge task interval in hours
# Set to "0" to disable auto purge feature
#autopurge.purgeInterval=1

server.1=8.217.66.241:2001:3001
server.2=8.218.241.79:2001:3001
server.3=0.0.0.0:2001:3001
server.4=8.218.166.40:2001:3001:observer
```