### 可视化工具
preettyzoo

### 安装zookeeper
> wget https://archive.apache.org/dist/zookeeper/zookeeper-3.5.8/apache-zookeeper-3.5.8-bin.tar.gz
> tar -zxvf apache-zookeeper-3.5.8-bin.tar.gz
> cd apache-zookeeper-3.5.8-bin
> mkdir data
> cp conf/zoo-sample.cfg conf/zoo.cfg
> zoo.cfg的存储位置`dataDir=/tmp/zookeeper`
> 3.5.0版本以上还会需要占用8080端口，`admin.serverPort=8080`

### 命令
1. 启动 bin/zkServer.sh -start conf/zoo.cfg
2. 连接 bin/zkCli.sh -server ip:port
3. 查看节点 ls /

### 要点
1. 节点数据结构
   1. 持久化目录节点、持久化顺序目录节点、临时目录节点、临时顺序目录节点、container节点、TTL节点
2. 监听机制：监听机制响应一次后需要重新监听

### New in 3.5.0: The following options are used to configure the AdminServer.
1. admin.enableServer : (Java system property: zookeeper.admin.enableServer) Set to "false" to disable the AdminServer. By default the AdminServer is enabled.   
2. admin.serverAddress : (Java system property: zookeeper.admin.serverAddress) The address the embedded Jetty server listens on. Defaults to 0.0.0.0.  
3. admin.serverPort : (Java system property: zookeeper.admin.serverPort) The port the embedded Jetty server listens on. Defaults to 8080.  
4. admin.idleTimeout : (Java system property: zookeeper.admin.idleTimeout) Set the maximum idle time in milliseconds that a connection can wait before sending or receiving data. Defaults to 30000 ms.  
5. admin.commandURL : (Java system property: zookeeper.admin.commandURL) The URL for listing and issuing commands relative to the root URL. Defaults to "/commands".


### zookeeper
leader
follower
observer

### 阿里云服务器端口监听
本机需要改成 0.0.0.0 以zookeeper为例
如下
```shell
server.1=8.217.66.241:2001:3001 本机需要改为0.0.0.0:2001:3001
server.2=8.218.241.79:2001:3001
server.3=47.242.58.236:2001:3001
server.4=8.218.166.40:2001:3001:observer
```
https://blog.csdn.net/qq_35457078/article/details/86064199

### 参考
http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance