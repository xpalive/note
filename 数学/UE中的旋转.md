在 Unreal Engine (UE) 中，尽管坐标系是左手坐标系，使用的旋转矩阵与右手坐标系的定义在符号上有所不同。这可能会引起混淆。我们来逐步分析为什么在代码中使用的旋转矩阵与理论上的左手坐标系旋转矩阵不同。

### 1. **坐标系与旋转矩阵的关系**
- **左手坐标系**：在左手坐标系中，绕某个轴旋转时，遵循左手法则。
- **右手坐标系**：在右手坐标系中，遵循右手法则。

虽然 UE 使用左手坐标系，但其使用的旋转矩阵实际上是根据物体在世界坐标系中的行为来定义的。这就意味着 UE 中的旋转矩阵是基于右手坐标系的定义，但它们的应用是在左手坐标系的上下文中。

### 2. **旋转矩阵的符号变化**
以下是代码中与左手坐标系相符的旋转矩阵与代码中使用的旋转矩阵的比较：

#### 理论上的左手坐标系旋转矩阵：
- **绕 X 轴**：
  \[
  R_x(\theta) = \begin{bmatrix}
  1 & 0 & 0 \\
  0 & \cos(\theta) & \sin(\theta) \\
  0 & -\sin(\theta) & \cos(\theta)
  \end{bmatrix}
  \]

- **绕 Y 轴**：
  \[
  R_y(\theta) = \begin{bmatrix}
  \cos(\theta) & 0 & -\sin(\theta) \\
  0 & 1 & 0 \\
  \sin(\theta) & 0 & \cos(\theta)
  \end{bmatrix}
  \]

- **绕 Z 轴**：
  \[
  R_z(\theta) = \begin{bmatrix}
  \cos(\theta) & \sin(\theta) & 0 \\
  -\sin(\theta) & \cos(\theta) & 0 \\
  0 & 0 & 1
  \end{bmatrix}
  \]

#### 代码中的旋转矩阵：
- **绕 X 轴（Roll）**：
  \[
  R_x(\theta) = \begin{bmatrix}
  1 & 0 & 0 \\
  0 & \cos(\theta) & -\sin(\theta) \\
  0 & \sin(\theta) & \cos(\theta)
  \end{bmatrix}
  \]

- **绕 Y 轴（Pitch）**：
  \[
  R_y(\theta) = \begin{bmatrix}
  \cos(\theta) & 0 & \sin(\theta) \\
  0 & 1 & 0 \\
  -\sin(\theta) & 0 & \cos(\theta)
  \end{bmatrix}
  \]

- **绕 Z 轴（Yaw）**：
  \[
  R_z(\theta) = \begin{bmatrix}
  \cos(\theta) & -\sin(\theta) & 0 \\
  \sin(\theta) & \cos(\theta) & 0 \\
  0 & 0 & 1
  \end{bmatrix}
  \]

### 3. **为什么符号不同**
在代码中，虽然旋转矩阵的定义在形式上看起来与左手坐标系的定义不符，但它们实际上是对旋转的方向进行了一种适配。

- 在 UE 中，由于实现上的方便，采用了与右手坐标系相似的矩阵形式，同时利用不同的旋转顺序（Yaw → Pitch → Roll）来确保旋转行为符合左手坐标系的预期。
- 具体来说，虽然使用的矩阵形式与右手坐标系的旋转矩阵相似，但由于旋转顺序的影响，最终得到的旋转效果符合左手坐标系的逻辑。

### 结论
旋转矩阵的符号变化主要是为了适配特定的旋转顺序和坐标系约定。虽然理论上左手坐标系的旋转矩阵形式可能与右手坐标系不同，但在 UE 的实现中，通过特定的旋转顺序和实现细节，确保了在使用过程中符合左手坐标系的行为预期。