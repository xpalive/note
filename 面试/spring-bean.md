### 配置文件的读取实际上上spring boot的行为
ConfigFileApplicationListener
ConfigDataEnvironmentPostProcessor 
### 实例化
BeanFactoryPostProcessor 
备注：如果使用注解扫描的方式注入的方式那么只能使用无参的构造方法，因为在处理BeanFactoryPostProcessor的时候，BeanPostProcessor还没开始处理，没有办法获取有参的构造方法
BeanFactoryPostProcessor（扫描beanDefinition阶段）
    1. postProcessBeanFactory：可以获取当前的beanFactory，可直接将beanPostProcessor添加到当前容器中
       例子：（ConfigurationClassPostProcessor#postProcessBeanFactory）
BeanDefinitionRegistryPostProcessor（扫描beanDefinitely阶段）
    1. 注册postProcessBeanDefinitionRegistry：beanDefinition的注册逻辑
       例子：（ConfigurationClassPostProcessor#postProcessBeanDefinitionRegistry）

InstantiationAwareBeanPostProcessor（beanDefinition转换为bean阶段，实例化）
    1. 实例化前 postProcessBeforeInstantiation：如果返回了对象则直接到初始化后
        1. SmartInstantiationAwareBeanPostProcessor.determineCandidateConstructors => 选择构造方法
        2. MergedBeanDefinitionPostProcessor.postProcessMergedBeanDefinition => 处理BeanDefinition
    2. 属性填充 postProcessProperties
    3. 实例化后 postProcessAfterInstantiation: !mbd.isSynthetic() 且 如果返回了false就直接中断属性填充了

BeanPostProcessor（beanDefinition转换为bean阶段，实例化）
    1. aware (BeanName, BeanClassLoader, BeanFactory)
    1. postProcessBeforeInitialization 初始化前
    1. 初始化后applyBeanPostProcessorsAfterInitialization：已经设置好属性值了
       例子：在AbstractAutoProxyCreator 中会对bean对象进行处理生成代理对象成为bean对象

### @Component 和 @Configuration 的区别
区别在于@Configuration提供了代理机制，生成的bean是一个代理类
在调用其方法体中被@Bean标注的方法会先判断是否存在当前bean，如果存在则直接返回，不存在则生成
而@Component则会返回多个不同的实例

### @Import
在调用@Import导入类时，如果这个类只是一个bean则可以不同添加@Configuration注解
如果这个类中有其他的方法被标注了@Bean则需要添加@Configuration方法，否则@Bean不生效

### 备注
mbd.isSynthetic() 用来判断一个bean是否是由容器运行时创建的而不是由用户自己定义的
如通过xml或者是Java配置类来定义的bean，isSynthetic返回的是false
比如<bean>标签，@Bean，@Component，@Import，@ComponentScan引入的类
通过动态代理ProxyFactoryBean创建的对象生成的beanDefinition返回的就是true

ImportBeanDefinitionRegistrar


### 初始化机制的顺序
@PostConstruct
afterPropertiesSet(InitializingBean 接口)
initMethod(@Bean(initMethod= "methodName"))

这里的methodName指的是初始化对应的对象中的方法名