```yaml
server:
  port: 8888
spring:
  cloud:
    gateway:
      routes:
        - id: finance_route
          uri: http://127.0.0.1:8081/finance
          predicates:
            - Path=/bearCoin/**
          filters:
            - RewritePath=/bearCoin/(?<segment>.*), /bearCoin/${segment}
            - AddRequestHeader=X-Request-Foo, Bar
```
配置说明：
1. Path=/bearCoin/**： 匹配所有以 /bearCoin/ 开头的请求路径。
2. RewritePath 过滤器： 
   1. 将路径 /bearCoin/detailList?id=0 保持不变地转发到目标服务中 /finance/bearCoin/detailList?id=0。
   2. - AddRequestHeader=X-Request-Foo, Bar 添加头信息