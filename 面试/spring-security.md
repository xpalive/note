spring security
比较适合单应用程序，其中csrf是需要和会话绑定的，或者是通过redis等第三方组件来共享会话来防止跨站点攻击

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
                .antMatchers("/public/**").permitAll() // 公共资源无需认证
                .antMatchers("/admin/**").hasRole("ADMIN") // 只有管理员角色可访问
                .anyRequest().authenticated(); // 其他所有请求需要认证
    }
}

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {
    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.csrf().disable()
                .authorizeExchange()
                .pathMatchers("/public/**").permitAll() // 公共资源无需认证
                .pathMatchers("/admin/**").hasRole("ADMIN") // 只有管理员角色可访问
                .anyExchange().authenticated() // 其他所有请求需要认证
                .and()
                .build();
    }
}
```

特性              authorizeRequests	              authorizeExchange
适用编程模型        Servlet（Spring MVC）	          Reactive（Spring WebFlux / Gateway）
核心接口           HttpSecurity	                  ServerHttpSecurity
请求类型           同步（阻塞）	                      异步（非阻塞）
路径匹配器         antMatchers 和 regexMatchers	  pathMatchers
适用场景           Web 应用程序、传统 Servlet 应用	  WebFlux 应用程序、Spring Cloud Gateway
授权配置方式       命令式（imperative）	              声明式（declarative）


### 使用账号密码验证的话还需要自定义一些bean
```
@Bean
public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}

@Bean
public AuthenticationManager authenticationManager(AuthenticationManagerBuilder auth) throws Exception {
    return auth.authenticationProvider(customAuthenticationProvider()).build();
}

@Bean
public CustomAuthenticationProvider customAuthenticationProvider() {
    return new CustomAuthenticationProvider(userDetailsService, passwordEncoder());
}
```